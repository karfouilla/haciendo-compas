/**
 * @file paternwidget.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////


#include "paternwidget.h"

#include <QMouseEvent>

#include "patern.h"
#include "regularlayout.h"
#include "scoreatomwidget.h"

PaternWidget::PaternWidget(const Patern& _patern, QWidget* parent):
	QWidget(parent),
	m_patern(_patern),
	m_pLayout(Q_NULLPTR),
	m_lstAtoms()
{
	setAutoFillBackground(true);
	setBackgroundRole(QPalette::Dark);
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	updateWidgets();
}

const Patern& PaternWidget::patern() const
{
	return m_patern;
}

void PaternWidget::setPatern(const Patern& _patern)
{
	m_patern = _patern;
	updateWidgets();
}

void PaternWidget::updateWidgets()
{
	if(m_pLayout)
	{
		while(!m_pLayout->isEmpty())
			delete m_pLayout->takeAt(0);
		qDeleteAll(m_lstAtoms); // test
		m_lstAtoms.clear();
	}
	else
	{
		m_pLayout = new RegularLayout(Qt::Horizontal, this);
		m_pLayout->setContentsMargins(2, 2, 2, 2);
		m_pLayout->setSpacing(4);
		setLayout(m_pLayout);
	}
	foreach(const ScoreAtom& atom, m_patern.catoms())
	{
		ScoreAtomWidget* wdgAtom(new ScoreAtomWidget(atom, this));
		m_pLayout->addWidget(wdgAtom);
		m_lstAtoms.append(wdgAtom);
	}
}

void PaternWidget::paintEvent(QPaintEvent* event)
{
	QWidget::paintEvent(event);
}

void PaternWidget::mousePressEvent(QMouseEvent* event)
{
	QWidget::mousePressEvent(event);
	if(event->button() == Qt::RightButton)
	{
		emit triggered(m_patern.id());
	}
}
