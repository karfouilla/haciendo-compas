/**
 * @file patern.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-04
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "patern.h"

Patern::Patern():
	m_id(),
	m_szName(),
	m_lstAtoms()
{ }
Patern::Patern(const QString& _id, const QString& szName):
	m_id(_id),
	m_szName(szName),
	m_lstAtoms()
{ }
Patern::Patern(const Patern& other):
	m_id(other.m_id),
	m_szName(other.m_szName),
	m_lstAtoms(other.m_lstAtoms)
{ }
Patern::Patern(Patern&& other):
	m_id(std::move(other.m_id)),
	m_szName(std::move(other.m_szName)),
	m_lstAtoms(std::move(other.m_lstAtoms))
{ }
Patern& Patern::operator=(const Patern& other)
{
	m_id = other.m_id;
	m_szName = other.m_szName;
	m_lstAtoms = other.m_lstAtoms;
	return *this;
}
Patern& Patern::operator=(Patern&& other)
{
	m_id = std::move(other.m_id);
	m_szName = std::move(other.m_szName);
	m_lstAtoms = std::move(other.m_lstAtoms);
	return *this;
}

const QString& Patern::id() const
{
	return m_id;
}
const QString& Patern::name() const
{
	return m_szName;
}

void Patern::setId(const QString& _id)
{
	m_id = _id;
}
void Patern::setName(const QString& szName)
{
	m_szName = szName;
}

const QList<ScoreAtom>& Patern::catoms() const
{
	return m_lstAtoms;
}
const ScoreAtom& Patern::catom(int i) const
{
	return m_lstAtoms.at(i);
}

QList<ScoreAtom>& Patern::atoms()
{
	return m_lstAtoms;
}
ScoreAtom& Patern::atom(int i)
{
	return m_lstAtoms[i];
}
