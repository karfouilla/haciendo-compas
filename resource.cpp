/**
 * @file resource.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-08
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "resource.h"

#include <QtCore/QBuffer>
#include <QtCore/QFile>

Resource::Resource() Q_DECL_NOTHROW:
	m_mapXMLDatas()
{ }

Resource::Resource(const Resource& other):
	m_mapXMLDatas(other.m_mapXMLDatas)
{ }

Resource::Resource(Resource&& other) Q_DECL_NOTHROW:
	m_mapXMLDatas(std::move(other.m_mapXMLDatas))
{ }

Resource& Resource::operator=(const Resource& other)
{
	m_mapXMLDatas = other.m_mapXMLDatas;
	return *this;
}

Resource& Resource::operator=(Resource&& other) Q_DECL_NOTHROW
{
	m_mapXMLDatas = std::move(other.m_mapXMLDatas);
	return *this;
}

void Resource::addEntry(const QString& szName, const QByteArray& datas)
{
	m_mapXMLDatas.insert(szName, datas);
}

QIODevice* Resource::open(const Resource::Name& name)
{
	QIODevice* stream(Q_NULLPTR);
	if(name.type() == TYPE_FILE)
	{
		stream = new QFile(name.filename());
	}
	else if(name.type() == TYPE_XML)
	{
		QByteArray& datas(m_mapXMLDatas[name.filename()]);
		stream = new QBuffer(&datas);
	}
	stream->open(QIODevice::ReadOnly);
	return stream;
}

QIODevice* Resource::open(const QString& szFilename, Resource::Type eType)
{
	return open(Name(szFilename, eType));
}

const QMap<QString, QByteArray>& Resource::datas() const
{
	return m_mapXMLDatas;
}

Resource::Name::Name() Q_DECL_NOTHROW:
	m_szFileName(),
	m_eType(TYPE_FILE)
{ }

Resource::Name::Name(const QString& szFileName,
					 Resource::Type eType) Q_DECL_NOTHROW:
	m_szFileName(szFileName),
	m_eType(eType)
{ }

Resource::Name::Name(const Resource::Name& other)  Q_DECL_NOTHROW:
	m_szFileName(other.m_szFileName),
	m_eType(other.m_eType)
{ }

Resource::Name::Name(Resource::Name&& other) Q_DECL_NOTHROW:
	m_szFileName(std::move(other.m_szFileName)),
	m_eType(other.m_eType)
{ }

Resource::Name&
Resource::Name::operator=(const Resource::Name& other) Q_DECL_NOTHROW
{
	m_szFileName = other.m_szFileName;
	m_eType = other.m_eType;
	return *this;
}

Resource::Name& Resource::Name::operator=(Resource::Name&& other) Q_DECL_NOTHROW
{
	m_szFileName = std::move(other.m_szFileName);
	m_eType = other.m_eType;
	return *this;
}

const QString& Resource::Name::filename() const Q_DECL_NOTHROW
{
	return m_szFileName;
}

Resource::Type Resource::Name::type() const Q_DECL_NOTHROW
{
	return m_eType;
}

void Resource::Name::setFilename(const QString& szFilename) Q_DECL_NOTHROW
{
	m_szFileName = szFilename;
}

void Resource::Name::setType(Resource::Type eType) Q_DECL_NOTHROW
{
	m_eType = eType;
}
