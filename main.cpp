/**
 * @file main.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-08
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtWidgets/QApplication>

#include "mainwindow.h"

#include "scoreindex.h"
#include "scorebeat.h"
#include "scoresection.h"
#include "step.h"

#include <QTranslator>


int main(int argc, char** argv)
{
	QApplication app(argc, argv);

	QString locale(QLocale::system().name().section('_', 0, 0));

	QTranslator translator;
	translator.load(QString(":/data/lang/%1").arg(locale));
	app.installTranslator(&translator);

	MainWindow* win(new MainWindow);
	win->showMaximized();

	return app.exec();
}
