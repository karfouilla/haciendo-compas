#ifndef AUDIODATA_H_INCLUDED
#define AUDIODATA_H_INCLUDED
/**
 * @file audiodata.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-06
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QByteArray>
#include <QtCore/QIODevice>

#include <QtMultimedia/QAudioFormat>

class AudioData
{
public:
	AudioData();
	bool load(QIODevice* file);

	QByteArray& data();
	const QByteArray& cdata() const;

	QAudioFormat& format();
	const QAudioFormat& cformat() const;
	void setFormat(const QAudioFormat& format);

private:
	Q_DISABLE_COPY(AudioData)

private:
	QAudioFormat m_format;
	QByteArray m_data;
};

#endif // AUDIODATA_H_INCLUDED
