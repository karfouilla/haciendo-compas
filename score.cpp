/**
 * @file score.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-01
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "score.h"

#include "scoresection.h"
#include "step.h"

Score::Score():
	m_resource(),
	m_mapSteps(),
	m_lstPaterns(),
	m_pData(Q_NULLPTR),
	m_uDefaultDivision(4)
{
	m_pData = new ScoreSection;
}

Score::Score(const Score& other):
	m_resource(other.m_resource),
	m_mapSteps(),
	m_lstPaterns(other.m_lstPaterns),
	m_pData(Q_NULLPTR),
	m_uDefaultDivision(4)
{
	foreach(Step* pStep, m_mapSteps)
		m_mapSteps.insert(pStep->id(), new Step(*pStep));

	m_pData = new ScoreSection(*other.m_pData);
}

Score::~Score()
{
	delete m_pData;
	foreach(Step* pStep, m_mapSteps)
		delete pStep;
}

const Resource& Score::cresource() const
{
	return m_resource;
}
Resource& Score::resource()
{
	return m_resource;
}

const QMap<QString, Step*>& Score::csteps() const
{
	return m_mapSteps;
}
QMap<QString, Step*>& Score::steps()
{
	return m_mapSteps;
}

const Step* Score::cstep(const QString& id) const
{
	return m_mapSteps.value(id, Q_NULLPTR);
}
Step* Score::step(const QString& id)
{
	QMap<QString, Step*>::Iterator it(m_mapSteps.find(id));
	if(it == m_mapSteps.end())
		return Q_NULLPTR;
	else
		return it.value();
}

const QMap<QString, Patern>& Score::cpaterns() const
{
	return m_lstPaterns;
}
QMap<QString, Patern>& Score::paterns()
{
	return m_lstPaterns;
}

const Patern& Score::cpatern(const QString& id) const
{
	QMap<QString, Patern>::ConstIterator it(m_lstPaterns.constFind(id));
	Q_ASSERT(it != m_lstPaterns.end());
	return it.value();
}
Patern& Score::patern(const QString& id)
{
	QMap<QString, Patern>::Iterator it(m_lstPaterns.find(id));
	Q_ASSERT(it != m_lstPaterns.end());
	return it.value();
}

const ScoreSection* Score::cdata() const
{
	return m_pData;
}

ScoreSection* Score::data() const
{
	return m_pData;
}

quint32 Score::defaultDivision() const
{
	return m_uDefaultDivision;
}

void Score::setDefaultDivision(quint32 uDefaultDivision)
{
	m_uDefaultDivision = uDefaultDivision;
}
