#ifndef SCOREWIDGET_H_INCLUDED
#define SCOREWIDGET_H_INCLUDED
/**
 * @file scorewidget.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-24
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

#include "scoregroup.h"

class ScoreLines;
class ScoreSection;

class ScoreWidget : public QWidget
{
	Q_OBJECT
public:
	enum CursorName {
		CURSOR_DEFAULT,
		CURSOR_PLAY,
		CURSOR_LAST
	};

	explicit ScoreWidget(QWidget* parent = Q_NULLPTR);

	ScoreSection* model() const;
	void setModel(ScoreSection* pModel);

	int beatPerLines() const;
	void setBeatPerLines(int iBeatPerLines);
	void setSelection(const ScoreGroup& selection);
	ScoreGroup selection() const;


	ScoreIndex scoreCursor(std::size_t cursorName) const;
	void setScoreCursor(std::size_t cursorName, const ScoreIndex& idxCursor,
						bool updateSelection = true);

signals:
	void modelChanged(ScoreSection* pModel);
	void selectionChanged(ScoreGroup);
	void scoreCursorChanged(std::size_t, ScoreIndex);

public slots:
	void updateCurrentModel();
	void repaintChildren();
	void adjustCursor();
	void adjustSelection();

private slots:
	void updateModel(ScoreSection* pModel);

protected:
	virtual void mousePressEvent(QMouseEvent* event) Q_DECL_OVERRIDE;
	virtual void keyPressEvent(QKeyEvent* event) Q_DECL_OVERRIDE;
	virtual void focusInEvent(QFocusEvent* event) Q_DECL_OVERRIDE;
	virtual void focusOutEvent(QFocusEvent* event) Q_DECL_OVERRIDE;

private:
	QVector<ScoreLines*> m_lstLines;
	QVBoxLayout* m_pLayout;
	ScoreSection* m_pModel;
	int m_iBeatPerLines;
	ScoreGroup m_selection;
	ScoreIndex m_idxCursor[CURSOR_LAST];
};

#endif // SCOREWIDGET_H_INCLUDED
