#ifndef MAINWINDOW_H_INCLUDED
#define MAINWINDOW_H_INCLUDED
/**
 * @file mainwindow.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-08
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtMultimedia/QAudioOutput>

#include <QtWidgets/QMainWindow>

#include "score.h"
#include "step.h"
#include "scoreatom.h"
#include "scorewidget.h"
#include "scoreaudiogenerator.h"

QT_BEGIN_NAMESPACE
class QScrollArea;
QT_END_NAMESPACE

class PaternListWidget;
class DockScore;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QString filename = QString(),
						QWidget* parent = Q_NULLPTR);
	virtual ~MainWindow();

public slots:
	void dialogSetDivision();
	void dialogSetTempo();
	void dialogSetVolume();
	void dialogSetBeatPerLines();
	void dialogNewFile();
	void dialogOpenFile();
	void dialogSaveAsFile();
	void dialogSaveFile();
	void dialogExportAudio();

	void togglePlay();
	void stopPlaying();
	void playStateChange(QAudio::State state);

	void copySelection();
	void cutSelection();
	void pasteSelection();

private:
	void loadStepsRes();
	void initActions();
	void initStepActions();
	void initPaternActions();
	void initMenu();
	void initStepMenu(QMenu* addStepMenu);
	void initPaternMenu(QMenu* addPaternMenu);
	void initToolBars();
	void initDocks();

	void removeStepActions();
	void removePaternActions();
	void removeStepMenu(QMenu* addStepMenu);
	void removePaternMenu(QMenu* addPaternMenu);

	void addStep(Step* step);
	void addPatern(const QString& id);

	bool askFilenameSave();
	bool askFilenameExport(QString& filename);
	void loadFile();
	void saveFile();
	void prepareScore();

private:
	Score* m_pScore;
	ScoreSection* m_pClipboard;
	QScrollArea* m_wdgCentral;
	ScoreWidget* m_wdgScore;
	ScoreAudioGenerator* m_pGenerator;
	QAudioOutput* m_pOutput;

	QString m_szFilename;

	// File
	QAction* m_aNew;
	QAction* m_aOpen;
	QAction* m_aSave;
	QAction* m_aSaveAs;
	QAction* m_aExportAudio;
	QAction* m_aQuit;

	// Edit
	QAction* m_aCopy;
	QAction* m_aCut;
	QAction* m_aPaste;
	QList<QAction*> m_lstAddStep;
	QList<QAction*> m_lstAddPatern;
	QAction* m_aSimplify;
	QAction* m_aSetDivision;
	QAction* m_aSetTempo;
	QAction* m_aSetVolume;
	QAction* m_aToggleAccent;

	// Display
	QAction* m_aSetBeatPerLines;

	// Playing
	QAction* m_aPlay;
	QAction* m_aStop;
	QAction* m_aRepeat;

	// Menus
	QMenu* m_menuFile;
	QMenu* m_menuExport;
	QMenu* m_menuEdit;
	QMenu* m_menuAddStep;
	QMenu* m_menuAddPatern;
	QMenu* m_menuCursor;
	QMenu* m_menuDisplay;
	QMenu* m_menuPlay;

	// Toolbars
	QToolBar* m_tbFile;
	QToolBar* m_tbEdit;
	QToolBar* m_tbPlay;

	// Docks
	QDockWidget* m_dockPaterns;
	QDockWidget* m_dockScore;

	// Docks contents
	PaternListWidget* m_wdgPaternList;
	DockScore* m_wdgOptionScore;
};

#endif // MAINWINDOW_H_INCLUDED
