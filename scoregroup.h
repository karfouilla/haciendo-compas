#ifndef SCOREGROUP_H_INCLUDED
#define SCOREGROUP_H_INCLUDED
/**
 * @file scoregroup.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-20
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scoreindex.h"

class ScoreGroup
{
public:
	ScoreGroup() Q_DECL_NOTHROW;
	ScoreGroup(const ScoreGroup& other) Q_DECL_NOTHROW;
	ScoreGroup(ScoreIndex idxBegin, ScoreIndex idxEnd) Q_DECL_NOTHROW;
	ScoreGroup& operator=(const ScoreGroup& other) Q_DECL_NOTHROW;

	ScoreIndex begin() const Q_DECL_NOTHROW;
	ScoreIndex end() const Q_DECL_NOTHROW;

	void setBegin(const ScoreIndex& idxBegin) Q_DECL_NOTHROW;
	void setEnd(const ScoreIndex& idxEnd) Q_DECL_NOTHROW;

	bool isEmpty() Q_DECL_NOTHROW;

private:
	ScoreIndex m_idxBegin;
	ScoreIndex m_idxEnd;
};

#endif // SCOREGROUP_H_INCLUDED
