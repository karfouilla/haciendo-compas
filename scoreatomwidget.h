#ifndef SCOREATOMWIDGET_H_INCLUDED
#define SCOREATOMWIDGET_H_INCLUDED
/**
 * @file scoreatomwidget.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-21
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QTime>

#include <QtWidgets/QWidget>

#include "scoreatom.h"

class ScoreAtomWidget : public QWidget
{
	Q_OBJECT
public:
	explicit ScoreAtomWidget(QWidget* parent = Q_NULLPTR);
	ScoreAtomWidget(const ScoreAtom& atomData, QWidget* parent = Q_NULLPTR);

	virtual QSize minimumSizeHint() const Q_DECL_OVERRIDE;
	virtual QSize sizeHint() const Q_DECL_OVERRIDE;
	virtual QSize effectiveSize() const;
	static QSize nominalSize();

	QColor colorAccent() const;
	void setColorAccent(const QColor& colorAccent);

protected:
	virtual void paintEvent(QPaintEvent* event) Q_DECL_OVERRIDE;
	virtual void contextMenuEvent(QContextMenuEvent *event) Q_DECL_OVERRIDE;
	virtual void mousePressEvent(QMouseEvent* event) Q_DECL_OVERRIDE;
	virtual void mouseMoveEvent(QMouseEvent* event) Q_DECL_OVERRIDE;

private:
	ScoreAtom m_atomData;
	QColor m_colorAccent;
	QPoint m_clickLeftPos;
	QTime m_clickLeftTime;
};

#endif // SCOREATOMWIDGET_H
