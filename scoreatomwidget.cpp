/**
 * @file scoreatomwidget.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-21
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scoreatomwidget.h"

#include <QtCore/QMimeData>
#include <QtCore/QTime>

#include <QtGui/QContextMenuEvent>
#include <QtGui/QDrag>
#include <QtGui/QPainter>
#include <QtGui/QPainterPath>
#include <QtGui/QStyleHints>

#include <QtWidgets/QApplication>
#include <QtWidgets/QStyle>
#include <QtWidgets/QMenu>

#include "scoreatom.h"
#include "step.h"


const QColor DEFAULT_ACCENT_COLOR(QColor::fromHsv(230, 180, 150));

ScoreAtomWidget::ScoreAtomWidget(QWidget* parent):
	QWidget(parent),
	m_atomData(Q_NULLPTR),
	m_colorAccent(DEFAULT_ACCENT_COLOR),
	m_clickLeftPos(),
	m_clickLeftTime()
{
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

ScoreAtomWidget::ScoreAtomWidget(const ScoreAtom& atomData, QWidget* parent):
	QWidget(parent),
	m_atomData(atomData),
	m_colorAccent(DEFAULT_ACCENT_COLOR),
	m_clickLeftPos(),
	m_clickLeftTime()
{
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

QSize ScoreAtomWidget::minimumSizeHint() const
{
	int left, top, right, bottom; // marges
	getContentsMargins(&left, &top, &right, &bottom);
	QSize margins(left + right, top + bottom);
	return effectiveSize() + margins;
}

QSize ScoreAtomWidget::sizeHint() const
{
	return minimumSizeHint();
}

QSize ScoreAtomWidget::effectiveSize() const
{
	return nominalSize();
}

QSize ScoreAtomWidget::nominalSize()
{
	return QSize(26, 26);
}

void ScoreAtomWidget::paintEvent(QPaintEvent* event)
{
	QWidget::paintEvent(event);

	QPainter painter(this);

	QRect outer(contentsRect());
	QRect inner(outer.adjusted(1, 1, -1, -1));

	const Step* step(m_atomData.step());
	if(step)
	{
		const QPixmap& pixmap(step->image());
		if(m_atomData.haveAccent())
		{
			painter.setBrush(palette().highlight());
			painter.setPen(Qt::NoPen);
			painter.drawRect(outer);
		}
		else
		{
			painter.setBrush(palette().base());
			painter.setPen(Qt::NoPen);
			painter.drawRect(outer);
		}

		painter.drawPixmap(inner.topLeft()+step->imagePos(), pixmap);
	}
	else
	{
		painter.setBrush(Qt::NoBrush);
		painter.setPen(QPen(QBrush(Qt::black), 1., Qt::DotLine));
		painter.drawRect(outer.adjusted(0, 0, -1, -1));
	}
}

void ScoreAtomWidget::contextMenuEvent(QContextMenuEvent* event)
{
	QWidget::contextMenuEvent(event);
}

void ScoreAtomWidget::mousePressEvent(QMouseEvent* event)
{
	QWidget::mousePressEvent(event);
	/*if(event->button() == Qt::LeftButton)
	{
		m_clickLeftPos = event->pos();
		m_clickLeftTime.start();
	}*/
}

void ScoreAtomWidget::mouseMoveEvent(QMouseEvent* event)
{
	QWidget::mouseMoveEvent(event);
	/*if(event->buttons() & Qt::LeftButton)
	{
		QPoint dist(event->pos() - m_clickLeftPos);
		if(dist.manhattanLength() > QApplication::startDragDistance() ||
			m_clickLeftTime.elapsed() > QApplication::startDragTime())
		{
			QPixmap pixmap(contentsRect().size());
			render(&pixmap, QPoint(), QRegion(contentsRect()));

			QByteArray datas;
			QDataStream dataStream(&datas, QIODevice::WriteOnly);
			if(m_atomData.step())
				dataStream << m_atomData.step()->id();
			else
				dataStream << QStringLiteral("-1"); //! @todo METTRE UN PAS VIDE QUELQUE PART ET FAIRE EN SORTE QU'IL SOIT MODIFIABLE PAR LE XML

			QMimeData* mimeData(new QMimeData());
			mimeData->setData("application/x-atom", datas);
			mimeData->setImageData(pixmap);
			if(m_atomData.step() != Q_NULLPTR)
				mimeData->setText(m_atomData.step()->name());
			else
				mimeData->setText("Empty step");

			QDrag* drag(new QDrag(this));
			drag->setPixmap(pixmap);
			drag->setMimeData(mimeData);
			drag->exec(Qt::MoveAction | Qt::CopyAction, Qt::MoveAction);
		}
	}*/
}

QColor ScoreAtomWidget::colorAccent() const
{
	return m_colorAccent;
}

void ScoreAtomWidget::setColorAccent(const QColor& colorAccent)
{
	m_colorAccent = colorAccent;
}
