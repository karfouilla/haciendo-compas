#ifndef SCOREWRITER_H_INCLUDED
#define SCOREWRITER_H_INCLUDED
/**
 * @file scorewriter.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-04
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QIODevice>
#include <QtCore/QXmlStreamWriter>

#include "score.h"

class ScoreBeat;

class ScoreWriter
{
public:
	ScoreWriter(QIODevice* device);
	void write(const Score* score);

private:
	Q_DISABLE_COPY(ScoreWriter)

	void autoBreakLine(QString& dataString, const QByteArray& data64, int num);
	void writeResource(const Resource& resource);
	void writeSteps(const QMap<QString, Step*>& steps);
	void writeStep(const Step* step);
	void writeResourceName(const QString& name, const Resource::Name& resName);
	void writePaterns(const QMap<QString, Patern>& paterns);
	void writePatern(const Patern& patern);
	void writeAtom(const ScoreAtom& atom);
	void writeSection(const ScoreSection* section);
	void writeBeat(const ScoreBeat& beat);

private:
	QIODevice* m_pFile;
	QXmlStreamWriter m_writer;
};

#endif // SCOREWRITER_H_INCLUDED
