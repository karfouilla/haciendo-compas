/**
 * @file scoreindex.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-11
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scoreindex.h"

#include "scoreatom.h"
#include "scoresection.h"

ScoreIndex::ScoreIndex():
	m_pSection(Q_NULLPTR),
	m_idxPosition(0),
	m_subdiv()
{ }
ScoreIndex::ScoreIndex(const ScoreSection* pSection,
					   quint32 idxBeatPos,
					   Subdiv sub) Q_DECL_NOTHROW:
	m_pSection(pSection),
	m_idxPosition(idxBeatPos),
	m_subdiv(sub)
{ }

ScoreIndex::ScoreIndex(const ScoreIndex& other) Q_DECL_NOTHROW:
	m_pSection(other.m_pSection),
	m_idxPosition(other.m_idxPosition),
	m_subdiv(other.m_subdiv)
{ }
ScoreIndex& ScoreIndex::operator=(const ScoreIndex& other) Q_DECL_NOTHROW
{
	m_pSection = other.m_pSection;
	m_idxPosition = other.m_idxPosition;
	m_subdiv = other.m_subdiv;
	return *this;
}

const ScoreSection* ScoreIndex::section() const Q_DECL_NOTHROW
{
	return m_pSection;
}
quint32 ScoreIndex::beatIndex() const Q_DECL_NOTHROW
{
	return m_idxPosition;
}
Subdiv ScoreIndex::subdiv() const Q_DECL_NOTHROW
{
	return m_subdiv;
}

ScoreBeatListCIt ScoreIndex::beatCIt() const Q_DECL_NOTHROW
{
	return m_pSection->citerOfBeatIndex(m_idxPosition);
}
ScoreAtomListCIt ScoreIndex::atomCIt() const Q_DECL_NOTHROW
{
	return beat().citerOfSubdiv(m_subdiv);
}

const ScoreBeat& ScoreIndex::beat() const Q_DECL_NOTHROW
{
	return m_pSection->beatAt(m_idxPosition);
}
const ScoreAtom& ScoreIndex::atom() const Q_DECL_NOTHROW
{
	return beat().cvalueOfSubdiv(m_subdiv);
}

ScoreIndex ScoreIndex::advance(Subdiv sub) const Q_DECL_NOTHROW
{
	quint32 nextPos(m_idxPosition);
	Subdiv nextSubdiv(m_subdiv);
	if(!nextSubdiv.isValid())
		nextSubdiv = Subdiv::ZERO;
	nextSubdiv = nextSubdiv+sub;

	if(nextSubdiv.num() >= nextSubdiv.den()) // nextSubdiv >= 1
	{
		quint32 nbBeats(nextSubdiv.num()/nextSubdiv.den());
		nextSubdiv.setNum(nextSubdiv.num()%nextSubdiv.den());
		nextPos += nbBeats;
	}
	return ScoreIndex(m_pSection, nextPos, nextSubdiv);
}

ScoreIndex ScoreIndex::advance(ScoreIndex::AdvanceMode to) const Q_DECL_NOTHROW
{
	switch (to)
	{
	case AM_CURRENT_BEAT:
		return ScoreIndex(m_pSection, m_idxPosition, Subdiv::INVALID);

	case AM_FIRST_ATOM:
	{
		Subdiv sub(0, beat().usize());
		return ScoreIndex(m_pSection, m_idxPosition, sub);
	}

	case AM_LAST_ATOM:
	{
		quint32 beatSize(beat().usize());
		Subdiv sub;
		if(beatSize > 0)
			sub = Subdiv(beatSize-1, beatSize);
		else
			sub = Subdiv::INVALID;

		return ScoreIndex(m_pSection, m_idxPosition, sub);
	}

	case AM_END_ATOM:
	{
		quint32 beatSize(beat().usize());
		Subdiv sub(beatSize, beatSize);
		return ScoreIndex(m_pSection, m_idxPosition, sub);
	}

	case AM_NEXT_ATOM:
	{
		return ScoreIndex(m_pSection, m_idxPosition, m_subdiv + beat().unit());
	}

	case AM_PREV_ATOM:
	{
		return ScoreIndex(m_pSection, m_idxPosition, m_subdiv - beat().unit());
	}

	case AM_NEXT_BEAT:
		return ScoreIndex(m_pSection, m_idxPosition+1);

	case AM_PREV_BEAT:
		return ScoreIndex(m_pSection, m_idxPosition-1);
	}
}

bool ScoreIndex::isBeginOfBeat() const Q_DECL_NOTHROW
{
	return (m_subdiv.num() <= 0);
}
bool ScoreIndex::isLastOfBeat() const Q_DECL_NOTHROW
{
	if(m_subdiv.den() > 0)
		return (m_subdiv.num()+1 == m_subdiv.den());
	else
		return true;
}
bool ScoreIndex::isEndOfBeat() const Q_DECL_NOTHROW
{
	return (m_subdiv.num() >= m_subdiv.den());
}

bool ScoreIndex::isBegin() const Q_DECL_NOTHROW
{
	return (m_idxPosition == 0);
}
bool ScoreIndex::isEnd() const Q_DECL_NOTHROW
{
	return (m_idxPosition >= m_pSection->beatCount());
}

void ScoreIndex::normalize(bool letInvalid) Q_DECL_NOTHROW
{
	if(letInvalid)
	{
		if(m_subdiv.isValid())
		{
			if(m_subdiv.isOne())
			{
				++m_idxPosition;
				m_subdiv = Subdiv::ZERO;
			}
		}
	}
	else
	{
		if(!m_subdiv.isValid())
			m_subdiv = Subdiv::ZERO;
		else if(m_subdiv.isOne())
		{
			++m_idxPosition;
			m_subdiv = Subdiv::ZERO;
		}
	}
}

bool ScoreIndex::operator==(const ScoreIndex& other) const Q_DECL_NOTHROW
{
	return (m_pSection == other.m_pSection &&
			m_idxPosition == other.m_idxPosition &&
			m_subdiv.simplified() == other.m_subdiv.simplified());
}
bool ScoreIndex::operator!=(const ScoreIndex& other) const Q_DECL_NOTHROW
{
	return !operator==(other);
}

bool ScoreIndex::operator<(const ScoreIndex& other) const Q_DECL_NOTHROW
{
	Q_ASSERT(m_pSection == other.m_pSection);
	if(m_idxPosition < other.m_idxPosition) // yes, sure
		return true;

	if(m_idxPosition > other.m_idxPosition) // no, sure
		return false;

	// == : not sure
	Subdiv sub(subdiv());
	Subdiv othersub(other.subdiv());
	if(!sub.isValid() || !othersub.isValid())
		return othersub.isValid(); // !isValid < isValid (table de vérité)

	quint32 com(Subdiv::common(sub, othersub));
	return (sub.convertTo(com) < othersub.convertTo(com));
}

bool ScoreIndex::operator>(const ScoreIndex& other) const Q_DECL_NOTHROW
{
	Q_ASSERT(m_pSection == other.m_pSection);
	if(m_idxPosition > other.m_idxPosition) // yes, sure
		return true;

	if(m_idxPosition < other.m_idxPosition) // no, sure
		return false;

	// == : not sure
	Subdiv sub(subdiv());
	Subdiv othersub(other.subdiv());
	if(!sub.isValid() || !othersub.isValid())
		return sub.isValid(); // isValid > !isValid (table de vérité)

	quint32 com(Subdiv::common(sub, othersub));
	return (sub.convertTo(com) > othersub.convertTo(com));
}

bool ScoreIndex::operator<=(const ScoreIndex& other) const Q_DECL_NOTHROW
{
	return (operator<(other) || operator==(other));
}

bool ScoreIndex::operator>=(const ScoreIndex& other) const Q_DECL_NOTHROW
{
	return (operator>(other) || operator==(other));
}
