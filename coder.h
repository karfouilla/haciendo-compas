#ifndef CODER_H_INCLUDED
#define CODER_H_INCLUDED
/**
 * @file coder.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-12
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////


#include <QtCore/QByteArray>
#include <QtCore/QIODevice>
#include <QtCore/QProcess>
#include <QtMultimedia/QAudioFormat>

namespace Coder
{
const int CHUNK_SIZE(16384);

QProcess* ffmpeg(const QStringList& args);
bool sendData(QProcess* process, QIODevice* input);
bool receiveData(QProcess* process, QByteArray* output);
bool encode(QIODevice* input, QAudioFormat format, const QString& output);
bool decode(QIODevice* input, QAudioFormat format, QByteArray* output);
}

#endif // CODER_H_INCLUDED
