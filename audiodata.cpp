/**
 * @file audiodata.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-06
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "audiodata.h"

#include <QtCore/QProcess>

#include "coder.h"

AudioData::AudioData():
	m_format(),
	m_data()
{ }

bool AudioData::load(QIODevice* file)
{
	return Coder::decode(file, m_format, &m_data);
}

const QByteArray& AudioData::cdata() const
{
	return m_data;
}
QByteArray& AudioData::data()
{
	return m_data;
}

const QAudioFormat& AudioData::cformat() const
{
	return m_format;
}
QAudioFormat& AudioData::format()
{
	return m_format;
}
void AudioData::setFormat(const QAudioFormat& format)
{
	m_format = format;
}
