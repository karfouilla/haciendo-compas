#ifndef RESOURCE_H_INCLUDED
#define RESOURCE_H_INCLUDED
/**
 * @file resource.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-08
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QByteArray>
#include <QtCore/QMap>
#include <QtCore/QString>

QT_BEGIN_NAMESPACE
class QIODevice;
QT_END_NAMESPACE

class Resource
{
public:
	enum Type {
		TYPE_FILE,
		TYPE_XML
	};

	class Name
	{
	public:
		Name() Q_DECL_NOTHROW;
		Name(const QString& szFileName, Type eType = TYPE_FILE) Q_DECL_NOTHROW;
		Name(const Name& other) Q_DECL_NOTHROW;
		Name(Name&& other) Q_DECL_NOTHROW;
		Name& operator=(const Name& other) Q_DECL_NOTHROW;
		Name& operator=(Name&& other) Q_DECL_NOTHROW;

		const QString& filename() const Q_DECL_NOTHROW;
		Type type() const Q_DECL_NOTHROW;
		void setFilename(const QString& szFilename) Q_DECL_NOTHROW;
		void setType(Type eType) Q_DECL_NOTHROW;

	private:
		QString m_szFileName;
		Type m_eType;
	};

	Resource() Q_DECL_NOTHROW;
	Resource(const Resource& other);
	Resource(Resource&& other) Q_DECL_NOTHROW;
	Resource& operator=(const Resource& other);
	Resource& operator=(Resource&& other) Q_DECL_NOTHROW;

	void addEntry(const QString& szName, const QByteArray& datas);
	QIODevice* open(const Name& name);
	QIODevice* open(const QString& szFilename, Type eType = TYPE_FILE);

	const QMap<QString, QByteArray>& datas() const;

private:
	QMap<QString, QByteArray> m_mapXMLDatas;
};

#endif // RESOURCE_H_INCLUDED
