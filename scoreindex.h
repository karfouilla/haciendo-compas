#ifndef SCOREINDEX_H_INCLUDED
#define SCOREINDEX_H_INCLUDED
/**
 * @file scoreindex.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-11
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scorebeat.h"
#include "subdiv.h"

class ScoreSection;

class ScoreIndex
{
public:
	enum AdvanceMode {
		AM_CURRENT_BEAT, // select current beat (if atom selected)
		AM_FIRST_ATOM, // select first atom of beat (use if beat are selected)
		AM_LAST_ATOM, // select last atom of beat (use if beat are selected)
		AM_END_ATOM, // select end of beat (use if beat are selected)

		AM_NEXT_ATOM,
		AM_PREV_ATOM,
		AM_NEXT_BEAT,
		AM_PREV_BEAT
	};

public:
	ScoreIndex();
	ScoreIndex(const ScoreSection* pSection, quint32 idxBeatPos,
			   Subdiv sub = Subdiv()) Q_DECL_NOTHROW;
	ScoreIndex(const ScoreIndex& other) Q_DECL_NOTHROW;
	ScoreIndex& operator=(const ScoreIndex& other) Q_DECL_NOTHROW;

	const ScoreSection* section() const Q_DECL_NOTHROW;
	quint32 beatIndex() const Q_DECL_NOTHROW;
	Subdiv subdiv() const Q_DECL_NOTHROW;

	ScoreBeatListCIt beatCIt() const Q_DECL_NOTHROW;
	ScoreAtomListCIt atomCIt() const Q_DECL_NOTHROW;

	const ScoreBeat& beat() const Q_DECL_NOTHROW;
	const ScoreAtom& atom() const Q_DECL_NOTHROW;

	ScoreIndex advance(Subdiv sub) const Q_DECL_NOTHROW;
	ScoreIndex advance(AdvanceMode to) const Q_DECL_NOTHROW;
	bool isBeginOfBeat() const Q_DECL_NOTHROW;
	bool isLastOfBeat() const Q_DECL_NOTHROW;
	bool isEndOfBeat() const Q_DECL_NOTHROW;
	bool isBegin() const Q_DECL_NOTHROW;
	bool isEnd() const Q_DECL_NOTHROW;

	void normalize(bool letInvalid = true) Q_DECL_NOTHROW;

	bool operator==(const ScoreIndex& other) const Q_DECL_NOTHROW;
	bool operator!=(const ScoreIndex& other) const Q_DECL_NOTHROW;
	bool operator<(const ScoreIndex& other) const Q_DECL_NOTHROW;
	bool operator>(const ScoreIndex& other) const Q_DECL_NOTHROW;
	bool operator<=(const ScoreIndex& other) const Q_DECL_NOTHROW;
	bool operator>=(const ScoreIndex& other) const Q_DECL_NOTHROW;

private:
	const ScoreSection* m_pSection;
	quint32 m_idxPosition;
	Subdiv m_subdiv;
};

#endif // SCOREINDEX_H_INCLUDED
