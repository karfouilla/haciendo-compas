/**
 * @file subdiv.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-09
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "subdiv.h"

#include <QLinkedList>

const Subdiv Subdiv::INVALID(0, 0);
const Subdiv Subdiv::ZERO(0, 1);
const Subdiv Subdiv::ONE(1, 1);

static quint32 pgcd(quint32 first, quint32 second)
{
	// cas d'arrêt
	if(second == 0)
		return first;

	return pgcd(second, first%second);
}
static quint32 ppcm(quint32 first, quint32 second)
{
	return ((first*second) / pgcd(first, second));
}

Subdiv::Subdiv() Q_DECL_NOTHROW:
	m_uNum(0),
	m_uDen(0)
{ }
Subdiv::Subdiv(quint32 uNum, quint32 uDen) Q_DECL_NOTHROW:
	m_uNum(uNum),
	m_uDen(uDen)
{ }

Subdiv::Subdiv(const Subdiv& other) Q_DECL_NOTHROW:
	m_uNum(other.m_uNum),
	m_uDen(other.m_uDen)
{ }
Subdiv& Subdiv::operator=(const Subdiv& other) Q_DECL_NOTHROW
{
	m_uNum = other.m_uNum;
	m_uDen = other.m_uDen;
	return *this;
}

quint32 Subdiv::num() const Q_DECL_NOTHROW
{
	return m_uNum;
}

quint32 Subdiv::den() const Q_DECL_NOTHROW
{
	return m_uDen;
}

void Subdiv::setNum(quint32 uNum) Q_DECL_NOTHROW
{
	m_uNum = uNum;
}
void Subdiv::setDen(quint32 uDen) Q_DECL_NOTHROW
{
	m_uDen = uDen;
}

bool Subdiv::isNull() const Q_DECL_NOTHROW
{
	return (m_uNum == 0);
}

bool Subdiv::isOne() const Q_DECL_NOTHROW
{
	return (m_uNum == m_uDen);
}
bool Subdiv::isValid() const Q_DECL_NOTHROW
{
	return (m_uDen != 0);
}

bool Subdiv::isConvertible(quint32 uDen) const Q_DECL_NOTHROW
{
	return ((m_uNum*uDen) % m_uDen == 0);
}
quint32 Subdiv::convertTo(quint32 uDen) const Q_DECL_NOTHROW
{
	return ((m_uNum*uDen) / m_uDen);
}

void Subdiv::simplify() Q_DECL_NOTHROW
{
	if(isValid())
	{
		quint32 factor = pgcd(m_uDen, m_uNum);
		m_uNum /= factor;
		m_uDen /= factor;
	}
}
Subdiv Subdiv::simplified() const Q_DECL_NOTHROW
{
	Subdiv other(*this);
	other.simplify();
	return other;
}

bool Subdiv::operator==(const Subdiv& other) const Q_DECL_NOTHROW
{
	return (m_uNum == other.m_uNum && m_uDen == other.m_uDen);
}

Subdiv Subdiv::operator+(const Subdiv& other) const Q_DECL_NOTHROW
{
	quint32 resDen(common(*this, other));
	return Subdiv(convertTo(resDen) + other.convertTo(resDen), resDen);
}
Subdiv Subdiv::operator-(const Subdiv& other) const Q_DECL_NOTHROW
{
	quint32 resDen(common(*this, other));
	return Subdiv(convertTo(resDen) - other.convertTo(resDen), resDen);
}
Subdiv Subdiv::operator*(const Subdiv& other) const Q_DECL_NOTHROW
{
	return Subdiv(num()*other.num(), den()*other.den());
}

// static
quint32 Subdiv::common(const Subdiv& first, const Subdiv& second) Q_DECL_NOTHROW
{
	return ppcm(first.den(), second.den());
}

quint32 Subdiv::common(const QLinkedList<Subdiv>& list) Q_DECL_NOTHROW
{
	QLinkedList<Subdiv>::ConstIterator cit(list.cbegin());
	Subdiv last(1, cit->den());
	++cit;

	while(cit != list.cend())
	{
		last.setDen(common(last, *cit));
		++cit;
	}
	return last.den();
}
