#ifndef SCOREATOM_H_INCLUDED
#define SCOREATOM_H_INCLUDED
/**
 * @file scoreatom.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QLinkedList>

class ScoreGroup;

class Step;

class ScoreAtom
{
public:
	ScoreAtom();
	ScoreAtom(const ScoreAtom& other);
	ScoreAtom(ScoreAtom&& other);
	ScoreAtom(Step* pStep, bool accent = false);
	ScoreAtom(Step* pStep, bool accent, int iTempo, float fVolume);
	virtual ~ScoreAtom();
	ScoreAtom& operator=(const ScoreAtom& other);
	ScoreAtom& operator=(ScoreAtom&& other);

	const Step* step() const Q_DECL_NOTHROW;
	const int* tempo() const Q_DECL_NOTHROW;
	const float* volume() const Q_DECL_NOTHROW;
	bool haveAccent() const Q_DECL_NOTHROW;
	QLinkedList<ScoreGroup>::Iterator* group() Q_DECL_NOTHROW;

	void setStep(Step* pStep) Q_DECL_NOTHROW;
	void setTempo(int iTempo) Q_DECL_NOTHROW;
	void setDefaultTempo() Q_DECL_NOTHROW;
	void setVolume(float fVolume) Q_DECL_NOTHROW;
	void setDefaultVolume() Q_DECL_NOTHROW;
	void setHaveAccent(bool accent) Q_DECL_NOTHROW;
	void setGroup(QLinkedList<ScoreGroup>::Iterator itGroup) Q_DECL_NOTHROW;
	void setNoGroup() Q_DECL_NOTHROW;

private:
	Step* m_pStep;
	int* m_iTempo;
	float* m_fVolume;
	bool m_haveAccent;
	QLinkedList<ScoreGroup>::Iterator* m_pGroup;
};

#endif // SCOREATOM_H_INCLUDED
