/**
 * @file audiosource.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-06
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "audiosource.h"

#include <QtCore/QtMath>

static inline void fromMonoI16LE(const char* data, float& left, float& right)
{
	const qint16* data16i(reinterpret_cast<const qint16*>(data));
	left = static_cast<float>(data16i[0]) / 32768.f;
	right = static_cast<float>(data16i[0]) / 32768.f;
}
static inline void fromStereoI16LE(const char* data, float& left, float& right)
{
	const qint16* data16i(reinterpret_cast<const qint16*>(data));
	left = static_cast<float>(data16i[0]) / 32768.f;
	right = static_cast<float>(data16i[1]) / 32768.f;
}
static inline void toStereoI16LE(char* data, float left, float right)
{
	qint16* data16i(reinterpret_cast<qint16*>(data));
	data16i[0] = static_cast<qint16>(left * 32767.f);
	data16i[1] = static_cast<qint16>(right * 32767.f);
}
static inline void toMonoI16LE(char* data, float left, float right)
{
	qint16* data16i(reinterpret_cast<qint16*>(data));
	data16i[0] = static_cast<qint16>((left+right)/2.f * 32767.f);
}

AudioSource::AudioSource(const AudioData* pData):
	m_pData(pData),
	m_fVolume(1.f),
	m_fDirection(float(M_PI_2)),
	m_fLeftVolume(1.),
	m_fRightVolume(1.),
	m_sampleBegin(0)
{ }

void AudioSource::setSampleBegin(qint32 _sampleBegin)
{
	m_sampleBegin = _sampleBegin;
}

qint32 AudioSource::sampleBegin() const
{
	return m_sampleBegin;
}
qint32 AudioSource::sampleEnd() const
{
	int dataLen(m_pData->cdata().size());
	return (m_sampleBegin + m_pData->cformat().framesForBytes(dataLen));
}

void AudioSource::read(char* dstData, QAudioFormat dstFormat, qint32 dstPos,
					   qint32 srcPos, qint32 samplesCount) const
{
	const char* srcData(m_pData->cdata().constData());
	srcData += m_pData->cformat().bytesForFrames(srcPos);
	dstData += dstFormat.bytesForFrames(dstPos);
	for(int i=0; i<samplesCount; ++i)
	{
		float dstLeft, dstRight;
		float srcLeft, srcRight;

		// float conversion
		if(dstFormat.channelCount() == 2)
			fromStereoI16LE(dstData, dstLeft, dstRight);
		else
			fromMonoI16LE(dstData, dstLeft, dstRight);

		if(m_pData->cformat().channelCount() == 2)
			fromStereoI16LE(srcData, srcLeft, srcRight);
		else
			fromMonoI16LE(srcData, srcLeft, srcRight);

		// effect (volume and direction)
		srcLeft *= m_fVolume * m_fLeftVolume;
		srcRight *= m_fVolume * m_fRightVolume;

		// merge (blend mode add)
		dstLeft += srcLeft;
		dstRight += srcRight;

		if(dstFormat.channelCount() == 2)
			toStereoI16LE(dstData, dstLeft, dstRight);
		else
			toMonoI16LE(dstData, dstLeft, dstRight);

		srcData += m_pData->cformat().bytesPerFrame();
		dstData += dstFormat.bytesPerFrame();
	}
}

float AudioSource::volume() const
{
	return m_fVolume;
}

void AudioSource::setVolume(float fVolume)
{
	m_fVolume = fVolume;
}

float AudioSource::direction() const
{
	return m_fDirection;
}

void AudioSource::setDirection(float fDirection)
{
	m_fDirection = fDirection;

	float cosDir(static_cast<float>(qCos(m_fDirection)));
	float rightTmp((cosDir+1.f) / 2.f);
	float leftTmp = 1.f - rightTmp;

	float norm(qMax(leftTmp, rightTmp));

	m_fLeftVolume = leftTmp/norm;
	m_fRightVolume = rightTmp/norm;
}
