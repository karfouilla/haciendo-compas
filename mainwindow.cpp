/**
 * @file mainwindow.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-08
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "mainwindow.h"

#include "regularlayout.h"
#include "step.h"
#include "scoreatom.h"
#include "scoreatomwidget.h"
#include "scorelines.h"
#include "scoresection.h"
#include "scorereader.h"
#include "scorewriter.h"
#include "audiogenerator.h"
#include "audiosource.h"
#include "audiodata.h"
#include "scoreaudiogenerator.h"
#include "paternlistwidget.h"
#include "dockscore.h"

#include <QtCore/QDir>
#include <QtCore/QSaveFile>
#include <QtCore/QtMath>

#include <QtGui/QPainter>

#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QInputDialog>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QToolBar>

#include <QtMultimedia/QAudioOutput>

#include <QDebug>

#include "coder.h"

MainWindow::MainWindow(QString filename, QWidget* parent):
	QMainWindow(parent),
	m_pScore(Q_NULLPTR),
	m_pClipboard(Q_NULLPTR),
	m_wdgCentral(Q_NULLPTR),
	m_wdgScore(Q_NULLPTR),
	m_pGenerator(Q_NULLPTR),
	m_pOutput(Q_NULLPTR),
	m_szFilename(filename),
	m_aNew(Q_NULLPTR),
	m_aOpen(Q_NULLPTR),
	m_aSave(Q_NULLPTR),
	m_aSaveAs(Q_NULLPTR),
	m_aExportAudio(Q_NULLPTR),
	m_aQuit(Q_NULLPTR),
	m_aCopy(Q_NULLPTR),
	m_aCut(Q_NULLPTR),
	m_aPaste(Q_NULLPTR),
	m_lstAddStep(),
	m_lstAddPatern(),
	m_aSimplify(Q_NULLPTR),
	m_aSetDivision(Q_NULLPTR),
	m_aSetTempo(Q_NULLPTR),
	m_aSetVolume(Q_NULLPTR),
	m_aToggleAccent(Q_NULLPTR),
	m_aSetBeatPerLines(Q_NULLPTR),
	m_aPlay(Q_NULLPTR),
	m_aStop(Q_NULLPTR),
	m_aRepeat(Q_NULLPTR),
	m_menuFile(Q_NULLPTR),
	m_menuExport(Q_NULLPTR),
	m_menuEdit(Q_NULLPTR),
	m_menuAddStep(Q_NULLPTR),
	m_menuAddPatern(Q_NULLPTR),
	m_menuCursor(Q_NULLPTR),
	m_menuDisplay(Q_NULLPTR),
	m_menuPlay(Q_NULLPTR),
	m_tbFile(Q_NULLPTR),
	m_tbEdit(Q_NULLPTR),
	m_tbPlay(Q_NULLPTR),
	m_dockPaterns(Q_NULLPTR),
	m_dockScore(Q_NULLPTR),
	m_wdgPaternList(Q_NULLPTR),
	m_wdgOptionScore(Q_NULLPTR)
{
	setAttribute(Qt::WA_DeleteOnClose);

	setWindowIcon(QIcon(":/data/haciendo-compas.svg"));
	setWindowTitle(tr("Haciendo Compas"));

	m_pScore = new Score;
	loadFile(); // load default template
	loadStepsRes();

	// initCentralWidget
	m_wdgScore = new ScoreWidget;
	m_wdgScore->setModel(m_pScore->data());
	m_wdgScore->setBeatPerLines(8);

	m_wdgCentral = new QScrollArea(this);
	m_wdgCentral->setWidget(m_wdgScore);
	m_wdgCentral->setWidgetResizable(true);
	m_wdgCentral->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	m_wdgCentral->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	m_wdgCentral->setMinimumSize(480, 120);

	initActions();
	initMenu();
	initToolBars();
	initDocks();

	QAudioFormat format;
	format.setCodec("audio/pcm");
	format.setChannelCount(2);
	format.setSampleRate(44100);
	format.setSampleSize(16);
	format.setSampleType(QAudioFormat::SignedInt);
	format.setByteOrder(QAudioFormat::LittleEndian);

	m_pOutput = new QAudioOutput(format, this);

	connect(m_pOutput, &QAudioOutput::stateChanged,
			this, &MainWindow::playStateChange);

	connect(m_pScore->data(), &ScoreSection::changed,
			m_wdgScore, &ScoreWidget::updateCurrentModel);


	setCentralWidget(m_wdgCentral);
}

MainWindow::~MainWindow()
{
	delete m_pScore;
}

void MainWindow::dialogSetDivision()
{
	bool ok(false);
	int div(static_cast<int>(m_pScore->defaultDivision()));
	div = QInputDialog::getInt(this, tr("Beat division"),
							   tr("Default beat division"),
							   div, 1, 48, 1, &ok);
	if(ok)
	{
		m_pScore->setDefaultDivision(static_cast<quint32>(div));
	}
}

void MainWindow::dialogSetTempo()
{
	if(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT).isEnd())
	{
		QMessageBox::information(this, tr("Invalid cursor position"),
								 tr("Cursor don't point out to step"));
	}
	else
	{
		ScoreAtom atom(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT).atom());
		bool ok(false);
		int tempo(80);
		if(atom.tempo())
			tempo = *atom.tempo();
		tempo = QInputDialog::getInt(this, tr("Tempo"),
									 tr("Change tempo from cursor"),
									 tempo, 30, 240, 1, &ok);
		if(ok)
		{
			atom.setTempo(tempo);
			m_pScore->data()->emplace(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT), atom);
		}
	}
}

void MainWindow::dialogSetVolume()
{
	if(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT).isEnd())
	{
		QMessageBox::information(this, tr("Invalid cursor position"),
								 tr("Cursor don't point out to step"));
	}
	else
	{
		ScoreAtom atom(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT).atom());
		bool ok(false);
		float volume(1.f);
		if(atom.volume())
			volume = *atom.volume();
		double volume100(QInputDialog::getDouble(this, tr("Volume"),
												 tr("Change volume from cursor"),
												 100.*volume, 0, 200, 2, &ok));
		volume = static_cast<float>(volume100/100.);

		if(ok)
		{
			atom.setVolume(volume);
			m_pScore->data()->emplace(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT), atom);
		}
	}
}

void MainWindow::dialogSetBeatPerLines()
{
	bool ok(false);
	int beatPerLines(m_wdgScore->beatPerLines());
	beatPerLines = QInputDialog::getInt(this, tr("Beat per lines"),
										tr("Number of beat per lines"),
										beatPerLines, 1, 12, 1, &ok);
	if(ok)
	{
		m_wdgScore->setBeatPerLines(beatPerLines);
	}
}

void MainWindow::dialogNewFile()
{
	MainWindow* next(new MainWindow());
	next->show();
}

void MainWindow::dialogOpenFile()
{
	QString dir;
	QString filter(tr("XML Score Flamenco (*.fls);;XML (*.xml)"));

	if(!m_szFilename.isEmpty())
		dir = QFileInfo(m_szFilename).path();

	QFileDialog dialog(this, tr("Open score"), dir, filter);
	dialog.setAcceptMode(QFileDialog::AcceptOpen);
	dialog.setFileMode(QFileDialog::AnyFile);
	if(dialog.exec())
	{
		QStringList fileList(dialog.selectedFiles());
		if(!fileList.isEmpty())
		{
			MainWindow* next(new MainWindow(fileList.first()));
			next->show();
		}
	}
}

bool MainWindow::askFilenameSave()
{
	QString dir;
	QString filter(tr("XML Score Flamenco (*.fls);;XML (*.xml)"));

	if(!m_szFilename.isEmpty())
		dir = QFileInfo(m_szFilename).path();

	QFileDialog dialog(this, tr("Save score"), dir, filter);
	dialog.setAcceptMode(QFileDialog::AcceptSave);
	dialog.setFileMode(QFileDialog::AnyFile);
	if(dialog.exec())
	{
		QStringList fileList(dialog.selectedFiles());
		if(!fileList.isEmpty()) // assert
			m_szFilename = fileList.first();
		return true;
	}
	else
	{
		return false;
	}
}

bool MainWindow::askFilenameExport(QString& filename)
{
	QString dir;
	QString filter(tr("Waveform file (*.wav);;"
					  "Free Lossless Audio Codec file (*.flac);;"
					  "Ogg vorbis (*.ogg);;"
					  "MPEG-1/2 Audio Layer III file (*.mp3)"));

	if(!m_szFilename.isEmpty())
		dir = QFileInfo(m_szFilename).path();

	QFileDialog dialog(this, tr("Export score"), dir, filter);
	dialog.setAcceptMode(QFileDialog::AcceptSave);
	dialog.setFileMode(QFileDialog::AnyFile);
	if(dialog.exec())
	{
		QStringList fileList(dialog.selectedFiles());
		if(!fileList.isEmpty()) // assert
			filename = fileList.first();
		return true;
	}
	else
	{
		return false;
	}
}

void MainWindow::loadFile()
{
	QFile in;
	if(m_szFilename.isEmpty()) // load default template
		in.setFileName(QStringLiteral(":/data/score/template.fls"));
	else
		in.setFileName(m_szFilename);

	if(in.open(QIODevice::ReadOnly))
	{
		ScoreReader reader(&in);
		if(reader.read(m_pScore))
		{
			in.close();
		}
		else
		{
			QString msg(tr("Error while reading file"));
			QMessageBox::critical(this, tr("Reading file error"), msg);
		}
	}
	else
	{
		QString msg(tr("Error while opening file: %1")
					.arg(in.errorString()));

		QMessageBox::critical(this, tr("Opening file error"), msg);
	}
}

void MainWindow::saveFile()
{
	QSaveFile out(m_szFilename);
	if(out.open(QIODevice::WriteOnly))
	{
		ScoreWriter writer(&out);
		writer.write(m_pScore);
	}
	if(!out.commit())
	{
		QString msg(tr("Error while saving file: %1")
					.arg(out.errorString()));

		QMessageBox::critical(this, tr("Saving file error"), msg);
	}
}

void MainWindow::dialogSaveAsFile()
{
	if(askFilenameSave())
		saveFile();
}

void MainWindow::dialogSaveFile()
{
	if(m_szFilename.isEmpty())
		dialogSaveAsFile();
	else
		saveFile();
}

void MainWindow::dialogExportAudio()
{
	QString filename;
	if(askFilenameExport(filename))
	{
		ScoreAudioGenerator* outGenerator(new ScoreAudioGenerator(m_pOutput->format(), this));
		outGenerator->setScore(m_pScore);
		outGenerator->open(QIODevice::ReadOnly);
		outGenerator->setRepeat(1);

		if(!Coder::encode(outGenerator, outGenerator->format(), filename))
		{
			QString msg(tr("Error while exporting file"));

			QMessageBox::critical(this, tr("Exporting file error"), msg);
		}

		delete outGenerator;
	}
}

void MainWindow::togglePlay()
{
	if(m_pOutput->state() == QAudio::SuspendedState)
	{
		m_pOutput->resume();
	}
	else if(m_pOutput->state() == QAudio::ActiveState)
	{
		m_pOutput->suspend();
	}
	else if(m_pOutput->state() == QAudio::StoppedState)
	{
		if(m_pGenerator)
			delete m_pGenerator;

		m_pGenerator = new ScoreAudioGenerator(m_pOutput->format(), this);
		m_pGenerator->setScore(m_pScore, m_wdgScore->scoreCursor(ScoreWidget::CURSOR_PLAY));
		m_pGenerator->open(QIODevice::ReadOnly);
		m_pGenerator->setRepeat((m_aRepeat->isChecked()) ? -1 : 1);

		m_pOutput->start(m_pGenerator);
	}
	//! Chose à faires:
	//! @todo Ajouter une bar de lecture #GRAPHIQUE
}

void MainWindow::stopPlaying()
{
	if(m_pGenerator)
	{
		m_pOutput->stop();
		delete m_pGenerator;
		m_pGenerator = Q_NULLPTR;
	}
}

void MainWindow::playStateChange(QAudio::State state)
{
	if(state == QAudio::ActiveState)
	{
		m_aPlay->setText(tr("Pause"));
		m_aPlay->setIcon(QIcon::fromTheme("media-playback-pause", QIcon(":/data/icon/media-playback-pause")));
	}
	else
	{
		m_aPlay->setText(tr("Play"));
		m_aPlay->setIcon(QIcon::fromTheme("media-playback-start", QIcon(":/data/icon/media-playback-start")));
	}
	if(m_pGenerator)
	{
		if(m_pGenerator->atEnd())
		{
			m_pOutput->stop();
			delete m_pGenerator;
			m_pGenerator = Q_NULLPTR;
		}
	}
}

void MainWindow::copySelection()
{
	ScoreGroup selection(m_wdgScore->selection());
	if(!selection.isEmpty())
	{
		if(m_pClipboard)
			delete m_pClipboard;

		m_pClipboard = m_pScore->data()->peek(selection);
	}
}

void MainWindow::cutSelection()
{
	ScoreGroup selection(m_wdgScore->selection());
	if(selection.begin() != selection.end()) //!< @todo METTRE UNE FONCTION DANS ScoreGroup POUR ÇA
	{
		if(m_pClipboard)
			delete m_pClipboard;
		//! @bug Fix bug, set cursor pos
		m_pClipboard = m_pScore->data()->take(selection);
		ScoreIndex pos = m_pScore->data()->lastOp().end();
		m_wdgScore->setScoreCursor(ScoreWidget::CURSOR_DEFAULT, pos); //!< @todo à vérifier pour le replacement avec plusieurs curseurs
	}
}

void MainWindow::pasteSelection()
{
	if(m_pClipboard)
	{
		//! @bug Fix bug, set cursor pos
		m_pScore->data()->emplace(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT), *m_pClipboard);
		ScoreIndex pos = m_pScore->data()->lastOp().end();
		m_wdgScore->setScoreCursor(ScoreWidget::CURSOR_DEFAULT, pos); //!< @todo à vérifier pour le replacement avec plusieurs curseurs
	}
}

void MainWindow::loadStepsRes()
{
	foreach(Step* step, m_pScore->steps())
	{
		if(!step->loadImage(&m_pScore->resource()))
		{
			qWarning() << "Error loading step image of"
					   << step->name() << "id=" << step->id();
		}
		if(!step->loadAudio(&m_pScore->resource()))
		{
			qWarning() << "Error loading step audio of"
					   << step->name() << "id=" << step->id();
		}
	}
}

void MainWindow::initActions()
{
	// File menu
	m_aNew = new QAction(tr("New"), this);
	m_aNew->setShortcut(QKeySequence::New);
	m_aNew->setIcon(QIcon::fromTheme("document-new", QIcon(":/data/icon/document-new")));
	connect(m_aNew, &QAction::triggered, this, &MainWindow::dialogNewFile);

	m_aOpen = new QAction(tr("Open"), this);
	m_aOpen->setShortcut(QKeySequence::Open);
	m_aOpen->setIcon(QIcon::fromTheme("document-open", QIcon(":/data/icon/document-open")));
	connect(m_aOpen, &QAction::triggered, this, &MainWindow::dialogOpenFile);

	m_aSave = new QAction(tr("Save"), this);
	m_aSave->setShortcut(QKeySequence::Save);
	m_aSave->setIcon(QIcon::fromTheme("document-save", QIcon(":/data/icon/document-save")));
	connect(m_aSave, &QAction::triggered, this, &MainWindow::dialogSaveFile);

	m_aSaveAs = new QAction(tr("Save as..."), this);
	m_aSaveAs->setShortcut(QKeySequence::SaveAs);
	m_aSaveAs->setIcon(QIcon::fromTheme("document-save-as", QIcon(":/data/icon/document-save-as")));
	connect(m_aSaveAs, &QAction::triggered,
			this, &MainWindow::dialogSaveAsFile);

	m_aExportAudio = new QAction(tr("Audio"), this);
	m_aExportAudio->setIcon(QIcon::fromTheme("audio-x-generic", QIcon(":/data/icon/audio-x-generic")));
	connect(m_aExportAudio, &QAction::triggered,
			this, &MainWindow::dialogExportAudio);

	m_aQuit = new QAction(tr("Quit"), this);
	m_aQuit->setShortcut(QKeySequence::Quit);
	m_aQuit->setIcon(QIcon::fromTheme("application-exit", QIcon(":/data/icon/application-exit")));
	connect(m_aQuit, &QAction::triggered, this, &MainWindow::close);

	// Edit menu
	initStepActions();
	initPaternActions();

	m_aCopy = new QAction(tr("Copy"), this);
	m_aCopy->setShortcut(QKeySequence::Copy);
	m_aCopy->setIcon(QIcon::fromTheme("edit-copy", QIcon(":/data/icon/edit-copy")));
	connect(m_aCopy, &QAction::triggered, this, &MainWindow::copySelection);

	m_aCut = new QAction(tr("Cut"), this);
	m_aCut->setShortcut(QKeySequence::Cut);
	m_aCut->setIcon(QIcon::fromTheme("edit-cut", QIcon(":/data/icon/edit-cut")));
	m_aCut->setEnabled(false);
	connect(m_aCut, &QAction::triggered, this, &MainWindow::cutSelection);

	m_aPaste = new QAction(tr("Paste"), this);
	m_aPaste->setShortcut(QKeySequence::Paste);
	m_aPaste->setIcon(QIcon::fromTheme("edit-paste", QIcon(":/data/icon/edit-paste")));
	connect(m_aPaste, &QAction::triggered, this, &MainWindow::pasteSelection);

	m_aSimplify = new QAction(tr("Simplify"), this);
	connect(m_aSimplify, &QAction::triggered,
			m_pScore->data(), &ScoreSection::simplify);

	m_aSetDivision = new QAction(tr("Set division"), this);
	m_aSetDivision->setIcon(QIcon(":/data/icon/division.svg"));
	connect(m_aSetDivision, &QAction::triggered,
			this, &MainWindow::dialogSetDivision);

	m_aSetTempo = new QAction(tr("Add change in tempo"), this);
	connect(m_aSetTempo, &QAction::triggered,
			this, &MainWindow::dialogSetTempo);

	m_aSetVolume = new QAction(tr("Add change in volume"), this);
	connect(m_aSetVolume, &QAction::triggered,
			this, &MainWindow::dialogSetVolume);

	m_aToggleAccent = new QAction(tr("Accent"), this);
	m_aToggleAccent->setIcon(QIcon(":/data/icon/accent.svg"));
	m_aToggleAccent->setCheckable(true);

	// Display menu
	m_aSetBeatPerLines = new QAction(tr("Set beat per lines"), this);
	connect(m_aSetBeatPerLines, &QAction::triggered,
			this, &MainWindow::dialogSetBeatPerLines);

	// Playing menu
	m_aPlay = new QAction(tr("Play"), this);
	m_aPlay->setShortcut(QKeySequence(Qt::Key_Space));
	m_aPlay->setIcon(QIcon::fromTheme("media-playback-start", QIcon(":/data/icon/media-playback-start")));
	connect(m_aPlay, &QAction::triggered, this, &MainWindow::togglePlay);

	m_aStop = new QAction(tr("Stop"), this);
	m_aStop->setIcon(QIcon::fromTheme("media-playback-stop", QIcon(":/data/icon/media-playback-stop")));
	connect(m_aStop, &QAction::triggered, this, &MainWindow::stopPlaying);

	m_aRepeat = new QAction(tr("Repeat"), this);
	m_aRepeat->setIcon(QIcon::fromTheme("media-playlist-repeat", QIcon(":/data/icon/media-playlist-repeat")));
	m_aRepeat->setCheckable(true);
}

void MainWindow::initStepActions()
{
	int key = Qt::Key_0;
	QAction* action;
	{
		// draft: mettre ça ailleur
		QList<QSize> sizes;
		sizes << QSize(16, 16) << QSize(22, 22)
			  << QSize(24, 24) << QSize(32, 32) << QSize(48, 48)
			  << QSize(64, 64) << QSize(96, 96);
		QIcon emptyIcon;

		foreach(QSize size, sizes)
		{
			QPixmap emptyImage(size);
			emptyImage.fill(QColor(0, 0, 0, 0)); // clear

			QPainter painter(&emptyImage);
			painter.setBrush(Qt::NoBrush);
			painter.setPen(QPen(QBrush(Qt::black), 1., Qt::DotLine));
			painter.drawRect(emptyImage.rect().adjusted(0, 0, -1, -1));

			emptyIcon.addPixmap(emptyImage);
		}

		action = new QAction(tr("Empty Step"), this);
		action->setShortcut(QKeySequence(key));
		action->setIcon(emptyIcon);
		connect(action, &QAction::triggered, [this]() { addStep(Q_NULLPTR); });
		m_lstAddStep.append(action);
		++key;
	}
	foreach(Step* step, m_pScore->steps())
	{
		QPixmap icon(24, 24);
		QPainter painter(&icon);
		painter.fillRect(0, 0, 24, 24, Qt::white);
		painter.drawPixmap(step->imagePos(), step->image());

		action = new QAction(icon, step->name(), this);
		if(key <= Qt::Key_9)
			action->setShortcut(QKeySequence(key));
		connect(action, &QAction::triggered, [this, step]() { addStep(step); });
		m_lstAddStep.append(action);
		++key;
	}
}

void MainWindow::initPaternActions()
{
	QAction* action;
	int key = key = Qt::Key_A;
	foreach(const Patern& patern, m_pScore->cpaterns())
	{
		action = new QAction(patern.name(), this);
		if(key <= Qt::Key_Z)
			action->setShortcut(QKeySequence(key));
		connect(action, &QAction::triggered, [this, patern]() { addPatern(patern.id()); });
		m_lstAddPatern.append(action);
		++key;
	}
}

void MainWindow::initMenu()
{
	// File
	m_menuFile = menuBar()->addMenu(tr("&File"));
	m_menuFile->addAction(m_aNew);
	m_menuFile->addAction(m_aOpen);
	m_menuFile->addAction(m_aSave);
	m_menuFile->addAction(m_aSaveAs);

	m_menuExport = m_menuFile->addMenu(tr("Export..."));
	m_menuExport->setIcon(QIcon::fromTheme("document-export", QIcon(":/data/icon/document-export")));
	m_menuExport->addAction(m_aExportAudio);

	m_menuFile->addAction(m_aQuit);

	// Edit
	m_menuEdit = menuBar()->addMenu(tr("&Edit"));

	m_menuEdit->addAction(m_aCopy);
	m_menuEdit->addAction(m_aCut);
	m_menuEdit->addAction(m_aPaste);
	m_menuEdit->addSeparator();

	m_menuAddStep = m_menuEdit->addMenu(tr("Add step..."));
	initStepMenu(m_menuAddStep);

	m_menuAddPatern = m_menuEdit->addMenu(tr("Add patern..."));
	initPaternMenu(m_menuAddPatern);

	m_menuEdit->addSeparator();
	m_menuEdit->addAction(m_aSetDivision);
	m_menuEdit->addAction(m_aToggleAccent);
	m_menuEdit->addSeparator();

	m_menuCursor = m_menuEdit->addMenu(tr("Cursor"));
	m_menuCursor->addAction(m_aSetTempo);
	m_menuCursor->addAction(m_aSetVolume);

	m_menuEdit->addSeparator();
	m_menuEdit->addAction(m_aSimplify);

	// Display
	m_menuDisplay = menuBar()->addMenu(tr("Display"));
	m_menuDisplay->addAction(m_aSetBeatPerLines);

	// Play
	m_menuPlay = menuBar()->addMenu(tr("Playing"));
	m_menuPlay->addAction(m_aPlay);
	m_menuPlay->addAction(m_aStop);
	m_menuPlay->addAction(m_aRepeat);
}

void MainWindow::initStepMenu(QMenu* addStepMenu)
{
	foreach(QAction* action, m_lstAddStep)
	{
		addStepMenu->addAction(action);
	}
}

void MainWindow::initPaternMenu(QMenu* addPaternMenu)
{
	foreach(QAction* action, m_lstAddPatern)
	{
		addPaternMenu->addAction(action);
	}
}

void MainWindow::initToolBars()
{
	m_tbFile = addToolBar(tr("File"));
	m_tbFile->addAction(m_aNew);
	m_tbFile->addAction(m_aOpen);
	m_tbFile->addAction(m_aSave);

	m_tbEdit = addToolBar(tr("Edit"));
	m_tbEdit->addAction(m_aCopy);
	m_tbEdit->addAction(m_aCut);
	m_tbEdit->addAction(m_aPaste);
	m_tbEdit->addAction(m_aSetDivision);
	m_tbEdit->addAction(m_aToggleAccent);
	foreach(QAction* action, m_lstAddStep)
	{
		m_tbEdit->addAction(action);
	}

	m_tbPlay = addToolBar(tr("Playing"));
	m_tbPlay->addAction(m_aPlay);
	m_tbPlay->addAction(m_aStop);
	m_tbPlay->addAction(m_aRepeat);
}

void MainWindow::initDocks()
{
	//! @todo UTILISER DES ICONS AU LIEU DES PIXMAPS POUR LES PAS #CODE-QUALITÉ
	//! @todo METTRE L'ICON DU PAS VIDE QQ PAR (ex. en static dans Step) #CODE-QUALITÉ

	m_dockPaterns = new QDockWidget(tr("Paterns"), this);

	m_wdgPaternList = new PaternListWidget(m_pScore->cpaterns(), m_dockPaterns);
	connect(m_wdgPaternList, &PaternListWidget::triggered,
			this, &MainWindow::addPatern);

	m_dockPaterns->setWidget(m_wdgPaternList);
	addDockWidget(Qt::LeftDockWidgetArea, m_dockPaterns);

	m_dockScore = new QDockWidget(tr("Score"), this);

	m_wdgOptionScore = new DockScore(m_pScore, m_wdgScore, m_dockScore);

	m_dockScore->setWidget(m_wdgOptionScore);
	addDockWidget(Qt::BottomDockWidgetArea, m_dockScore);
}

void MainWindow::addStep(Step* step)
{
	ScoreIndex pos(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT));
	if(pos.isEnd())
	{
		quint32 div(m_pScore->defaultDivision());
		pos = m_pScore->data()->insert(pos, ScoreBeat::generate(div));
	}
	ScoreAtom atom(step, m_aToggleAccent->isChecked());
	m_pScore->data()->emplace(pos, atom);
	pos = m_pScore->data()->lastOp().end();
	m_wdgScore->setScoreCursor(ScoreWidget::CURSOR_DEFAULT, pos);
}

void MainWindow::addPatern(const QString& id)
{
	ScoreIndex pos(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT));
	quint32 div(m_pScore->defaultDivision());
	const Patern& patern(m_pScore->cpatern(id));
	m_pScore->data()->emplacePatern(pos, div, patern);
	pos = m_pScore->data()->lastOp().end();
	m_wdgScore->setScoreCursor(ScoreWidget::CURSOR_DEFAULT, pos);
}
