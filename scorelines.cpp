/**
 * @file scorelines.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-22
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scorelines.h"

#include "scorelinewidget.h"

ScoreLines::ScoreLines(ScoreIndex idxBegin, ScoreIndex idxEnd,
					   ScoreWidget* pParent):
	QVBoxLayout(),
	m_idxBegin(idxBegin),
	m_idxEnd(idxEnd),
	m_wdgScoreLine(Q_NULLPTR),
	m_wdgParentScore(pParent)
{
	m_wdgScoreLine = new ScoreLineWidget(this);
	addWidget(m_wdgScoreLine);
}

ScoreIndex ScoreLines::begin() const
{
	return m_idxBegin;
}

ScoreIndex ScoreLines::end() const
{
	return m_idxEnd;
}

ScoreLineWidget* ScoreLines::scoreLineWidget() const
{
	return m_wdgScoreLine;
}

ScoreWidget* ScoreLines::scoreWiget()
{
	return m_wdgParentScore;
}
