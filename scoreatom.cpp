/**
 * @file scoreatom.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scoreatom.h"

ScoreAtom::ScoreAtom():
	m_pStep(Q_NULLPTR),
	m_iTempo(Q_NULLPTR),
	m_fVolume(Q_NULLPTR),
	m_haveAccent(false),
//	m_itParent(),
	m_pGroup(Q_NULLPTR)
{ }
ScoreAtom::ScoreAtom(const ScoreAtom& other):
	m_pStep(other.m_pStep),
	m_iTempo(other.m_iTempo ? new int(*other.m_iTempo) : Q_NULLPTR),
	m_fVolume(other.m_fVolume ? new float(*other.m_fVolume) : Q_NULLPTR),
	m_haveAccent(other.m_haveAccent),
//	m_itParent(other.m_itParent),
	m_pGroup(other.m_pGroup)
{ }
ScoreAtom::ScoreAtom(ScoreAtom&& other):
	m_pStep(other.m_pStep),
	m_iTempo(other.m_iTempo),
	m_fVolume(other.m_fVolume),
	m_haveAccent(other.m_haveAccent),
//	m_itParent(other.m_itParent),
	m_pGroup(other.m_pGroup)
{
	other.m_iTempo = Q_NULLPTR;
	other.m_fVolume = Q_NULLPTR;
	other.m_pGroup = Q_NULLPTR;
}
ScoreAtom::ScoreAtom(Step* pStep, bool accent, int iTempo, float fVolume):
	m_pStep(pStep),
	m_iTempo(new int(iTempo)),
	m_fVolume(new float(fVolume)),
	m_haveAccent(accent),
//	m_itParent(),
	m_pGroup(Q_NULLPTR)
{ }
ScoreAtom::ScoreAtom(Step* pStep, bool accent):
	m_pStep(pStep),
	m_iTempo(Q_NULLPTR),
	m_fVolume(Q_NULLPTR),
	m_haveAccent(accent),
//	m_itParent(),
	m_pGroup(Q_NULLPTR)
{ }

ScoreAtom::~ScoreAtom()
{
	setDefaultTempo();
	setDefaultVolume();
	setNoGroup();
}

ScoreAtom& ScoreAtom::operator=(const ScoreAtom& other)
{
	m_pStep = other.m_pStep;
	if(other.m_iTempo)
		setTempo(*other.m_iTempo);
	if(other.m_fVolume)
		setVolume(*other.m_fVolume);
	m_haveAccent = other.m_haveAccent;
//	m_itParent = other.m_itParent;
	if(other.m_pGroup)
		setGroup(*other.m_pGroup);
	return *this;
}
ScoreAtom& ScoreAtom::operator=(ScoreAtom&& other)
{
	m_pStep = other.m_pStep;
	if(other.m_iTempo)
		setTempo(*other.m_iTempo);
	if(other.m_fVolume)
		setVolume(*other.m_fVolume);
	m_haveAccent = other.m_haveAccent;
//	m_itParent = other.m_itParent;
	if(other.m_pGroup)
		setGroup(*other.m_pGroup);

	other.m_iTempo = Q_NULLPTR;
	other.m_fVolume = Q_NULLPTR;
	other.m_pGroup = Q_NULLPTR;
	return *this;
}

const Step* ScoreAtom::step() const Q_DECL_NOTHROW
{
	return m_pStep;
}
const int* ScoreAtom::tempo() const Q_DECL_NOTHROW
{
	return m_iTempo;
}
const float* ScoreAtom::volume() const Q_DECL_NOTHROW
{
	return m_fVolume;
}
bool ScoreAtom::haveAccent() const Q_DECL_NOTHROW
{
	return m_haveAccent;
}

QLinkedList<ScoreGroup>::Iterator* ScoreAtom::group() Q_DECL_NOTHROW
{
	return m_pGroup;
}

void ScoreAtom::setStep(Step* pStep) Q_DECL_NOTHROW
{
	m_pStep = pStep;
}
void ScoreAtom::setTempo(int iTempo) Q_DECL_NOTHROW
{
	if(m_iTempo == Q_NULLPTR)
		m_iTempo = new int;

	*m_iTempo = iTempo;
}
void ScoreAtom::setDefaultTempo() Q_DECL_NOTHROW
{
	if(m_iTempo != Q_NULLPTR)
	{
		delete m_iTempo;
		m_iTempo = Q_NULLPTR;
	}
}
void ScoreAtom::setVolume(float fVolume) Q_DECL_NOTHROW
{
	if(m_fVolume == Q_NULLPTR)
		m_fVolume = new float;

	*m_fVolume = fVolume;
}
void ScoreAtom::setDefaultVolume() Q_DECL_NOTHROW
{
	if(m_fVolume != Q_NULLPTR)
	{
		delete m_fVolume;
		m_fVolume = Q_NULLPTR;
	}
}
void ScoreAtom::setHaveAccent(bool accent) Q_DECL_NOTHROW
{
	m_haveAccent = accent;
}

void
ScoreAtom::setGroup(QLinkedList<ScoreGroup>::Iterator itGroup) Q_DECL_NOTHROW
{
	if(m_pGroup == Q_NULLPTR)
		m_pGroup = new QLinkedList<ScoreGroup>::Iterator;

	*m_pGroup = itGroup;
}

void ScoreAtom::setNoGroup() Q_DECL_NOTHROW
{
	if(m_pGroup)
	{
		delete m_pGroup;
		m_pGroup = Q_NULLPTR;
	}
}
/*void ScoreAtom::setParent(ScoreBeatList::Iterator itParent) Q_DECL_NOTHROW
{
	m_itParent = itParent;
}*/
