/**
 * @file scoresection.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scoresection.h"

ScoreSection::ScoreSection(QObject* parent):
	QObject(parent),
	m_lstBeats(),
	m_gpLastOp(),
	m_beginAt(),
	m_endAt()
{ }

ScoreSection::ScoreSection(const ScoreSection& other):
	QObject(other.parent()),
	m_lstBeats(other.m_lstBeats),
	m_gpLastOp(other.m_gpLastOp),
	m_beginAt(other.m_beginAt),
	m_endAt(other.m_endAt)
{ }

ScoreSection& ScoreSection::operator=(const ScoreSection& other)
{
	setParent(other.parent());
	m_lstBeats = other.m_lstBeats;
	m_beginAt = other.m_beginAt;
	m_endAt = other.m_endAt;
	return *this;
}

quint32
ScoreSection::indexOfBeatIter(ScoreBeatListCIt itBeat) const Q_DECL_NOTHROW
{
	return static_cast<quint32>(itBeat - m_lstBeats.cbegin());
}

ScoreBeatListCIt
ScoreSection::citerOfBeatIndex(quint32 uBeat) const Q_DECL_NOTHROW
{
	return (m_lstBeats.cbegin() + uBeat);
}

const ScoreBeat& ScoreSection::beatAt(quint32 uBeat) const Q_DECL_NOTHROW
{
	return m_lstBeats.at(static_cast<int>(uBeat));
}

ScoreBeatListIt ScoreSection::iterOfBeatIndex(quint32 uBeat) Q_DECL_NOTHROW
{
	return (m_lstBeats.begin() + uBeat);
}

quint32 ScoreSection::commonBeatDiv() const Q_DECL_NOTHROW
{
	QLinkedList<Subdiv> sizes;

	ScoreIndex idx(beginBeat());
	while(idx != endBeat())
	{
		Subdiv unit(idx.beat().unit());
		if(unit.isValid())
			sizes.append(unit);
		idx = idx.advance(ScoreIndex::AM_NEXT_BEAT);
	}
	return Subdiv::common(sizes);
}

ScoreIndex ScoreSection::insert(ScoreIndex position, const ScoreBeat& beat)
{
	Q_ASSERT(position.section() == this);
	position.normalize();

	insertBeat(position.beatIndex(), beat);
	emit changed();

	return ScoreIndex(this, position.beatIndex());
}
ScoreIndex ScoreSection::insert(ScoreIndex position, const ScoreAtom& atom)
{
	Q_ASSERT(position.section() == this);

	quint32 beatIndex(position.beatIndex());
	Subdiv sub(position.subdiv());

	ScoreBeat& refBeat(beat(beatIndex));
	refBeat.insertAtom(refBeat.indexOfSubdiv(sub), atom);
	emit changed();
	return ScoreIndex(this, beatIndex, Subdiv(sub.num(), sub.den()+1));
}

ScoreIndex ScoreSection::insertPatern(ScoreIndex position, quint32 division,
									  const Patern& patern)
{
	ScoreSection* paternSection(createPatern(division, patern));
	ScoreIndex it(insert(position, *paternSection));
	delete paternSection;
	return it;
}

ScoreIndex ScoreSection::emplacePatern(ScoreIndex position, quint32 division,
									   const Patern& patern)
{
	ScoreSection* paternSection(createPatern(division, patern));
	ScoreIndex it(emplace(position, *paternSection));
	delete paternSection;
	return it;
}

ScoreSection* ScoreSection::createPatern(quint32 division,
										 const Patern& patern)
{
	ScoreSection* section(new ScoreSection);
	ScoreBeat beat;
	foreach(const ScoreAtom& atom, patern.catoms())
	{
		beat.appendAtom(atom);
		if(beat.usize() >= division)
		{
			section->insert(section->endBeat(), beat);
			beat.clearAtom();
		}
	}
	if(!beat.isEmptyAtom())
	{
		section->setEndAt(Subdiv(beat.usize(), division));
		while(beat.usize() < division)
			beat.appendAtom(ScoreAtom());
		section->insert(section->endBeat(), beat);
	}
	return section;
}
ScoreIndex ScoreSection::insert(ScoreIndex position, const ScoreSection& group)
{
	Q_ASSERT(position.section() == this);

	if(position.beatIndex() < beatCount())
	{
		ScoreIndex ret;
		ScoreSection* endPart(peek(position, endBeat()));
		position = erase(position, endBeat());
		ret = position = emplace(position, group);
		position = position.advance(group.size());
		position = emplace(position, *endPart);
		delete endPart;
		emit changed();
		return ret;
	}
	else
	{
		return emplace(position, group);
	}
}

ScoreIndex ScoreSection::emplace(ScoreIndex position, const ScoreBeat& rBeat)
{
	if(position == endBeat())
		return insert(position, rBeat); // Cannot remplace end

	Q_ASSERT(position.section() == this);
	Q_ASSERT(position.subdiv().isValid() == false);

	beat(position.beatIndex()) = rBeat;
	emit changed();

	return position;
}
ScoreIndex ScoreSection::emplace(ScoreIndex position, const ScoreAtom& atom)
{
	Q_ASSERT(position.section() == this);

	position.normalize(false);

	quint32 beatIndex(position.beatIndex());
	Subdiv sub(position.subdiv());

	ScoreBeat& refBeat(beat(beatIndex));
	refBeat.valueOfSubdiv(sub) = atom;

	m_gpLastOp.setBegin(position);
	m_gpLastOp.setEnd(position.advance(refBeat.unit()));

	emit changed();

	return m_gpLastOp.begin();
}

ScoreIndex ScoreSection::emplace(ScoreIndex position, const ScoreSection& group)
{
	Q_ASSERT(position.section() == this);

	Subdiv sub(position.subdiv());
	if(!sub.isValid())
		sub = Subdiv::ZERO;

	ScoreIndex dstidx(position);
	ScoreIndex srcidx(group.beginBeat());
	while(srcidx != group.endBeat())
	{
		Subdiv begin(Subdiv::ZERO);
		Subdiv end(Subdiv::ONE);
		if(srcidx == group.beginBeat() && group.beginAt().isValid())
			begin = group.beginAt();
		if(srcidx.advance(ScoreIndex::AM_NEXT_BEAT) == group.endBeat()
				&& group.endAt().isValid())
		{
			end = group.endAt();
		}

		dstidx = emplaceBeat(dstidx, srcidx.beat(), begin, end);
		sub = sub+end-begin;
		if(sub.num() >= sub.den())
		{
			sub.setNum(sub.num()%sub.den());
			dstidx = dstidx.advance(ScoreIndex::AM_NEXT_BEAT);
			dstidx = ScoreIndex(this, dstidx.beatIndex(), sub);
		}
		else
		{
			dstidx = ScoreIndex(this, dstidx.beatIndex(), sub);
		}

		srcidx = srcidx.advance(ScoreIndex::AM_NEXT_BEAT);
	}
	m_gpLastOp.setBegin(position);
	m_gpLastOp.setEnd(dstidx);

	emit changed();

	return m_gpLastOp.begin();
}

ScoreIndex ScoreSection::erase(ScoreIndex position)
{
	Q_ASSERT(position.section() == this);

	if(position.subdiv().isValid())
	{
		quint32 beatIndex(position.beatIndex());
		Subdiv sub(position.subdiv());

		ScoreBeat& refBeat(beat(beatIndex));
		sub = refBeat.removeAtom(sub);

		return ScoreIndex(this, beatIndex, sub);
	}
	else
	{
		quint32 beatIndex(position.beatIndex());
		removeBeat(beatIndex);
		return ScoreIndex(this, beatIndex);
	}
}

ScoreIndex ScoreSection::eraseFrom(ScoreIndex position)
{
	Q_ASSERT(position.section() == this);

	Subdiv sub(position.subdiv());
	ScoreIndex basePos(position);
	if(sub.isValid() && !sub.isNull())
		position = position.advance(ScoreIndex::AM_NEXT_BEAT);

	while(position != endBeat())
		position = erase(position);

	if(sub.isValid() && !sub.isNull())
		return basePos;
	else
		return position;
}

ScoreIndex ScoreSection::erase(ScoreIndex begin, ScoreIndex end)
{
	ScoreSection* tmp(peek(end, endBeat()));
	ScoreIndex beginEnd(eraseFrom(begin));
	beginEnd = emplace(beginEnd, *tmp);
	delete tmp;
	return beginEnd;
}

void ScoreSection::shiftBeat(int count)
{
	if(count >= 0)
	{
		for(int i=0; i<count; ++i)
		{
			m_lstBeats.insert(m_lstBeats.begin(), ScoreBeat());
		}
	}
	else
	{
		for(int i=0; i<-count; ++i)
		{
			m_lstBeats.erase(m_lstBeats.begin());
		}
	}
}

ScoreIndex ScoreSection::emplaceBeat(ScoreIndex position, const ScoreBeat& rBeat,
									 Subdiv from, Subdiv to)
{
	Subdiv sub(position.subdiv());
	if(sub.isValid() && sub.isOne())
	{
		position = position.advance(ScoreIndex::AM_NEXT_BEAT);
		sub = Subdiv::ZERO;
	}

	quint32 beatIndex(position.beatIndex());
	Subdiv size(to-from);

	if(!sub.isValid() || sub.isNull())
	{
		if(beatIndex == beatCount())
			m_lstBeats.append(ScoreBeat());
		ScoreBeat& refBeat(beat(beatIndex));
		refBeat.copyAt(rBeat, from, to, Subdiv::ZERO);
	}
	else
	{
		if(beatIndex == beatCount())
		{
			m_lstBeats.append(ScoreBeat());
			position = ScoreIndex(this, beatIndex, sub);
		}
		ScoreBeat& refBeat(beat(beatIndex));
		Subdiv toAt(sub+size);
		if(toAt.num() > toAt.den())
			toAt = Subdiv::ONE;
		refBeat.copyFrom(rBeat, from, sub, toAt);
		position = position.advance(ScoreIndex::AM_NEXT_BEAT);

		toAt = sub+size;
		if(toAt.num() > toAt.den())
		{
			if(position.beatIndex() == beatCount())
			{
				quint32 pos(position.beatIndex());
				m_lstBeats.append(ScoreBeat());
				position = ScoreIndex(this, pos, sub);
			}
			beat(position.beatIndex()).copyAt(rBeat, Subdiv::ONE - sub + from,
											  to, Subdiv::ZERO);
		}
	}
	return ScoreIndex(this, beatIndex, sub);
}

ScoreGroup ScoreSection::lastOp() const
{
	return m_gpLastOp;
}

void ScoreSection::simplify()
{
	for(quint32 i=0; i<beatCount(); ++i)
	{
		beat(i).simplify();
	}
	emit changed();
}

Subdiv ScoreSection::beginAt() const Q_DECL_NOTHROW
{
	return m_beginAt;
}
Subdiv ScoreSection::endAt() const Q_DECL_NOTHROW
{
	return m_endAt;
}

void ScoreSection::setBeginAt(const Subdiv& beginAt) Q_DECL_NOTHROW
{
	m_beginAt = beginAt;
}
void ScoreSection::setEndAt(const Subdiv& endAt) Q_DECL_NOTHROW
{
	m_endAt = endAt;
}

ScoreSection* ScoreSection::multiply(Subdiv factor) const
{
	Subdiv mod(factor.num()%factor.den(), factor.den());
	if(mod.isNull())
		mod = Subdiv::ONE;
	ScoreSection* section = new ScoreSection;
	ScoreIndex position(section->beginBeat());
	Subdiv shift(Subdiv::ZERO);
	foreach(const ScoreBeat& beat, m_lstBeats)
	{
		ScoreBeatList toAdds(beat.multiply(factor));
		ScoreBeatListIt it(toAdds.begin());
		for(int i=1; i<toAdds.size(); ++i)
		{
			position = section->emplaceBeat(position, *it,
											Subdiv::ZERO, Subdiv::ONE);
			position = section->index(position.beatIndex()+1, shift);
			++it;
		}
		shift = shift+mod;
		position = section->emplaceBeat(position, *it, Subdiv::ZERO, mod);
		if(shift.num() >= shift.den())
		{
			shift.setNum(shift.num()%shift.den());
			position = section->index(position.beatIndex()+1, shift);
		}
		else
			position = section->index(position.beatIndex(), shift);

	}
	return section;
}

ScoreSection* ScoreSection::peek(ScoreIndex begin,
								 ScoreIndex end) const
{
	Q_ASSERT(begin.section() == this && end.section() == this);
	ScoreSection* section(new ScoreSection);

	if(end <= begin)
		qSwap(begin, end);

	if(begin.subdiv().isNull()) // important to check isNull() BEFORE isOne()
		begin = ScoreIndex(this, begin.beatIndex());
	else if(begin.subdiv().isOne())
		begin = ScoreIndex(this, begin.beatIndex()+1);

	if(end.subdiv().isNull()) // important to check isNull() BEFORE isOne()
		end = ScoreIndex(this, end.beatIndex());
	else if(end.subdiv().isOne())
		end = ScoreIndex(this, end.beatIndex()+1);

	section->setBeginAt(begin.subdiv());
	section->setEndAt(end.subdiv());

	begin = ScoreIndex(this, begin.beatIndex());
	if(end.subdiv().isValid())
		end = ScoreIndex(this, end.beatIndex()+1);
	else
		end = ScoreIndex(this, end.beatIndex());

	ScoreIndex idx(begin);
	while(idx != end)
	{
		section->insert(section->endBeat(), idx.beat());
		idx = idx.advance(ScoreIndex::AM_NEXT_BEAT);
	}
	return section;
}

ScoreSection* ScoreSection::take(ScoreIndex begin, ScoreIndex end)
{
	ScoreSection* datas(peek(begin, end));
	erase(begin, end);
	return datas;
}

ScoreBeat& ScoreSection::beat(quint32 uBeat)
{
	return m_lstBeats[static_cast<int>(uBeat)];
}

ScoreIndex ScoreSection::beginBeat() const Q_DECL_NOTHROW
{
	return ScoreIndex(this, 0);
}
ScoreIndex ScoreSection::endBeat() const Q_DECL_NOTHROW
{
	return ScoreIndex(this, beatCount());
}

ScoreIndex ScoreSection::index(ScoreBeatListIt itBeat,
							   Subdiv sub) const Q_DECL_NOTHROW
{
	return ScoreIndex(this, indexOfBeatIter(itBeat), sub);
}
ScoreIndex ScoreSection::index(quint32 beat, Subdiv sub) const Q_DECL_NOTHROW
{
	return ScoreIndex(this, beat, sub);
}

Subdiv ScoreSection::size() const Q_DECL_NOTHROW
{
	quint32 nbBeat(static_cast<quint32>(m_lstBeats.size()));
	Subdiv totalSize(nbBeat, 1);
	if(beginAt().isValid())
		totalSize = totalSize-beginAt();
	if(endAt().isValid())
		totalSize = totalSize+endAt()-Subdiv::ONE;
	return totalSize;
}
