<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>DockScore</name>
    <message>
        <location filename="dockscore.ui" line="17"/>
        <source>Beat division</source>
        <translation>Division du temps</translation>
    </message>
    <message>
        <location filename="dockscore.ui" line="27"/>
        <source>Tempo</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="dockscore.ui" line="55"/>
        <source>Volume</source>
        <translation>Nuance/volume</translation>
    </message>
    <message>
        <location filename="dockscore.ui" line="99"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="dockscore.ui" line="114"/>
        <source>Accent</source>
        <translation>Accent</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="156"/>
        <source>Beat division</source>
        <translation>Division du temps</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <source>Default beat division</source>
        <translation>Division du temps par défaut</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="169"/>
        <location filename="mainwindow.cpp" line="194"/>
        <source>Invalid cursor position</source>
        <translation>Position du curseur invalide</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="170"/>
        <location filename="mainwindow.cpp" line="195"/>
        <source>Cursor don&apos;t point out to step</source>
        <translation>Le curseur ne pointe pas sur un pas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="179"/>
        <source>Tempo</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="180"/>
        <source>Change tempo from cursor</source>
        <translation>Changement de tempo au curseur</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="204"/>
        <source>Volume</source>
        <translation>Nuance/volume</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="205"/>
        <source>Change volume from cursor</source>
        <translation>Changement de nuance/volume au curseur</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="221"/>
        <source>Beat per lines</source>
        <translation>Temps par lignes</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="222"/>
        <source>Number of beat per lines</source>
        <translation>Nombre de temps par lignes</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <location filename="mainwindow.cpp" line="261"/>
        <source>XML Score Flamenco (*.fls);;XML (*.xml)</source>
        <translation>Partition flamenco XML (*.fls);;XML (*.xml)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="244"/>
        <source>Open score</source>
        <translation>Ouvrir une partition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>Save score</source>
        <translation>Enregistrer une partition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <source>Waveform file (*.wav);;Free Lossless Audio Codec file (*.flac);;Ogg vorbis (*.ogg);;MPEG-1/2 Audio Layer III file (*.mp3)</source>
        <translation>Fichier waveform (*.wav);;Fichier Free Lossless Audio Codec (*.flac);;Fichier ogg vorbis (*.ogg);;Fichier MPEG-1/2 Audio Layer III (*.mp3)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="293"/>
        <source>Export score</source>
        <translation>Exportation de la partition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="326"/>
        <source>Error while reading file</source>
        <translation>Erreur lors de la lecture du fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="327"/>
        <source>Reading file error</source>
        <translation>Erreur de lecture de fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="332"/>
        <source>Error while opening file: %1</source>
        <translation>Erreur lors de l&apos;ouverture du fichier : %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="335"/>
        <source>Opening file error</source>
        <translation>Erreur d&apos;ouverture du fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="349"/>
        <source>Error while saving file: %1</source>
        <translation>Erreur lors de la sauvegarde du fichier : %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="352"/>
        <source>Saving file error</source>
        <translation>Erreur de sauvegarde du fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="382"/>
        <source>Error while exporting file</source>
        <translation>Erreur survenu lors de l&apos;exportation du fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="384"/>
        <source>Exporting file error</source>
        <translation>Erreur d&apos;exportation du fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="436"/>
        <location filename="mainwindow.cpp" line="532"/>
        <source>Play</source>
        <translation>Lecture</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="470"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="475"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="480"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <source>Save as...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="491"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="496"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="505"/>
        <source>Simplify</source>
        <translation>Simplifier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="509"/>
        <source>Set division</source>
        <translation>Définir la division</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="514"/>
        <source>Add change in tempo</source>
        <translation>Ajouter un changement de tempo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="518"/>
        <source>Add change in volume</source>
        <translation>Ajouter un changement de nuance/volume</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="522"/>
        <source>Accent</source>
        <translation>Accent</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="527"/>
        <source>Set beat per lines</source>
        <translation>Changer le nombre de temps par lignes</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="537"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="541"/>
        <source>Repeat</source>
        <translation>Répéter</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="571"/>
        <source>Empty Step</source>
        <translation>Pas vide</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="612"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="618"/>
        <source>Export...</source>
        <oldsource>Export</oldsource>
        <translation>Exporter...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="625"/>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="627"/>
        <source>Add step...</source>
        <oldsource>Add step</oldsource>
        <translation>Ajouter pas...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="630"/>
        <source>Add patern...</source>
        <oldsource>Add patern</oldsource>
        <translation>Ajouter motif...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="638"/>
        <source>Cursor</source>
        <translation>Curseur</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="646"/>
        <source>Display</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="650"/>
        <location filename="mainwindow.cpp" line="687"/>
        <source>Playing</source>
        <translation>Lecture</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="674"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="679"/>
        <source>Edit</source>
        <translation>Édition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="698"/>
        <source>Paterns</source>
        <translation>Motifs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="707"/>
        <source>Score</source>
        <translation>Partition</translation>
    </message>
</context>
</TS>
