#ifndef DOCKSCORE_H_INCLUDED
#define DOCKSCORE_H_INCLUDED
/**
 * @file dockscore.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

#include "score.h"
#include "scoreindex.h"
#include "scorewidget.h"

namespace Ui {
class DockScore;
}

class DockScore : public QWidget
{
	Q_OBJECT
public:
	explicit DockScore(Score* pScore, ScoreWidget* wdgScore,
					   QWidget* parent = Q_NULLPTR);
	~DockScore();

	void setTempo(bool enable, bool activate, int value);
	void setVolume(bool enable, bool activate, float value);
	void setAccent(bool enable, bool activate = false);

	void updateTempo();
	void updateVolume();

public slots:
	void setIndex(const ScoreIndex& index);
	void accentToggled(bool haveAccent);
	void setDivision(int but = -1);

private:
	Ui::DockScore* ui;

	QList<QToolButton*> m_lstBeatDiv;
	QPushButton* m_btBeatDivSet;
	QSpinBox* m_sbBeatDiv;

	Score* m_pScore;
	ScoreWidget* m_wdgScore;
	ScoreIndex m_index;
};

#endif // DOCKSCORE_H_INCLUDED
