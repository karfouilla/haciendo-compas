/**
 * @file paternlistwidget.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "paternlistwidget.h"

PaternListWidget::PaternListWidget(const QMap<QString, Patern>& paterns,
								   QWidget* parent):
	QWidget(parent),
	m_mapPaterns(paterns),
	m_pLayout(Q_NULLPTR),
	m_lstWidgets()
{
	updateWidgets();
}

const QMap<QString, Patern>& PaternListWidget::paterns() const
{
	return m_mapPaterns;
}

void PaternListWidget::setPaterns(const QMap<QString, Patern>& mapPaterns)
{
	m_mapPaterns = mapPaterns;
	updateWidgets();
}

void PaternListWidget::updateWidgets()
{
	if(m_pLayout)
	{
		while(!m_pLayout->isEmpty())
			delete m_pLayout->takeAt(0);
		qDeleteAll(m_lstWidgets); // test
		m_lstWidgets.clear();
	}
	else
	{
		m_pLayout = new QVBoxLayout(this);
		m_pLayout->setSpacing(4);
		setLayout(m_pLayout);
	}
	foreach(const Patern& patern, m_mapPaterns)
	{
		PaternWidget* paternWidget(new PaternWidget(patern, this));
		connect(paternWidget, &PaternWidget::triggered,
				this, &PaternListWidget::paternTrigger);

		m_lstWidgets.append(paternWidget);
		m_pLayout->addWidget(paternWidget);
	}
	m_pLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Minimum,
											 QSizePolicy::Expanding));
}

void PaternListWidget::paternTrigger(const QString& id)
{
	emit triggered(id);
}
