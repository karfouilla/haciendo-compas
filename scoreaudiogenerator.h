#ifndef SCOREAUDIOGENERATOR_H_INCLUDED
#define SCOREAUDIOGENERATOR_H_INCLUDED
/**
 * @file scoreaudiogenerator.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-07
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "audiogenerator.h"
#include "scoreindex.h"

class Score;

class ScoreAudioGenerator : public AudioGenerator
{
public:
	explicit ScoreAudioGenerator(QAudioFormat format,
								 QObject* parent = Q_NULLPTR);

	void setScore(const Score* pScore,
				  const ScoreIndex& startPos = ScoreIndex());
};

#endif // SCOREAUDIOGENERATOR_H_INCLUDED
