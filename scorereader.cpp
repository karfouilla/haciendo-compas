/**
 * @file scorereader.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-05
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scorereader.h"

#include "scoresection.h"
#include "step.h"

ScoreReader::ScoreReader(QIODevice* device):
	m_pFile(device),
	m_pScore(Q_NULLPTR),
	m_reader(m_pFile)
{ }

bool ScoreReader::read(Score* score)
{
	m_pScore = score;
	if(!checkBegin(QStringLiteral("flamenco")))
		return false;

	while(!m_reader.atEnd() && !m_reader.hasError())
	{
		m_reader.readNext();
		if(m_reader.isStartElement())
		{
			if(m_reader.name() == QStringLiteral("resources"))
			{
				if(!readResource(score->resource()))
					return false;
			}
			else if(m_reader.name() == QStringLiteral("steps"))
			{
				if(!readSteps(score->steps()))
					return false;
			}
			else if(m_reader.name() == QStringLiteral("paterns"))
			{
				if(!readPaterns(score->paterns()))
					return false;
			}
			else if(m_reader.name() == QStringLiteral("score"))
			{
				if(!readSection(score->data()))
					return false;
			}
			else // unknown element
			{
				m_reader.skipCurrentElement();
			}
		}
	}
	return !m_reader.hasError();
}

bool ScoreReader::checkBegin(const QString& element)
{
	if(!m_reader.readNextStartElement())
		return false;
	if(m_reader.name() != element)
		return false;

	return !m_reader.hasError();
}

bool ScoreReader::checkEnd(const QString& element)
{
	if(m_reader.readNextStartElement())
		return false;
	if(m_reader.name() != element)
		return false; // error

	return !m_reader.hasError();
}

bool ScoreReader::readResource(Resource& res)
{
	while(!m_reader.atEnd() && !m_reader.hasError())
	{
		m_reader.readNext();
		if(m_reader.isStartElement())
		{
			if(m_reader.name() == QStringLiteral("file"))
			{
				if(!m_reader.attributes().hasAttribute("name"))
					return false;
				QStringRef entry(m_reader.attributes()
									 .value(QStringLiteral("name")));

				QString base64String(m_reader.readElementText());
				QByteArray base64(base64String.toLatin1());
				QByteArray data(QByteArray::fromBase64(base64));
				res.addEntry(entry.toString(), data);
			}
			else
			{
				m_reader.skipCurrentElement();
			}
		}
		else if(m_reader.isEndElement())
		{
			break;
		}
	}
	return !m_reader.hasError();
}

bool ScoreReader::readSteps(QMap<QString, Step*>& steps)
{
	while(!m_reader.atEnd() && !m_reader.hasError())
	{
		m_reader.readNext();
		if(m_reader.isStartElement())
		{
			if(m_reader.name() == QStringLiteral("step"))
			{
				Step* step(new Step);
				if(!readStep(step))
				{
					delete step;
					return false;
				}
				steps.insert(step->id(), step);
			}
			else
			{
				m_reader.skipCurrentElement();
			}
		}
		else if(m_reader.isEndElement())
		{
			break;
		}
	}
	return !m_reader.hasError();
}

bool ScoreReader::readStep(Step* step)
{
	if(!m_reader.attributes().hasAttribute("id"))
		return false;
	step->setId(m_reader.attributes().value("id").toString());

	while(!m_reader.atEnd() && !m_reader.hasError())
	{
		m_reader.readNext();
		if(m_reader.isStartElement())
		{
			if(m_reader.name() == QStringLiteral("name"))
			{
				step->setName(m_reader.readElementText());
			}
			else if(m_reader.name() == QStringLiteral("image"))
			{
				Resource::Name name;
				if(!readResourceName(name))
					return false;
				step->setImageName(name);
			}
			else if(m_reader.name() == QStringLiteral("audio"))
			{
				Resource::Name name;
				if(!readResourceName(name))
					return false;
				step->setAudioName(name);
			}
			else if(m_reader.name() == QStringLiteral("volume"))
			{
				step->setVolume(m_reader.readElementText().toFloat());
			}
			else if(m_reader.name() == QStringLiteral("direction"))
			{
				step->setDirection(m_reader.readElementText().toFloat());
			}
			else if(m_reader.name() == QStringLiteral("symbol"))
			{
				step->setLPSymbol(m_reader.readElementText());
			}
			else if(m_reader.name() == QStringLiteral("position"))
			{
				step->setLPPosition(m_reader.readElementText().toInt());
			}
			else if(m_reader.name() == QStringLiteral("symbname"))
			{
				step->setLPSymbName(m_reader.readElementText());
			}
			else if(m_reader.name() == QStringLiteral("command"))
			{
				step->setLPCommand(m_reader.readElementText());
			}
			else if(m_reader.name() == QStringLiteral("imgposx"))
			{
				QPoint imgPos(step->imagePos());
				imgPos.setX(m_reader.readElementText().toInt());
				step->setImagePos(imgPos);
			}
			else if(m_reader.name() == QStringLiteral("imgposy"))
			{
				QPoint imgPos(step->imagePos());
				imgPos.setY(m_reader.readElementText().toInt());
				step->setImagePos(imgPos);
			}
			else
			{
				m_reader.skipCurrentElement();
			}
		}
		else if(m_reader.isEndElement())
		{
			break;
		}
	}
	return !m_reader.hasError();
}

bool ScoreReader::readResourceName(Resource::Name& resourceName)
{
	resourceName.setType(Resource::TYPE_FILE);
	if(!m_reader.attributes().hasAttribute("type"))
		if(m_reader.attributes().value("type") == QStringLiteral("xml"))
			resourceName.setType(Resource::TYPE_XML);
	resourceName.setFilename(m_reader.readElementText());
	return !m_reader.hasError();
}

bool ScoreReader::readPaterns(QMap<QString, Patern>& paterns)
{
	while(!m_reader.atEnd() && !m_reader.hasError())
	{
		m_reader.readNext();
		if(m_reader.isStartElement())
		{
			if(m_reader.name() == QStringLiteral("patern"))
			{
				Patern patern;
				if(!readPatern(patern))
					return false;
				paterns.insert(patern.id(), patern);
			}
			else
			{
				m_reader.skipCurrentElement();
			}
		}
		else if(m_reader.isEndElement())
		{
			break;
		}
	}
	return !m_reader.hasError();
}

bool ScoreReader::readPatern(Patern& patern)
{
	if(!m_reader.attributes().hasAttribute("id"))
		return false;
	patern.setId(m_reader.attributes().value("id").toString());
	if(m_reader.attributes().hasAttribute("name"))
		patern.setName(m_reader.attributes().value("name").toString());

	while(!m_reader.atEnd() && !m_reader.hasError())
	{
		m_reader.readNext();
		if(m_reader.isStartElement())
		{
			ScoreAtom atom;
			if(!readAtom(atom))
				return false;
			patern.atoms().append(atom);
		}
		else if(m_reader.isEndElement())
		{
			break;
		}
	}
	return !m_reader.hasError();
}

bool ScoreReader::readAtom(ScoreAtom& atom)
{
	if(m_reader.attributes().hasAttribute("accent"))
	{
		QString haveAccent(m_reader.attributes().value("accent").toString());
		if(haveAccent == "true" || haveAccent == "1")
			atom.setHaveAccent(true);
	}
	if(m_reader.attributes().hasAttribute("volume"))
	{
		atom.setVolume(m_reader.attributes().value("volume").toFloat());
	}
	if(m_reader.attributes().hasAttribute("tempo"))
	{
		atom.setTempo(m_reader.attributes().value("tempo").toInt());
	}
	atom.setStep(m_pScore->step(m_reader.readElementText()));
	return !m_reader.hasError();
}

bool ScoreReader::readSection(ScoreSection* section)
{
	while(!m_reader.atEnd() && !m_reader.hasError())
	{
		m_reader.readNext();
		if(m_reader.isStartElement())
		{
			if(m_reader.name() == QStringLiteral("beat"))
			{
				ScoreBeat beat;
				if(!readBeat(beat))
					return false;

				if(beat.isEmptyAtom())
					beat.appendAtom(ScoreAtom());

				section->insert(section->endBeat(), beat);
			}
			else
			{
				m_reader.skipCurrentElement();
			}
		}
		else if(m_reader.isEndElement())
		{
			break;
		}
	}
	return !m_reader.hasError();
}

bool ScoreReader::readBeat(ScoreBeat& beat)
{
	while(!m_reader.atEnd() && !m_reader.hasError())
	{
		m_reader.readNext();
		if(m_reader.isStartElement())
		{
			if(m_reader.name() == QStringLiteral("atom"))
			{
				ScoreAtom atom;
				if(!readAtom(atom))
					return false;
				beat.appendAtom(atom);
			}
			else
			{
				m_reader.skipCurrentElement();
			}
		}
		else if(m_reader.isEndElement())
		{
			break;
		}
	}
	return !m_reader.hasError();
}
