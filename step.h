#ifndef STEP_H_INCLUDED
#define STEP_H_INCLUDED
/**
 * @file step.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-09
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QByteArray>
#include <QtCore/QString>
#include <QtGui/QPixmap>

#include "audiodata.h"
#include "resource.h"

class Step
{
public:
	Step();
	Step(const QString& id, const QString& szName,
		 qreal dVolume, qreal dDirection);
	Step(const QString& id, const QString& szName, qreal dVolume,
		 qreal dDirection, QPoint v2ImagePos,
		 const Resource::Name& resImage, const Resource::Name& resAudio);
	Step(const Step& other);
	Step(Step&& other);
	Step& operator=(const Step& other);
	Step& operator=(Step&& other) Q_DECL_NOEXCEPT;


	const QString& id() const;
	const QString& name() const Q_DECL_NOTHROW;
	qreal volume() const Q_DECL_NOTHROW;
	qreal direction() const Q_DECL_NOTHROW;

	const Resource::Name& imageName() const Q_DECL_NOTHROW;
	const Resource::Name& audioName() const Q_DECL_NOTHROW;
	const QPixmap& image() const Q_DECL_NOTHROW;
	const AudioData& audio() const Q_DECL_NOTHROW;

	const QString& LPSymbol() const Q_DECL_NOTHROW;
	int LPPosition() const Q_DECL_NOTHROW;
	const QString& LPSymbName() const Q_DECL_NOTHROW;
	const QString& LPCommand() const Q_DECL_NOTHROW;


	void setId(const QString& id);
	void setName(const QString& szName) Q_DECL_NOTHROW;
	void setVolume(qreal dVolume) Q_DECL_NOTHROW;
	void setDirection(qreal dDirection) Q_DECL_NOTHROW;

	void setImageName(const Resource::Name& resImage) Q_DECL_NOTHROW;
	void setAudioName(const Resource::Name& resAudio) Q_DECL_NOTHROW;

	void setLPSymbol(const QString& szLPSymbol) Q_DECL_NOTHROW;
	void setLPPosition(int iLPPosition) Q_DECL_NOTHROW;
	void setLPSymbName(const QString& szLPSymbName) Q_DECL_NOTHROW;
	void setLPCommand(const QString& szLPCommand) Q_DECL_NOTHROW;

	bool loadImage(Resource* resource);
	bool loadImage(Resource* resource, const Resource::Name& resImage);
	bool loadAudio(Resource* resource);
	bool loadAudio(Resource* resource, const Resource::Name& resAudio);

	QPoint imagePos() const;
	void setImagePos(const QPoint& imagePos);


private:
	QString m_id;
	QString m_szName;
	qreal m_dVolume;
	qreal m_dDirection;
	QPoint m_v2ImagePos;

	Resource::Name m_resImage;
	Resource::Name m_resAudio;
	QPixmap m_image;
	AudioData m_audio;

	// music score Lilypond (LP)
	QString m_szLPSymbol;
	int m_iLPPosition;
	QString m_szLPSymbName;
	QString m_szLPCommand;
};

#endif // STEP_H_INCLUDED
