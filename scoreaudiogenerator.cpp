/**
 * @file scoreaudiogenerator.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-07
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scoreaudiogenerator.h"

#include "score.h"
#include "scoresection.h"
#include "step.h"

const float ACCENT_VOLUME(3.f);

ScoreAudioGenerator::ScoreAudioGenerator(QAudioFormat format, QObject* parent):
	AudioGenerator(format, parent)
{ }

void ScoreAudioGenerator::setScore(const Score* pScore,
								   const ScoreIndex& startPos)
{
	qint32 sample(0);
	quint32 tempo(80);
	float scoreVolume(1.f);
	for(quint32 i=0; i<pScore->cdata()->beatCount(); ++i)
	{
		const ScoreBeat& beat(pScore->cdata()->beatAt(i));
		for(quint32 j=0; j<beat.usize(); ++j)
		{
			const ScoreAtom& atom(beat.cvalueOfIndex(j));

			if(startPos == pScore->cdata()->index(i, Subdiv(j, beat.usize())))
				setPos(sample);

			const Step* step(atom.step());
			if(atom.tempo())
				tempo = static_cast<quint32>(*atom.tempo());
			if(atom.volume())
				scoreVolume = *atom.volume();
			if(step)
			{
				float stepVolume(static_cast<float>(step->volume()));
				stepVolume *= scoreVolume;
				if(atom.haveAccent())
					stepVolume *= ACCENT_VOLUME;

				AudioSource source(&step->audio());
				source.setDirection(static_cast<float>(step->direction()));
				source.setVolume(stepVolume);
				source.setSampleBegin(sample);
				addSource(source);
			}
			quint32 sampleMin(static_cast<quint32>(60*format().sampleRate()));
			sample += sampleMin/(tempo*beat.usize());
		}
	}
	setLoopPoint(sample);
}
