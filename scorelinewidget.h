#ifndef SCORELINEWIDGET_H_INCLUDED
#define SCORELINEWIDGET_H_INCLUDED
/**
 * @file scorelinewidget.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-22
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QVector>
#include <QtWidgets/QWidget>

#include "scoregroup.h"
#include "scoreindex.h"

class RegularLayout;
class ScoreLines;
class ScoreAtom;
class ScoreAtomWidget;

class ScoreLineWidget : public QWidget
{
	Q_OBJECT
public:
	static QBrush highlightBeat();
	static QBrush highlightAtom();

public:
	explicit ScoreLineWidget(ScoreLines* pParent);

	int beatCount() const;
	void setBeatCount(int iBeatCount);

	int computeHeight();

	int atomNum(ScoreIndex idx);
	int beatNum(ScoreIndex idx);
	QRectF atomRect(int beat, int atm);
	QRectF beatRect(int beat);
	ScoreIndex indexBeatAt(QPoint pos);
	ScoreIndex indexAt(QPoint pos);
	ScoreIndex nearestIndexBeatAt(QPoint pos);
	ScoreIndex nearestIndexAt(QPoint pos);
	ScoreIndex nearestInterIndexAt(QPoint pos);

public slots:
	void changeSelection(ScoreGroup group);
	void changeCursor(std::size_t cursorName, ScoreIndex idxPos);

signals:
	void beatCountChanged(int);

protected:
	virtual void paintEvent(QPaintEvent* event) Q_DECL_OVERRIDE;
	virtual void mousePressEvent(QMouseEvent* event) Q_DECL_OVERRIDE;
	virtual void dragEnterEvent(QDragEnterEvent* event) Q_DECL_OVERRIDE;
	virtual void dragMoveEvent(QDragMoveEvent* event) Q_DECL_OVERRIDE;
	virtual void dragLeaveEvent(QDragLeaveEvent* event) Q_DECL_OVERRIDE;
	virtual void dropEvent(QDropEvent* event) Q_DECL_OVERRIDE;

private:
	void initAtoms();

private:
	ScoreLines* m_pParent;
	RegularLayout* m_pLayout;
	QVector<RegularLayout*> m_lstBeatLayout;
	QVector<ScoreAtomWidget*> m_lstAtoms;
	int m_iBeatCount;
};

#endif // SCORELINEWIDGET_H_INCLUDED
