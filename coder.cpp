/**
 * @file coder.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-12
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "coder.h"

bool Coder::encode(QIODevice* input, QAudioFormat format, const QString& output)
{
	QStringList args;
	// format (raw)
	args << QStringLiteral("-f") << QStringLiteral("s16le");
	// codec
	args << QStringLiteral("-c:a") << QStringLiteral("pcm_s16le");
	// channel count
	args << QStringLiteral("-ac") << QString("%1").arg(format.channelCount());
	// sample rate
	args << QStringLiteral("-ar") << QString("%1").arg(format.sampleRate());
	// input
	args << QStringLiteral("-i") << QStringLiteral("pipe:0");
	// output
	args << output;

	QProcess* process(ffmpeg(args));

	if(process == Q_NULLPTR)
		return false;

	if(!sendData(process, input))
	{
		delete process;
		return false;
	}

	process->waitForFinished();

	bool result(process->exitStatus() == QProcess::NormalExit);
	delete process;
	return result;
}

QProcess* Coder::ffmpeg(const QStringList& args)
{
	QProcess* process(new QProcess);
	process->setProgram(QStringLiteral("ffmpeg"));
	process->setArguments(args);
	process->start();

	if(!process->waitForStarted())
	{
		delete process;
		process = Q_NULLPTR;
	}
	return process;
}

bool Coder::sendData(QProcess* process, QIODevice* input)
{
	while(!input->atEnd())
	{
		char chunk[CHUNK_SIZE];
		qint64 len(input->read(chunk, CHUNK_SIZE));
		process->write(chunk, len);
	}
	process->closeWriteChannel();
	return true;
}

bool Coder::receiveData(QProcess* process, QByteArray* output)
{
	while(process->waitForReadyRead())
		output->append(process->read(process->bytesAvailable()));
	return true;
}

bool Coder::decode(QIODevice* input, QAudioFormat format, QByteArray* output)
{
	QStringList args;
	// input
	args << QStringLiteral("-i") << QStringLiteral("pipe:0");
	// format (raw)
	args << QStringLiteral("-f") << QStringLiteral("s16le");
	// codec
	args << QStringLiteral("-c:a") << QStringLiteral("pcm_s16le");
	// channel count
	args << QStringLiteral("-ac") << QString("%1").arg(format.channelCount());
	// sample rate
	args << QStringLiteral("-ar") << QString("%1").arg(format.sampleRate());
	// output
	args << QStringLiteral("pipe:1");

	QProcess* process(ffmpeg(args));

	if(process == Q_NULLPTR)
		return false;

	if(!sendData(process, input))
	{
		delete process;
		return false;
	}
	if(!receiveData(process, output))
	{
		delete process;
		return false;
	}

	process->waitForFinished();

	bool result(process->exitStatus() == QProcess::NormalExit);
	delete process;
	return result;
}
