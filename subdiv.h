#ifndef SUBDIV_H_INCLUDED
#define SUBDIV_H_INCLUDED
/**
 * @file subdiv.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-09
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QtGlobal>

QT_BEGIN_NAMESPACE
template<class T>
class QLinkedList;
QT_END_MOC_NAMESPACE

class Subdiv
{
public:
	static const Subdiv INVALID;
	static const Subdiv ZERO;
	static const Subdiv ONE;

	static quint32 common(const Subdiv& first,
						  const Subdiv& second) Q_DECL_NOTHROW;

	static quint32 common(const QLinkedList<Subdiv>& list) Q_DECL_NOTHROW;

public:
	Subdiv() Q_DECL_NOTHROW;
	Subdiv(quint32 uNum, quint32 uDen) Q_DECL_NOTHROW;
	Subdiv(const Subdiv& other) Q_DECL_NOTHROW;
	Subdiv& operator=(const Subdiv& other) Q_DECL_NOTHROW;

	quint32 num() const Q_DECL_NOTHROW;
	quint32 den() const Q_DECL_NOTHROW;

	void setNum(quint32 uNum) Q_DECL_NOTHROW;
	void setDen(quint32 uDen) Q_DECL_NOTHROW;

	bool isNull() const Q_DECL_NOTHROW;
	bool isOne() const Q_DECL_NOTHROW;
	bool isValid() const Q_DECL_NOTHROW;

	bool isConvertible(quint32 uDen) const Q_DECL_NOTHROW;
	quint32 convertTo(quint32 uDen) const Q_DECL_NOTHROW;
	void simplify() Q_DECL_NOTHROW;
	Subdiv simplified() const Q_DECL_NOTHROW;

	bool operator==(const Subdiv& other) const Q_DECL_NOTHROW;
	Subdiv operator+(const Subdiv& other) const Q_DECL_NOTHROW;
	Subdiv operator-(const Subdiv& other) const Q_DECL_NOTHROW;
	Subdiv operator*(const Subdiv& other) const Q_DECL_NOTHROW;

private:
	quint32 m_uNum;
	quint32 m_uDen;
};

#endif // SUBDIV_H_INCLUDED
