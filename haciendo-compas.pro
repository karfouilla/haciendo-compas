#-------------------------------------------------
#
# Project created by QtCreator 2019-06-08T19:26:02
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = haciendo-compas
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

RC_FILE = haciendo-compas.rc

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    resource.cpp \
    step.cpp \
    subdiv.cpp \
    scoreatom.cpp \
    scoresection.cpp \
    scoreindex.cpp \
    scorebeat.cpp \
    scoregroup.cpp \
    regularlayout.cpp \
    scoreatomwidget.cpp \
    scorelines.cpp \
    scorelinewidget.cpp \
    scorewidget.cpp \
    score.cpp \
    patern.cpp \
    scorewriter.cpp \
    scorereader.cpp \
    audiogenerator.cpp \
    audiosource.cpp \
    audiodata.cpp \
    scoreaudiogenerator.cpp \
    paternwidget.cpp \
    paternlistwidget.cpp \
    dockscore.cpp \
    coder.cpp

HEADERS += \
    mainwindow.h \
    resource.h \
    step.h \
    subdiv.h \
    scoreatom.h \
    scoresection.h \
    scorebeat.h \
    scoreindex.h \
    scoregroup.h \
    regularlayout.h \
    scoreatomwidget.h \
    scorelines.h \
    scorelinewidget.h \
    scorewidget.h \
    score.h \
    patern.h \
    scorewriter.h \
    scorereader.h \
    audiogenerator.h \
    audiosource.h \
    audiodata.h \
    scoreaudiogenerator.h \
    paternwidget.h \
    paternlistwidget.h \
    dockscore.h \
    coder.h

TRANSLATIONS = fr.ts

*clang* {
    QMAKE_CXXFLAGS += -Wabi
    QMAKE_CXXFLAGS += -Waddress-of-temporary
    QMAKE_CXXFLAGS += -Waddress
    QMAKE_CXXFLAGS += -Waggregate-return
    QMAKE_CXXFLAGS += -Wall
    QMAKE_CXXFLAGS += -Wambiguous-member-template
    QMAKE_CXXFLAGS += -Warc-abi
    QMAKE_CXXFLAGS += -Warc-non-pod-memaccess
    QMAKE_CXXFLAGS += -Warc-retain-cycles
    QMAKE_CXXFLAGS += -Warc-unsafe-retained-assign
    QMAKE_CXXFLAGS += -Warc
    QMAKE_CXXFLAGS += -Watomic-properties
    QMAKE_CXXFLAGS += -Wattributes
    QMAKE_CXXFLAGS += -Wavailability
    QMAKE_CXXFLAGS += -Wbad-function-cast
    QMAKE_CXXFLAGS += -Wbind-to-temporary-copy
    QMAKE_CXXFLAGS += -Wbitwise-op-parentheses
    QMAKE_CXXFLAGS += -Wbool-conversions
    QMAKE_CXXFLAGS += -Wbuiltin-macro-redefined
    QMAKE_CXXFLAGS += -Wc++-compat
    QMAKE_CXXFLAGS += -Wc++0x-compat
    QMAKE_CXXFLAGS += -Wc++0x-extensions
    QMAKE_CXXFLAGS += -Wcast-align
    QMAKE_CXXFLAGS += -Wcast-qual
    QMAKE_CXXFLAGS += -Wchar-align
    QMAKE_CXXFLAGS += -Wchar-subscripts
    QMAKE_CXXFLAGS += -Wcomment
    QMAKE_CXXFLAGS += -Wcomments
    QMAKE_CXXFLAGS += -Wconditional-uninitialized
    QMAKE_CXXFLAGS += -Wconversion
    QMAKE_CXXFLAGS += -Wctor-dtor-privacy
    QMAKE_CXXFLAGS += -Wcustom-atomic-properties
    QMAKE_CXXFLAGS += -Wdeclaration-after-statement
    QMAKE_CXXFLAGS += -Wdefault-arg-special-member
    QMAKE_CXXFLAGS += -Wdelegating-ctor-cycles
    QMAKE_CXXFLAGS += -Wdelete-non-virtual-dtor
    QMAKE_CXXFLAGS += -Wdeprecated-declarations
    QMAKE_CXXFLAGS += -Wdeprecated-implementations
    QMAKE_CXXFLAGS += -Wdeprecated-writable-strings
    QMAKE_CXXFLAGS += -Wdeprecated
    QMAKE_CXXFLAGS += -Wdisabled-optimization
    QMAKE_CXXFLAGS += -Wdiscard-qual
    QMAKE_CXXFLAGS += -Wdiv-by-zero
    QMAKE_CXXFLAGS += -Wduplicate-method-arg
    QMAKE_CXXFLAGS += -Weffc++
    QMAKE_CXXFLAGS += -Wempty-body
    QMAKE_CXXFLAGS += -Wendif-labels
    QMAKE_CXXFLAGS += -Wexit-time-destructors
    QMAKE_CXXFLAGS += -Wextra-tokens
    QMAKE_CXXFLAGS += -Wextra
    QMAKE_CXXFLAGS += -Wformat-extra-args
    QMAKE_CXXFLAGS += -Wformat-nonliteral
    QMAKE_CXXFLAGS += -Wformat-zero-length
    QMAKE_CXXFLAGS += -Wformat
    QMAKE_CXXFLAGS += -Wformat=2
    QMAKE_CXXFLAGS += -Wfour-char-constants
    QMAKE_CXXFLAGS += -Wglobal-constructors
    QMAKE_CXXFLAGS += -Wgnu-designator
    QMAKE_CXXFLAGS += -Wgnu
    QMAKE_CXXFLAGS += -Wheader-hygiene
    QMAKE_CXXFLAGS += -Widiomatic-parentheses
    QMAKE_CXXFLAGS += -Wignored-qualifiers
    QMAKE_CXXFLAGS += -Wimplicit-atomic-properties
    QMAKE_CXXFLAGS += -Wimplicit-function-declaration
    QMAKE_CXXFLAGS += -Wimplicit-int
    QMAKE_CXXFLAGS += -Wimplicit
    QMAKE_CXXFLAGS += -Wimport
    QMAKE_CXXFLAGS += -Wincompatible-pointer-types
    QMAKE_CXXFLAGS += -Winit-self
    QMAKE_CXXFLAGS += -Winitializer-overrides
    QMAKE_CXXFLAGS += -Winline
    QMAKE_CXXFLAGS += -Wint-to-pointer-cast
    QMAKE_CXXFLAGS += -Winvalid-offsetof
    QMAKE_CXXFLAGS += -Winvalid-pch
    QMAKE_CXXFLAGS += -Wlarge-by-value-copy
    QMAKE_CXXFLAGS += -Wliteral-range
    QMAKE_CXXFLAGS += -Wlocal-type-template-args
    QMAKE_CXXFLAGS += -Wlogical-op-parentheses
    QMAKE_CXXFLAGS += -Wlong-long
    QMAKE_CXXFLAGS += -Wmain
    QMAKE_CXXFLAGS += -Wmicrosoft
    QMAKE_CXXFLAGS += -Wmismatched-tags
    QMAKE_CXXFLAGS += -Wmissing-braces
    QMAKE_CXXFLAGS += -Wmissing-declarations
    QMAKE_CXXFLAGS += -Wmissing-field-initializers
    QMAKE_CXXFLAGS += -Wmissing-format-attribute
    QMAKE_CXXFLAGS += -Wmissing-include-dirs
    QMAKE_CXXFLAGS += -Wmissing-noreturn
    QMAKE_CXXFLAGS += -Wmost
    QMAKE_CXXFLAGS += -Wmultichar
    QMAKE_CXXFLAGS += -Wnested-externs
    QMAKE_CXXFLAGS += -Wnewline-eof
    QMAKE_CXXFLAGS += -Wnon-gcc
    QMAKE_CXXFLAGS += -Wnon-virtual-dtor
    QMAKE_CXXFLAGS += -Wnonfragile-abi2
    QMAKE_CXXFLAGS += -Wnonnull
    QMAKE_CXXFLAGS += -Wnonportable-cfstrings
    QMAKE_CXXFLAGS += -Wnull-dereference
    QMAKE_CXXFLAGS += -Wobjc-nonunified-exceptions
    QMAKE_CXXFLAGS += -Wold-style-cast
    QMAKE_CXXFLAGS += -Wold-style-definition
    QMAKE_CXXFLAGS += -Wout-of-line-declaration
    QMAKE_CXXFLAGS += -Woverflow
    QMAKE_CXXFLAGS += -Woverlength-strings
    QMAKE_CXXFLAGS += -Woverloaded-virtual
    QMAKE_CXXFLAGS += -Wpacked
#   QMAKE_CXXFLAGS += -Wpadded
    QMAKE_CXXFLAGS += -Wparentheses
    QMAKE_CXXFLAGS += -Wpointer-arith
    QMAKE_CXXFLAGS += -Wpointer-to-int-cast
    QMAKE_CXXFLAGS += -Wprotocol
    QMAKE_CXXFLAGS += -Wreadonly-setter-attrs
    QMAKE_CXXFLAGS += -Wredundant-decls
    QMAKE_CXXFLAGS += -Wreorder
    QMAKE_CXXFLAGS += -Wreturn-type
    QMAKE_CXXFLAGS += -Wself-assign
    QMAKE_CXXFLAGS += -Wsemicolon-before-method-body
    QMAKE_CXXFLAGS += -Wsequence-point
    QMAKE_CXXFLAGS += -Wshadow
    QMAKE_CXXFLAGS += -Wshorten-64-to-32
    QMAKE_CXXFLAGS += -Wsign-compare
    QMAKE_CXXFLAGS += -Wsign-promo
    QMAKE_CXXFLAGS += -Wsizeof-array-argument
    QMAKE_CXXFLAGS += -Wstack-protector
    QMAKE_CXXFLAGS += -Wstrict-aliasing
    QMAKE_CXXFLAGS += -Wstrict-overflow
    QMAKE_CXXFLAGS += -Wstrict-prototypes
    QMAKE_CXXFLAGS += -Wstrict-selector-match
    QMAKE_CXXFLAGS += -Wsuper-class-method-mismatch
    QMAKE_CXXFLAGS += -Wswitch-default
    QMAKE_CXXFLAGS += -Wswitch-enum
    QMAKE_CXXFLAGS += -Wswitch
    QMAKE_CXXFLAGS += -Wsynth
    QMAKE_CXXFLAGS += -Wtautological-compare
    QMAKE_CXXFLAGS += -Wtrigraphs
    QMAKE_CXXFLAGS += -Wtype-limits
    QMAKE_CXXFLAGS += -Wundeclared-selector
    QMAKE_CXXFLAGS += -Wuninitialized
    QMAKE_CXXFLAGS += -Wunknown-pragmas
    QMAKE_CXXFLAGS += -Wunnamed-type-template-args
    QMAKE_CXXFLAGS += -Wunneeded-internal-declaration
    QMAKE_CXXFLAGS += -Wunneeded-member-function
    QMAKE_CXXFLAGS += -Wunused-argument
    QMAKE_CXXFLAGS += -Wunused-exception-parameter
    QMAKE_CXXFLAGS += -Wunused-function
    QMAKE_CXXFLAGS += -Wunused-label
    QMAKE_CXXFLAGS += -Wunused-member-function
    QMAKE_CXXFLAGS += -Wunused-parameter
    QMAKE_CXXFLAGS += -Wunused-value
    QMAKE_CXXFLAGS += -Wunused-variable
    QMAKE_CXXFLAGS += -Wunused
    QMAKE_CXXFLAGS += -Wused-but-marked-unused
    QMAKE_CXXFLAGS += -Wvariadic-macros
    QMAKE_CXXFLAGS += -Wvector-conversions
    QMAKE_CXXFLAGS += -Wvla
    QMAKE_CXXFLAGS += -Wvolatile-register-var
    QMAKE_CXXFLAGS += -Wwrite-strings
}

RESOURCES += \
    default.qrc

FORMS += \
    dockscore.ui
