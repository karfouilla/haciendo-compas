#ifndef PATERN_H_INCLUDED
#define PATERN_H_INCLUDED
/**
 * @file patern.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-04
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QList>

#include "scoreatom.h"

class Patern
{
public:
	Patern();
	Patern(const QString& id, const QString& szName);
	Patern(const Patern& other);
	Patern(Patern&& other);
	Patern& operator=(const Patern& other);
	Patern& operator=(Patern&& other);

	const QString& id() const;
	const QString& name() const;

	void setId(const QString& id);
	void setName(const QString& szName);

	const QList<ScoreAtom>& catoms() const;
	const ScoreAtom& catom(int i) const;
	QList<ScoreAtom>& atoms();
	ScoreAtom& atom(int i);

private:
	QString m_id;
	QString m_szName;
	QList<ScoreAtom> m_lstAtoms;
};

#endif // PATERN_H_INCLUDED
