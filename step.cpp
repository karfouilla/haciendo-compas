/**
 * @file step.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-09
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "step.h"

#include <QtCore/QtMath>

#include <QtMultimedia/QAudioDecoder>

// Constructors
Step::Step():
	m_id(),
	m_szName(),
	m_dVolume(1.),
	m_dDirection(M_PI_2),
	m_v2ImagePos(0, 0),
	m_resImage(),
	m_resAudio(),
	m_image(),
	m_audio(),
	m_szLPSymbol(),
	m_iLPPosition(0),
	m_szLPSymbName(),
	m_szLPCommand()
{ }

Step::Step(const QString& id, const QString& szName,
		   qreal dVolume, qreal dDirection):
	m_id(id),
	m_szName(szName),
	m_dVolume(dVolume),
	m_dDirection(dDirection),
	m_v2ImagePos(0, 0),
	m_resImage(),
	m_resAudio(),
	m_image(),
	m_audio(),
	m_szLPSymbol(),
	m_iLPPosition(0),
	m_szLPSymbName(),
	m_szLPCommand()
{ }

Step::Step(const QString& id, const QString& szName, qreal dVolume,
		   qreal dDirection, QPoint v2ImagePos,
		   const Resource::Name& resImage, const Resource::Name& resAudio):
	m_id(id),
	m_szName(szName),
	m_dVolume(dVolume),
	m_dDirection(dDirection),
	m_v2ImagePos(v2ImagePos),
	m_resImage(resImage),
	m_resAudio(resAudio),
	m_image(),
	m_audio(),
	m_szLPSymbol(),
	m_iLPPosition(0),
	m_szLPSymbName(),
	m_szLPCommand()
{ }

Step::Step(const Step& other):
	m_id(other.m_id),
	m_szName(other.m_szName),
	m_dVolume(other.m_dVolume),
	m_dDirection(other.m_dDirection),
	m_v2ImagePos(other.m_v2ImagePos),
	m_resImage(other.m_resImage),
	m_resAudio(other.m_resAudio),
	m_image(other.m_image),
	m_audio(),
	m_szLPSymbol(other.m_szLPSymbol),
	m_iLPPosition(other.m_iLPPosition),
	m_szLPSymbName(other.m_szLPSymbName),
	m_szLPCommand(other.m_szLPCommand)
{ }

Step::Step(Step&& other):
	m_id(std::move(other.m_id)),
	m_szName(std::move(other.m_szName)),
	m_dVolume(other.m_dVolume),
	m_dDirection(other.m_dDirection),
	m_v2ImagePos(other.m_v2ImagePos),
	m_resImage(std::move(other.m_resImage)),
	m_resAudio(std::move(other.m_resAudio)),
	m_image(),
	m_audio(),
	m_szLPSymbol(std::move(other.m_szLPSymbol)),
	m_iLPPosition(other.m_iLPPosition),
	m_szLPSymbName(std::move(other.m_szLPSymbName)),
	m_szLPCommand(std::move(other.m_szLPCommand))
{
	m_image = std::move(other.m_image);
}

Step& Step::operator=(const Step& other)
{
	m_id = other.m_id;
	m_szName = other.m_szName;
	m_dVolume = other.m_dVolume;
	m_dDirection = other.m_dDirection;
	m_v2ImagePos = other.m_v2ImagePos;
	m_resImage = other.m_resImage;
	m_resAudio = other.m_resAudio;
	m_image = other.m_image;
	m_szLPSymbol = other.m_szLPSymbol;
	m_iLPPosition = other.m_iLPPosition;
	m_szLPSymbName = other.m_szLPSymbName;
	m_szLPCommand = other.m_szLPCommand;
	return *this;
}

Step& Step::operator=(Step&& other) Q_DECL_NOEXCEPT
{
	m_id = std::move(other.m_id);
	m_szName = std::move(other.m_szName);
	m_dVolume = other.m_dVolume;
	m_dDirection = other.m_dDirection;
	m_v2ImagePos = other.m_v2ImagePos;
	m_resImage = std::move(other.m_resImage);
	m_resAudio = std::move(other.m_resAudio);
	m_image = std::move(other.m_image);
	m_szLPSymbol = std::move(other.m_szLPSymbol);
	m_iLPPosition = other.m_iLPPosition;
	m_szLPSymbName = std::move(other.m_szLPSymbName);
	m_szLPCommand = std::move(other.m_szLPCommand);
	return *this;
}


// Getters
const QString& Step::id() const
{
	return m_id;
}
const QString& Step::name() const Q_DECL_NOTHROW
{
	return m_szName;
}
qreal Step::volume() const Q_DECL_NOTHROW
{
	return m_dVolume;
}
qreal Step::direction() const Q_DECL_NOTHROW
{
	return m_dDirection;
}

const Resource::Name& Step::imageName() const Q_DECL_NOTHROW
{
	return m_resImage;
}

const Resource::Name& Step::audioName() const Q_DECL_NOTHROW
{
	return m_resAudio;
}

const QPixmap& Step::image() const Q_DECL_NOTHROW
{
	return m_image;
}
const AudioData& Step::audio() const Q_DECL_NOTHROW
{
	return m_audio;
}

const QString& Step::LPSymbol() const Q_DECL_NOTHROW
{
	return m_szLPSymbol;
}
int Step::LPPosition() const Q_DECL_NOTHROW
{
	return m_iLPPosition;
}
const QString& Step::LPSymbName() const Q_DECL_NOTHROW
{
	return m_szLPSymbName;
}
const QString& Step::LPCommand() const Q_DECL_NOTHROW
{
	return m_szLPCommand;
}


// Setters
void Step::setId(const QString& id)
{
	m_id = id;
}
void Step::setLPSymbol(const QString& szLPSymbol) Q_DECL_NOTHROW
{
	m_szLPSymbol = szLPSymbol;
}
void Step::setLPPosition(int iLPPosition) Q_DECL_NOTHROW
{
	m_iLPPosition = iLPPosition;
}
void Step::setLPSymbName(const QString& szLPSymbName) Q_DECL_NOTHROW
{
	m_szLPSymbName = szLPSymbName;
}
void Step::setLPCommand(const QString& szLPCommand) Q_DECL_NOTHROW
{
	m_szLPCommand = szLPCommand;
}

void Step::setImageName(const Resource::Name& resImage) Q_DECL_NOTHROW
{
	m_resImage = resImage;
}
void Step::setAudioName(const Resource::Name& resAudio) Q_DECL_NOTHROW
{
	m_resAudio = resAudio;
}

void Step::setName(const QString& szName) Q_DECL_NOTHROW
{
	m_szName = szName;
}
void Step::setVolume(qreal dVolume) Q_DECL_NOTHROW
{
	m_dVolume = dVolume;
}
void Step::setDirection(qreal dDirection) Q_DECL_NOTHROW
{
	m_dDirection = dDirection;
}

// Load
bool Step::loadImage(Resource* resource)
{
	QIODevice* imageFile(resource->open(m_resImage));
	bool result(m_image.loadFromData(imageFile->readAll()));
	imageFile->close();
	delete imageFile;
	return result;
}
bool Step::loadAudio(Resource* resource)
{
	QIODevice* audioFile(resource->open(m_resAudio));

	QAudioFormat format;
	format.setCodec("audio/pcm");
	format.setChannelCount(1);
	format.setSampleRate(44100);
	format.setSampleSize(16);
	format.setSampleType(QAudioFormat::SignedInt);
	format.setByteOrder(QAudioFormat::LittleEndian);

	m_audio.setFormat(format);
	return m_audio.load(audioFile);
}
bool Step::loadImage(Resource* resource, const Resource::Name& resImage)
{
	setImageName(resImage);
	return loadImage(resource);
}
bool Step::loadAudio(Resource* resource, const Resource::Name& resAudio)
{
	setAudioName(resAudio);
	return loadAudio(resource);
}

QPoint Step::imagePos() const
{
	return m_v2ImagePos;
}

void Step::setImagePos(const QPoint& v2ImagePos)
{
	m_v2ImagePos = v2ImagePos;
}
