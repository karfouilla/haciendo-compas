#ifndef PATERNWIDGET_H_INCLUDED
#define PATERNWIDGET_H_INCLUDED
/**
 * @file paternwidget.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////


#include <QtWidgets/QWidget>

#include "patern.h"

class RegularLayout;
class ScoreAtomWidget;

class PaternWidget : public QWidget
{
	Q_OBJECT
public:
	explicit PaternWidget(const Patern& patern, QWidget* parent = Q_NULLPTR);
	virtual ~PaternWidget() { }

	const Patern& patern() const;
	void setPatern(const Patern& patern);

	void updateWidgets();

signals:
	void triggered(const QString&);

protected:
	virtual void paintEvent(QPaintEvent* event) Q_DECL_OVERRIDE;
	virtual void mousePressEvent(QMouseEvent* event) Q_DECL_OVERRIDE;

private:
	Patern m_patern;
	RegularLayout* m_pLayout;
	QList<ScoreAtomWidget*> m_lstAtoms;
};

#endif // PATERNWIDGET_H_INCLUDED
