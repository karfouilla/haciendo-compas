#ifndef PATERNLISTWIDGET_H_INCLUDED
#define PATERNLISTWIDGET_H_INCLUDED
/**
 * @file paternlistwidget.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////


#include <QtCore/QMap>

#include <QtWidgets/QWidget>
#include <QtWidgets/QVBoxLayout>

#include "patern.h"
#include "paternwidget.h"

class PaternListWidget : public QWidget
{
	Q_OBJECT
public:
	explicit PaternListWidget(const QMap<QString, Patern>& paterns,
							  QWidget* parent = Q_NULLPTR);

	const QMap<QString, Patern>& paterns() const;
	void setPaterns(const QMap<QString, Patern>& mapPaterns);

	void updateWidgets();

signals:
	void triggered(const QString&);

public slots:
	void paternTrigger(const QString& id);

private:
	QMap<QString, Patern> m_mapPaterns;
	QVBoxLayout* m_pLayout;
	QList<PaternWidget*> m_lstWidgets;
};

#endif // PATERNLISTWIDGET_H_INCLUDED
