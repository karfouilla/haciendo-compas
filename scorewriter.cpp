/**
 * @file scorewriter.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-04
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////


#include "scorewriter.h"

#include "scoresection.h"
#include "step.h"

ScoreWriter::ScoreWriter(QIODevice* device):
	m_pFile(device),
	m_writer(m_pFile)
{ }

void ScoreWriter::write(const Score* score)
{
	m_writer.setAutoFormatting(true);
	m_writer.setAutoFormattingIndent(2);

	m_writer.writeStartDocument();
	m_writer.writeStartElement(QStringLiteral("flamenco"));

	writeResource(score->cresource());
	writeSteps(score->csteps());
	writePaterns(score->cpaterns());
	writeSection(score->cdata());

	m_writer.writeEndElement();
	m_writer.writeEndDocument();
}

void ScoreWriter::autoBreakLine(QString& dataString,
								const QByteArray& data64, int num)
{
	dataString.reserve(data64.size() + data64.size()/num);
	int pos(0);
	while(pos < data64.size())
	{
		dataString.append(data64.mid(pos, num));
		dataString.append('\n');
		pos += num;
	}
}

void ScoreWriter::writeResource(const Resource& resource)
{
	typedef QMap<QString, QByteArray> ResData;
	typedef ResData::ConstIterator ResDataCIt;

	const ResData& resDatas(resource.datas());

	m_writer.writeStartElement(QStringLiteral("resources"));

	ResDataCIt cit(resDatas.cbegin());
	while(cit != resDatas.cend())
	{
		m_writer.writeStartElement(QStringLiteral("file"));
		m_writer.writeAttribute(QStringLiteral("name"), cit.key());
		m_writer.writeAttribute(QStringLiteral("type"),
								QStringLiteral("base64"));

		QString dataString;
		autoBreakLine(dataString, cit.value().toBase64(), 80);
		m_writer.writeCharacters(dataString);

		m_writer.writeEndElement();
		++cit;
	}
	m_writer.writeEndElement();
}

void ScoreWriter::writeSteps(const QMap<QString, Step*>& steps)
{
	typedef QMap<QString, Step*> StepsMap;
	typedef StepsMap::ConstIterator StepsMapCIt;

	m_writer.writeStartElement(QStringLiteral("steps"));
	m_writer.writeAttribute(QStringLiteral("count"),
							QString("%1").arg(steps.count()));

	StepsMapCIt cit(steps.cbegin());
	while(cit != steps.cend())
	{
		writeStep(cit.value());
		++cit;
	}

	m_writer.writeEndElement();
}

void ScoreWriter::writeStep(const Step* step)
{
	m_writer.writeStartElement(QStringLiteral("step"));
	m_writer.writeAttribute(QStringLiteral("id"), step->id());

	m_writer.writeTextElement(QStringLiteral("name"), step->name());

	writeResourceName(QStringLiteral("image"), step->imageName());
	writeResourceName(QStringLiteral("audio"), step->audioName());

	m_writer.writeTextElement(QStringLiteral("imgposx"),
							  QString("%1").arg(step->imagePos().x()));
	m_writer.writeTextElement(QStringLiteral("imgposy"),
							  QString("%1").arg(step->imagePos().y()));

	m_writer.writeTextElement(QStringLiteral("volume"),
							  QString("%1").arg(step->volume()));
	m_writer.writeTextElement(QStringLiteral("direction"),
							  QString("%1").arg(step->direction()));

	m_writer.writeTextElement(QStringLiteral("symbol"), step->LPSymbol());
	m_writer.writeTextElement(QStringLiteral("position"),
							  QString("%1").arg(step->LPPosition()));
	m_writer.writeTextElement(QStringLiteral("symbname"), step->LPSymbName());
	m_writer.writeTextElement(QStringLiteral("command"), step->LPCommand());

	m_writer.writeEndElement();
}

void ScoreWriter::writeResourceName(const QString& name,
									const Resource::Name& resName)
{
	// Convert type enum to QString
	QString type;
	switch(resName.type())
	{
	case Resource::TYPE_FILE:
		type = QStringLiteral("file");
		break;
	case Resource::TYPE_XML:
		type = QStringLiteral("xml");
		break;
	default:
		type = QStringLiteral("unknown");
		break;
	}

	m_writer.writeStartElement(name);
	m_writer.writeAttribute(QStringLiteral("type"), type);
	m_writer.writeCharacters(resName.filename());
	m_writer.writeEndElement();
}

void ScoreWriter::writePaterns(const QMap<QString, Patern>& paterns)
{
	typedef QMap<QString, Patern> PaternsMap;
	typedef PaternsMap::ConstIterator PaternsMapCIt;

	m_writer.writeStartElement(QStringLiteral("paterns"));

	PaternsMapCIt cit(paterns.cbegin());
	while(cit != paterns.cend())
	{
		writePatern(cit.value());
		++cit;
	}

	m_writer.writeEndElement();
}

void ScoreWriter::writePatern(const Patern& patern)
{
	m_writer.writeStartElement(QStringLiteral("patern"));
	m_writer.writeAttribute(QStringLiteral("id"), patern.id());
	m_writer.writeAttribute(QStringLiteral("name"), patern.name());

	foreach(const ScoreAtom& atom, patern.catoms())
	{
		writeAtom(atom);
	}

	m_writer.writeEndElement();
}

void ScoreWriter::writeAtom(const ScoreAtom& atom)
{
	m_writer.writeStartElement(QStringLiteral("atom"));

	if(atom.tempo())
	{
		m_writer.writeAttribute(QStringLiteral("tempo"),
								QString("%1").arg(*atom.tempo()));
	}
	if(atom.volume())
	{
		m_writer.writeAttribute(QStringLiteral("volume"),
								QString("%1").arg(*atom.volume()));
	}
	if(atom.haveAccent())
	{
		m_writer.writeAttribute(QStringLiteral("accent"),
								QStringLiteral("true"));
	}
	if(atom.step())
	{
		m_writer.writeCharacters(atom.step()->id());
	}
	// else: empty balise

	m_writer.writeEndElement();
}

void ScoreWriter::writeSection(const ScoreSection* section)
{
	m_writer.writeStartElement(QStringLiteral("score"));

	for(quint32 i=0; i<section->beatCount(); ++i)
	{
		writeBeat(section->beatAt(i));
	}

	m_writer.writeEndElement();
}

void ScoreWriter::writeBeat(const ScoreBeat& beat)
{
	m_writer.writeStartElement(QStringLiteral("beat"));

	for(quint32 i=0; i<beat.usize(); ++i)
	{
		writeAtom(beat.cvalueOfIndex(i));
	}

	m_writer.writeEndElement();
}
