/**
 * @file scorelinewidget.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-22
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scorelinewidget.h"

#include "regularlayout.h"
#include "scoreatomwidget.h"
#include "scorelines.h"

#include <QtGui/QDragEnterEvent>
#include <QtGui/QDragMoveEvent>
#include <QtGui/QDragLeaveEvent>
#include <QtGui/QDropEvent>
#include <QtGui/QPainter>

#include "step.h"
#include "scorewidget.h"

const float BEAT_LINE_WEIGHT(2.f);
const int LINE_MARGINS(6);
const int BEAT_MARGINS(1);
const int BEAT_SPACING(8);
const int ATOM_SPACING(1);

QBrush ScoreLineWidget::highlightBeat()
{
	return QBrush(QColor(0, 255, 128));
}

QBrush ScoreLineWidget::highlightAtom()
{
	return QBrush(QColor(0, 128, 255));
}

ScoreLineWidget::ScoreLineWidget(ScoreLines* pParent):
	QWidget(pParent->scoreWiget()),
	m_pParent(pParent),
	m_pLayout(Q_NULLPTR),
	m_lstBeatLayout(),
	m_lstAtoms(),
	m_iBeatCount(4)
{
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	m_pLayout = new RegularLayout(Qt::Horizontal, this);
	m_pLayout->setContentsMargins(LINE_MARGINS, LINE_MARGINS,
								  LINE_MARGINS, LINE_MARGINS);
	m_pLayout->setSpacing(BEAT_SPACING);
	m_pLayout->setVisualCount(m_iBeatCount);

	initAtoms();

	setLayout(m_pLayout);

	setAcceptDrops(true);
	setBackgroundRole(QPalette::Dark);
	setAutoFillBackground(true);
	setCursor(QCursor(Qt::IBeamCursor));
	setMinimumHeight(computeHeight());

	connect(m_pParent->scoreWiget(), &ScoreWidget::selectionChanged,
			this, &ScoreLineWidget::changeSelection);
	connect(m_pParent->scoreWiget(), &ScoreWidget::scoreCursorChanged,
			this, &ScoreLineWidget::changeCursor);
}

void ScoreLineWidget::paintEvent(QPaintEvent* event)
{
	QWidget::paintEvent(event);

	QRectF rect(contentsRect());

	QPainter painter(this);

	// Selection
	ScoreGroup selection(m_pParent->scoreWiget()->selection());
	QRectF totalSelRect;
	if(selection.begin().section())
	{
		ScoreIndex idx(selection.begin());
		ScoreIndex end(selection.end());
		if(end < idx)
		{
			qSwap(idx, end);
		}
		if(idx.subdiv().isValid())
		{
			while(idx != end)
			{
				int selBeat(beatNum(idx));
				QRectF selRect(atomRect(selBeat, atomNum(idx)));
				if(selRect.isValid())
					totalSelRect = totalSelRect.united(selRect);
				idx = idx.advance(Subdiv(1, idx.beat().usize()));
			}
			painter.setBrush(highlightAtom());
		}
		else
		{
			while(idx != end)
			{
				int selBeat(beatNum(idx));
				QRectF selRect(beatRect(selBeat));
				if(selRect.isValid())
					totalSelRect = totalSelRect.united(selRect);
				idx = idx.advance(ScoreIndex::AM_NEXT_BEAT);
			}
			painter.setBrush(highlightBeat());
		}
		//! @todo REMOVE MAGIC VALUES
		painter.setPen(QPen(painter.brush().color().dark(130), 2.));
		if(totalSelRect.isValid())
		{
			totalSelRect.adjust(-2, -4, 2, 4);
			painter.drawRoundedRect(totalSelRect, 4, 4);
		}
	}

	// Beats lines
	painter.setBrush(Qt::NoBrush);
	painter.setPen(QPen(QBrush(Qt::black), BEAT_LINE_WEIGHT,
						Qt::SolidLine, Qt::RoundCap));
	for(int i=1; i<m_pLayout->visualCount(); ++i)
	{
		QRectF separationRect(m_pLayout->spacerRect(i));
		qreal middleHor(separationRect.center().x());
		QLineF sepLine(middleHor, rect.top() + BEAT_LINE_WEIGHT,
					   middleHor, rect.bottom() - BEAT_LINE_WEIGHT);
		painter.drawLine(sepLine.toLine());
	}

	// Cursor
	for(std::size_t i=0; i<ScoreWidget::CURSOR_LAST; ++i)
	{
		ScoreIndex idx(m_pParent->scoreWiget()->scoreCursor(i));
		if(idx.section() != Q_NULLPTR)
		{
			QRectF cursorRect;
			int selBeat(beatNum(idx));
			if(0 <= selBeat && selBeat < m_iBeatCount) // in this line ?
			{
				// get real position
				if(!idx.subdiv().isValid() || idx.subdiv().isNull()
						|| idx.subdiv().isOne())
				{
					cursorRect = m_pLayout->spacerRect(selBeat);
				}
				else
				{
					int selAtom(atomNum(idx));
					cursorRect = m_lstBeatLayout.at(selBeat)->spacerRect(selAtom);
				}

				// draw settings
				painter.setBrush(Qt::NoBrush);
				//! @todo REMOVE MAGIC VALUES
				if(m_pParent->scoreWiget()->hasFocus())
				{
					QColor color((i == ScoreWidget::CURSOR_DEFAULT) ?
									 Qt::magenta : Qt::green);
					color.setAlphaF(.8);
					painter.setPen(QPen(QBrush(color), 5.));
				}
				else
				{
					QColor color(Qt::darkGray);
					color = color.darker();
					color.setAlphaF(.8);
					painter.setPen(QPen(QBrush(color), 5.));
				}

				// draw
				if(cursorRect.isValid())
				{
					qreal middleHor(cursorRect.center().x());
					QLineF cursorLine(middleHor, rect.top(),
									  middleHor, rect.bottom());
					painter.drawLine(cursorLine);
				}
			}
		}
	}
}

void ScoreLineWidget::mousePressEvent(QMouseEvent* event)
{
	QWidget::mousePressEvent(event);
	if(event->button() == Qt::LeftButton)
	{
		// Index sous le pointeur
		ScoreIndex curPos(nearestInterIndexAt(event->pos()));

		// Selection
		ScoreGroup lastSelection(m_pParent->scoreWiget()->selection());
		ScoreIndex lastCurPos(m_pParent->scoreWiget()->scoreCursor(ScoreWidget::CURSOR_DEFAULT));
		ScoreGroup selection;
		if((event->modifiers() & Qt::ShiftModifier))
		{
			if(!lastSelection.begin().section())
				selection.setBegin(lastCurPos);
			else
				selection.setBegin(lastSelection.begin());
		}
		else
		{
			selection.setBegin(curPos);
		}
		selection.setEnd(curPos);

		m_pParent->scoreWiget()->setSelection(selection);

		// Cursors
		for(std::size_t cursorName : {ScoreWidget::CURSOR_DEFAULT, ScoreWidget::CURSOR_PLAY})
		{
			m_pParent->scoreWiget()->setScoreCursor(cursorName, curPos, false);
		}

		event->accept();
	}
}

void ScoreLineWidget::dragEnterEvent(QDragEnterEvent* event)
{
	QWidget::dragEnterEvent(event);
}

void ScoreLineWidget::dragMoveEvent(QDragMoveEvent* event)
{
	QWidget::dragMoveEvent(event);
}

void ScoreLineWidget::dragLeaveEvent(QDragLeaveEvent* event)
{
	QWidget::dragLeaveEvent(event);
}

void ScoreLineWidget::dropEvent(QDropEvent* event)
{
	QWidget::dropEvent(event);
}

void ScoreLineWidget::initAtoms()
{
	ScoreIndex idx(m_pParent->begin());
	ScoreIndex end(m_pParent->end());
	while(idx < end)
	{
		RegularLayout* beatLayout(new RegularLayout(Qt::Horizontal));
		beatLayout->setContentsMargins(BEAT_MARGINS, BEAT_MARGINS,
									   BEAT_MARGINS, BEAT_MARGINS);
		beatLayout->setSpacing(ATOM_SPACING);

		idx = idx.advance(ScoreIndex::AM_FIRST_ATOM);
		while(!idx.isEndOfBeat())
		{
			ScoreAtomWidget* atomWidget;
			atomWidget = new ScoreAtomWidget(idx.atom(), this);
			m_lstAtoms.append(atomWidget);
			beatLayout->addWidget(atomWidget);

			idx = idx.advance(ScoreIndex::AM_NEXT_ATOM);
		}
		m_lstBeatLayout.append(beatLayout);
		m_pLayout->addItem(beatLayout);

		idx = idx.advance(ScoreIndex::AM_NEXT_BEAT);
	}
}

int ScoreLineWidget::beatCount() const
{
	return m_iBeatCount;
}

void ScoreLineWidget::setBeatCount(int iBeatCount)
{
	m_iBeatCount = iBeatCount;
	m_pLayout->setVisualCount(m_iBeatCount);
	emit beatCountChanged(iBeatCount);
}

int ScoreLineWidget::computeHeight()
{
	int margins(2*LINE_MARGINS);
	int inner(ScoreAtomWidget::nominalSize().height());
	int innerMargins(2*BEAT_MARGINS);
	return (inner+innerMargins+margins);
}

int ScoreLineWidget::atomNum(ScoreIndex idx)
{
	return static_cast<int>(idx.subdiv().convertTo(idx.beat().usize()));
}

int ScoreLineWidget::beatNum(ScoreIndex idx)
{
	quint32 absNum(idx.beatIndex());
	quint32 absBegin(m_pParent->begin().beatIndex());
	int num(static_cast<int>(absNum) - static_cast<int>(absBegin));
	return num;
}

QRectF ScoreLineWidget::atomRect(int beat, int atm)
{
	if(0 <= beat && beat < m_iBeatCount)
	{
		RegularLayout* beatLayout(m_lstBeatLayout.at(beat));
		if(0 <= atm && atm < beatLayout->count())
			return beatLayout->itemRect(atm);
	}
	return QRectF();
}

QRectF ScoreLineWidget::beatRect(int beat)
{
	if(0 <= beat && beat < m_iBeatCount)
		return m_pLayout->itemRect(beat);
	else
		return QRectF();
}

ScoreIndex ScoreLineWidget::indexBeatAt(QPoint pos)
{
	int lineIdx(m_pLayout->itemAtPos(pos));
	if(lineIdx < 0)
		return ScoreIndex();
	else
	{
		Subdiv adv(static_cast<quint32>(lineIdx), 1);
		return m_pParent->begin().advance(adv);
	}
}

ScoreIndex ScoreLineWidget::indexAt(QPoint pos)
{
	int inLineIdx(m_pLayout->itemAtPos(pos));
	if(inLineIdx < 0)
	{
		return ScoreIndex();
	}
	else
	{
		quint32 uinLineIdx(static_cast<quint32>(inLineIdx));
		int itmIdx(m_lstBeatLayout.at(inLineIdx)->itemAtPos(pos));
		if(itmIdx < 0)
		{
			return ScoreIndex();
		}
		else
		{
			quint32 atmIdx(static_cast<quint32>(itmIdx));
			ScoreIndex idxBeat(m_pParent->begin());
			idxBeat = idxBeat.advance(Subdiv(uinLineIdx, 1));
			return idxBeat.advance(Subdiv(atmIdx, idxBeat.beat().usize()));
		}
	}
}

ScoreIndex ScoreLineWidget::nearestIndexBeatAt(QPoint pos)
{
	if(m_pParent->begin() == m_pParent->end())
		return m_pParent->begin();

	int inLineIdx(m_pLayout->nearestItemAtPos(pos));
	quint32 uInLineIdx(static_cast<quint32>(inLineIdx));
	return m_pParent->begin().advance(Subdiv(uInLineIdx, 1));
}

ScoreIndex ScoreLineWidget::nearestIndexAt(QPoint pos)
{
	if(m_pParent->begin() == m_pParent->end())
		return m_pParent->begin();

	int inLineIdx(m_pLayout->nearestItemAtPos(pos));
	quint32 uInLineIdx(static_cast<quint32>(inLineIdx));
	int itmIdx(m_lstBeatLayout.at(inLineIdx)->nearestItemAtPos(pos));
	quint32 uItmIdx(static_cast<quint32>(itmIdx));
	ScoreIndex beatIdx(m_pParent->begin().advance(Subdiv(uInLineIdx, 1)));
	return beatIdx.advance(Subdiv(uItmIdx, beatIdx.beat().usize()));
}

ScoreIndex ScoreLineWidget::nearestInterIndexAt(QPoint pos)
{
	if(m_pParent->begin() == m_pParent->end())
		return m_pParent->begin();

	int inLineIdx(m_pLayout->nearestItemAtPos(pos));
	quint32 uInLineIdx(static_cast<quint32>(inLineIdx));
	int itmIdx(m_lstBeatLayout.at(inLineIdx)->nearestSpacerAtPos(pos));
	quint32 uItmIdx(static_cast<quint32>(itmIdx));
	ScoreIndex beatIdx(m_pParent->begin().advance(Subdiv(uInLineIdx, 1)));
	return beatIdx.advance(Subdiv(uItmIdx, beatIdx.beat().usize()));
}

void ScoreLineWidget::changeSelection(ScoreGroup group)
{
	Q_UNUSED(group);
	repaint();
}

void ScoreLineWidget::changeCursor(std::size_t cursorName, ScoreIndex idxPos)
{
	Q_UNUSED(cursorName);
	Q_UNUSED(idxPos);
	repaint();
}
