/**
 * @file audiogenerator.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-06
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////


#include "audiogenerator.h"

#include <cstring>
#include <iostream>

AudioGenerator::AudioGenerator(QAudioFormat format, QObject* parent):
	QIODevice(parent),
	m_iPos(0),
	m_iLen(0),
	m_iRepeat(0),
	m_iLoopPoint(-1),
	m_format(format),
	m_lstSources()
{ }

void AudioGenerator::addSource(const AudioSource& source)
{
	m_lstSources.append(source);
	if(source.sampleEnd() > m_iLen)
		m_iLen = source.sampleEnd();
}

void AudioGenerator::setLoopPoint(qint32 iLoopPoint)
{
	m_iLoopPoint = iLoopPoint;
}

qint32 AudioGenerator::length() const
{
	qint32 len(m_iLen);
	if(m_iRepeat >= 0)
		len += m_iLoopPoint*m_iRepeat;

	return len;
}

qint64 AudioGenerator::readData(char* data, qint64 maxlen)
{
	// Prendre en compte loopPoint
	if((openMode() & QIODevice::ReadOnly) == QIODevice::NotOpen)
		return -1;

	std::memset(data, 0, static_cast<size_t>(maxlen));

	qint32 dstBegin(m_iPos);
	qint32 dstSize(m_format.framesForBytes(static_cast<qint32>(maxlen)));
	qint32 dstEnd(dstBegin + dstSize);

	qint32 loopPoint((m_iLoopPoint < 0) ? m_iLen : m_iLoopPoint);

	qint32 repeatTimeBegin(dstBegin/loopPoint);
	qint32 repeatTimeEnd(dstEnd/loopPoint);
	qint32 maxSize(0);

	if(m_iRepeat >= 0)
	{
		repeatTimeBegin = qMin(repeatTimeBegin, m_iRepeat);
		repeatTimeEnd = qMin(repeatTimeEnd, m_iRepeat);
	}

	for(int i=repeatTimeBegin; i<=repeatTimeEnd; ++i)
	{
		// Pour chaque répétition
		qint32 dstBeginRep(dstBegin - i*loopPoint);
		qint32 dstEndRep(dstBeginRep+dstSize);
		maxSize = qMax(maxSize, readSources(data, dstBeginRep, dstEndRep));
	}

	if(m_iRepeat >= 0)
	{
		maxSize = qMin(length()-m_iPos, dstSize);
	}
	else
	{
		maxSize = dstSize;
	}

	m_iPos += maxSize;

	return m_format.bytesForFrames(maxSize);
}

qint64 AudioGenerator::writeData(const char* data, qint64 len)
{
	Q_UNUSED(data);
	Q_UNUSED(len);

	return 0;
}

void AudioGenerator::setPos(qint32 iPos) noexcept
{
	m_iPos = iPos;
}

qint32 AudioGenerator::readSources(char* data, qint32 dstBegin, qint32 dstEnd)
{
	qint32 maxEnd(0);
	if(dstEnd - dstBegin < 0)
		return maxEnd;

	foreach(const AudioSource& source, m_lstSources)
	{
		qint32 srcBegin(source.sampleBegin());
		qint32 srcEnd(source.sampleEnd());

		if(dstBegin < srcEnd && srcBegin < dstEnd)
		{
			qint32 begin(qMax(srcBegin, dstBegin));
			qint32 end(qMin(srcEnd, dstEnd));

			//        dstdata dstFormat     dstPos          srcPos        count
			source.read(data, m_format, begin-dstBegin, begin-srcBegin, end-begin);
			maxEnd = qMax(maxEnd, end-dstBegin);
		}
	}
	return maxEnd;
}

void AudioGenerator::setRepeat(const qint32& iRepeat)
{
	if(iRepeat > 0)
		m_iRepeat = iRepeat-1;
	else
		m_iRepeat = -1;
}

QAudioFormat AudioGenerator::format() const
{
	return m_format;
}

void AudioGenerator::setFormat(const QAudioFormat& _format)
{
	m_format = _format;
}

qint64 AudioGenerator::size() const
{
	return m_format.bytesForFrames(length());
}

bool AudioGenerator::isSequential() const
{
	return false;
}

qint64 AudioGenerator::pos() const
{
	return m_format.bytesForFrames(m_iPos);
}

bool AudioGenerator::seek(qint64 lPos)
{
	if(!QIODevice::seek(lPos))
		return false;

	m_iPos = m_format.framesForBytes(static_cast<qint32>(lPos));
	return true;
}
