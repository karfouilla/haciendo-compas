/**
 * @file scoregroup.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-20
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scoregroup.h"

ScoreGroup::ScoreGroup() Q_DECL_NOTHROW:
	m_idxBegin(),
	m_idxEnd()
{ }

ScoreGroup::ScoreGroup(const ScoreGroup& other) Q_DECL_NOTHROW:
	m_idxBegin(other.m_idxBegin),
	m_idxEnd(other.m_idxEnd)
{ }

ScoreGroup::ScoreGroup(ScoreIndex idxBegin, ScoreIndex idxEnd) Q_DECL_NOTHROW:
	m_idxBegin(idxBegin),
	m_idxEnd(idxEnd)
{ }

ScoreGroup& ScoreGroup::operator=(const ScoreGroup& other) Q_DECL_NOTHROW
{
	m_idxBegin = other.m_idxBegin;
	m_idxEnd = other.m_idxEnd;
	return *this;
}

ScoreIndex ScoreGroup::begin() const Q_DECL_NOTHROW
{
	return m_idxBegin;
}
ScoreIndex ScoreGroup::end() const Q_DECL_NOTHROW
{
	return m_idxEnd;
}

void ScoreGroup::setBegin(const ScoreIndex& idxBegin) Q_DECL_NOTHROW
{
	m_idxBegin = idxBegin;
}
void ScoreGroup::setEnd(const ScoreIndex& idxEnd) Q_DECL_NOTHROW
{
	m_idxEnd = idxEnd;
}

bool ScoreGroup::isEmpty() Q_DECL_NOTHROW
{
	return (m_idxBegin == m_idxEnd);
}
