/**
 * @file scorebeat.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-11
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scorebeat.h"

#include "scoreatom.h"
#include "subdiv.h"

ScoreBeat ScoreBeat::generate(quint32 division)
{
	ScoreBeat beat;
	beat.reserve(division);
	for(quint32 i=0; i<division; ++i)
		beat.append(ScoreAtom());
	return beat;
}

ScoreBeat::ScoreBeat():
	QList<ScoreAtom>()
{ }
ScoreBeat::ScoreBeat(const ScoreBeat& other):
	QList<ScoreAtom>(other)
{ }
ScoreBeat::ScoreBeat(ScoreBeat&& other):
	QList<ScoreAtom>(other)
{ }

ScoreBeat& ScoreBeat::operator=(const ScoreBeat& other)
{
	QList<ScoreAtom>::operator=(other);
	return *this;
}
ScoreBeat& ScoreBeat::operator=(ScoreBeat&& other)
{
	QList<ScoreAtom>::operator=(other);
	return *this;
}

void ScoreBeat::insertAtom(quint32 i, const ScoreAtom& v)
{
	insert(static_cast<int>(i), v);
}

Subdiv ScoreBeat::insertAtom(Subdiv sub, const ScoreAtom& v)
{
	quint32 idx(indexOfSubdiv(sub));
	insertAtom(idx, v);
	return subdivOfIndex(idx);
}

Subdiv ScoreBeat::removeAtom(Subdiv sub)
{
	quint32 idx(indexOfSubdiv(sub));
	removeAtom(idx);
	return subdivOfIndex(idx);
}

Subdiv ScoreBeat::subdivOfIter(ScoreBeat::CIt citPosition) const
{
	return Subdiv(indexOfIter(citPosition), usize());
}
quint32 ScoreBeat::indexOfIter(ScoreBeat::CIt citPosition) const
{
	return static_cast<quint32>(citPosition-cbegin());
}

ScoreBeat::It ScoreBeat::iterOfSubdiv(Subdiv sub)
{
	return iterOfIndex(indexOfSubdiv(sub));
}
ScoreBeat::CIt ScoreBeat::citerOfSubdiv(Subdiv sub) const
{
	return citerOfIndex(indexOfSubdiv(sub));
}
quint32 ScoreBeat::indexOfSubdiv(Subdiv sub) const
{
	if(sub.isValid())
		return sub.convertTo(usize());
	else
		return 0;
}

ScoreBeat::It ScoreBeat::iterOfIndex(quint32 index)
{
	return (begin()+index);
}
ScoreBeat::CIt ScoreBeat::citerOfIndex(quint32 index) const
{
	return (cbegin()+index);
}
Subdiv ScoreBeat::subdivOfIndex(quint32 index) const
{
	return Subdiv(index, usize());
}

const ScoreAtom& ScoreBeat::cvalueOfIndex(quint32 index) const
{
	return at(static_cast<int>(index));
}

const ScoreAtom& ScoreBeat::cvalueOfIter(ScoreBeat::CIt cit) const
{
	return (*cit);
}
const ScoreAtom& ScoreBeat::cvalueOfSubdiv(Subdiv sub) const
{
	return cvalueOfIndex(indexOfSubdiv(sub));
}

ScoreAtom& ScoreBeat::valueOfIndex(quint32 index)
{
	return operator [](static_cast<int>(index));
}
ScoreAtom& ScoreBeat::valueOfIter(ScoreBeat::It it)
{
	return (*it);
}
ScoreAtom& ScoreBeat::valueOfSubdiv(Subdiv sub)
{
	return valueOfIndex(indexOfSubdiv(sub));
}

ScoreBeatList ScoreBeat::multiply(Subdiv factor) const
{
	if(size() <= 0 || !factor.isValid() || factor.isNull())
		return ScoreBeatList();

	Subdiv resUnit(unit()*factor);
	ScoreBeatList beats;
	beats.append(ScoreBeat());
	ScoreBeat* beat(&beats.last());
	ConstIterator cit(cbegin());
	for(quint32 i=0; i<usize(); ++i)
	{
		if(beat->usize() >= resUnit.den())
		{
			beats.append(ScoreBeat());
			beat = &beats.last();
		}
		beat->append(*cit);
		++cit;
		for(quint32 j=1; j<resUnit.num(); ++j)
		{
			if(beat->usize() >= resUnit.den())
			{
				beats.append(ScoreBeat());
				beat = &beats.last();
			}
			beat->append(ScoreAtom());
		}
	}
	for(quint32 i=beat->usize(); i<resUnit.den(); ++i)
		beat->append(ScoreAtom());
	return beats;
}

void ScoreBeat::divise(quint32 factor)
{
	Q_ASSERT(size() > 0);
	Iterator it(end());
	while(it != begin())
	{
		for(quint32 i = 1; i<factor; ++i)
			it = insert(it, ScoreAtom());
		--it;
	}
}

void ScoreBeat::undivise(quint32 factor)
{
	Q_ASSERT(usize() % factor == 0);

	Iterator it(begin());
	while(it != end())
	{
		++it;
		for(quint32 i=1; i<factor; ++i)
		{
			it = erase(it);
		}
	}
}

void ScoreBeat::merge(const ScoreBeat& other, Subdiv from, Subdiv to)
{
	copyAt(other, from, to, from);
}

void ScoreBeat::copyFrom(const ScoreBeat& other, Subdiv from,
						 Subdiv atFrom, Subdiv atTo)
{
	copyAt(other, from, from+atTo-atFrom, atFrom);
}

void ScoreBeat::copyAt(const ScoreBeat& _other, Subdiv from,
					   Subdiv to, Subdiv at)
{
	ScoreBeat other(_other);
	if(size() <= 0)
		append(ScoreAtom());
	if(other.size() <= 0)
		other.append(ScoreAtom());
	quint32 targetUnit(Subdiv::common({unit(), other.unit(), at.simplified(),
									   from.simplified(), to.simplified()}));

	// convertion to target denominator
	from.setNum(from.convertTo(targetUnit));
	to.setNum(to.convertTo(targetUnit));
	at.setNum(at.convertTo(targetUnit));
	from.setDen(targetUnit);
	to.setDen(targetUnit);
	at.setDen(targetUnit);

	divise(targetUnit/unit().den());
	other.divise(targetUnit/other.unit().den());

	Iterator startTarget(iterOfSubdiv(at));
	ConstIterator start(other.citerOfSubdiv(from));
	ConstIterator finish(other.citerOfSubdiv(to));
	std::copy(start, finish, startTarget);
}

void ScoreBeat::simplify()
{
	QLinkedList<Subdiv> validPos;
	Subdiv position(0, usize());
	for(ConstIterator cit = cbegin(); cit != cend(); ++cit)
	{
		if(cit->step())
			validPos.append(position.simplified());
		position.setNum(position.num()+1);
	}

	if(validPos.isEmpty())
	{
		clear();
		appendAtom(ScoreAtom());
	}
	else
	{
		quint32 newSize(Subdiv::common(validPos));
		quint32 unitSize(usize()/newSize);
		undivise(unitSize);
	}
}
