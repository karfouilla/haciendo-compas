#ifndef AUDIOSOURCE_H_INCLUDED
#define AUDIOSOURCE_H_INCLUDED
/**
 * @file audiosource.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-06
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtMultimedia/QAudioFormat>

#include "audiodata.h"

class AudioSource
{
public:
	AudioSource(const AudioData* pData);

	qint32 sampleBegin() const;
	qint32 sampleEnd() const;

	void setSampleBegin(qint32 sampleBegin);

	void read(char* dstData, QAudioFormat dstFormat, qint32 dstPos,
			  qint32 srcPos, qint32 samplesCount) const;


	float volume() const;
	void setVolume(float fVolume);

	float direction() const;
	void setDirection(float direction);

private:
	const AudioData* m_pData;

	float m_fVolume;
	float m_fDirection;
	float m_fLeftVolume;
	float m_fRightVolume;

	qint32 m_sampleBegin;
};

#endif // AUDIOSOURCE_H_INCLUDED
