#ifndef SCORE_H_INCLUDED
#define SCORE_H_INCLUDED
/**
 * @file score.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-01
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "patern.h"
#include "resource.h"
#include "scoreatom.h"

class ScoreSection;
class Step;

class Score
{
public:
	Score();
	Score(const Score& other);
	virtual ~Score();

	const Resource& cresource() const;
	Resource& resource();

	const QMap<QString, Step*>& csteps() const;
	QMap<QString, Step*>& steps();
	const Step* cstep(const QString& id) const;
	Step* step(const QString& id);

	const QMap<QString, Patern>& cpaterns() const;
	QMap<QString, Patern>& paterns();
	const Patern& cpatern(const QString& id) const;
	Patern& patern(const QString& id);

	const ScoreSection* cdata() const;
	ScoreSection* data() const;

	quint32 defaultDivision() const;
	void setDefaultDivision(quint32 uDefaultDivision);

private:
	Resource m_resource;
	QMap<QString, Step*> m_mapSteps;
	QMap<QString, Patern> m_lstPaterns;
	ScoreSection* m_pData;
	quint32 m_uDefaultDivision;
};

#endif // SCORE_H_INCLUDED
