#ifndef AUDIOGENERATOR_H_INCLUDED
#define AUDIOGENERATOR_H_INCLUDED
/**
 * @file audiogenerator.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-06
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QIODevice>
#include <QAudioFormat>

#include "audiosource.h"

class AudioGenerator : public QIODevice
{
public:
	explicit AudioGenerator(QAudioFormat format, QObject* parent = Q_NULLPTR);

	void addSource(const AudioSource& source);
	void setLoopPoint(qint32 iLoopPoint);
	qint32 length() const;

	virtual qint64 size() const Q_DECL_OVERRIDE;
	virtual bool isSequential() const Q_DECL_OVERRIDE;
	virtual qint64 pos() const Q_DECL_OVERRIDE;
	virtual bool seek(qint64 lPos) Q_DECL_OVERRIDE;

	QAudioFormat format() const;
	void setFormat(const QAudioFormat& format);

	void setRepeat(const qint32& iRepeat);

protected:
	virtual qint64 readData(char* data, qint64 maxlen) Q_DECL_OVERRIDE;
	virtual qint64 writeData(const char* data, qint64 len) Q_DECL_OVERRIDE;

	void setPos(qint32 iPos) noexcept;

private:
	Q_DISABLE_COPY(AudioGenerator)

	qint32 readSources(char* data, qint32 dstBegin, qint32 dstEnd);

private:
	qint32 m_iPos; // Position en nombre de samples
	qint32 m_iLen;
	qint32 m_iRepeat;
	qint32 m_iLoopPoint;

	QAudioFormat m_format;
	QList<AudioSource> m_lstSources;
};

#endif // AUDIOGENERATOR_H_INCLUDED
