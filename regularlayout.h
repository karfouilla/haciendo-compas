#ifndef REGULARLAYOUT_H_INCLUDED
#define REGULARLAYOUT_H_INCLUDED
/**
 * @file regularlayout.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-20
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtWidgets/QLayout>

class RegularLayout : public QLayout
{
public:
	explicit RegularLayout(QWidget* parent = Q_NULLPTR);
	RegularLayout(Qt::Orientation orient, QWidget* parent = Q_NULLPTR);
	virtual ~RegularLayout();

	virtual void addItem(QLayoutItem* item) Q_DECL_OVERRIDE;
	virtual int count() const Q_DECL_OVERRIDE;
	int visualCount() const;
	virtual QLayoutItem* itemAt(int index) const Q_DECL_OVERRIDE;
	virtual QLayoutItem* takeAt(int index) Q_DECL_OVERRIDE;
	virtual QSize sizeHint() const Q_DECL_OVERRIDE;
	virtual QSize minimumSize() const Q_DECL_OVERRIDE;
	QRectF spacerRect(int num) const;
	QRectF itemRect(int num) const;
	QRectF wdgRect(int num) const;
	int itemAtPos(QPoint pos) const;
	int wdgAtPos(QPoint pos) const;
	int nearestSpacerAtPos(QPoint pos) const;
	int nearestItemAtPos(QPoint pos) const;

	virtual void setGeometry(const QRect& rect) Q_DECL_OVERRIDE;
	void setVisualCount(int iVisualCount);

	Qt::Orientation orientation() const;
	void setOrientation(Qt::Orientation orient);

private:
	Q_DISABLE_COPY(RegularLayout)

	QSize sizeValue(bool minimum) const;
	void getSizes(QRect& effectiveRect, float& wdgLen) const;
	int spacingReal() const;
	int spacingCount() const;
	int spacingSpace() const;

private:
	QList<QLayoutItem*> m_lstItems;
	Qt::Orientation m_orientation;
	int m_iVisualCount;
};

#endif // REGULARLAYOUT_H_INCLUDED
