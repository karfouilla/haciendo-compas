#ifndef SCOREBEAT_H_INCLUDED
#define SCOREBEAT_H_INCLUDED
/**
 * @file scorebeat.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QList>

#include "scoreatom.h"
#include "subdiv.h"

class ScoreBeat : private QList<ScoreAtom>
{
public:
	typedef QList<ScoreAtom>::Iterator It;
	typedef QList<ScoreAtom>::ConstIterator CIt;

public:
	static ScoreBeat generate(quint32 division);

public:
	ScoreBeat();
	ScoreBeat(const ScoreBeat& other);
	ScoreBeat(ScoreBeat&& other);
	ScoreBeat& operator=(const ScoreBeat& other);
	ScoreBeat& operator=(ScoreBeat&& other);

	inline quint32 usize() const { return static_cast<quint32>(size()); }
	inline Subdiv unit() const { return Subdiv(1, usize()); }

	void insertAtom(quint32 i, const ScoreAtom& v);
	inline void removeAtom(quint32 i) { removeAt(static_cast<int>(i)); }
	inline void appendAtom(const ScoreAtom& v) { append(v); }
	inline bool isEmptyAtom() const { return isEmpty(); }
	inline void clearAtom() { clear(); }

	inline Subdiv insertAtom(Subdiv sub, const ScoreAtom& v);
	Subdiv removeAtom(Subdiv sub);


	Subdiv subdivOfIter(CIt citPosition) const;
	quint32 indexOfIter(CIt citPosition) const;
	It iterOfSubdiv(Subdiv sub);
	CIt citerOfSubdiv(Subdiv sub) const;
	quint32 indexOfSubdiv(Subdiv sub) const;
	It iterOfIndex(quint32 index);
	CIt citerOfIndex(quint32 index) const;
	Subdiv subdivOfIndex(quint32 index) const;

	const ScoreAtom& cvalueOfIndex(quint32 index) const;
	const ScoreAtom& cvalueOfIter(CIt cit) const;
	const ScoreAtom& cvalueOfSubdiv(Subdiv sub) const;

	ScoreAtom& valueOfIndex(quint32 index);
	ScoreAtom& valueOfIter(It it);
	ScoreAtom& valueOfSubdiv(Subdiv sub);

	QList<ScoreBeat> multiply(Subdiv factor) const;
	void divise(quint32 factor);
	void undivise(quint32 factor);
	void merge(const ScoreBeat& other, Subdiv from, Subdiv to);
	void copyFrom(const ScoreBeat& other, Subdiv from,
				  Subdiv atFrom, Subdiv atTo);
	void copyAt(const ScoreBeat& other, Subdiv from,
				Subdiv to, Subdiv at);
	void simplify();

private:
};

typedef ScoreBeat::It ScoreAtomListIt;
typedef ScoreBeat::CIt ScoreAtomListCIt;

typedef QList<ScoreBeat> ScoreBeatList;
typedef ScoreBeatList::Iterator ScoreBeatListIt;
typedef ScoreBeatList::ConstIterator ScoreBeatListCIt;

#endif // SCOREBEAT_H_INCLUDED
