/**
 * @file scorewidget.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-24
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "scorewidget.h"

#include <QMouseEvent>

#include "scoreatomwidget.h"
#include "scoreindex.h"
#include "scorelines.h"
#include "scorelinewidget.h"
#include "scoresection.h"

ScoreWidget::ScoreWidget(QWidget* parent):
	QWidget(parent),
	m_lstLines(),
	m_pLayout(),
	m_pModel(Q_NULLPTR),
	m_iBeatPerLines(4),
	m_selection(),
	m_idxCursor()
{
	setFocusPolicy(Qt::StrongFocus);
	m_pLayout = new QVBoxLayout(this);
	setLayout(m_pLayout);
	connect(this, &ScoreWidget::modelChanged, this, &ScoreWidget::updateModel);
}

ScoreSection* ScoreWidget::model() const
{
	return m_pModel;
}

void ScoreWidget::setModel(ScoreSection* pModel)
{
	m_pModel = pModel;
	for(std::size_t i=0; i<CURSOR_LAST; ++i)
		m_idxCursor[i] = m_pModel->beginBeat();
	adjustSelection();
	emit modelChanged(m_pModel);
}

void ScoreWidget::updateModel(ScoreSection* pModel)
{
	QWidget().setLayout(m_pLayout); // hack to clear layout

	m_pLayout = new QVBoxLayout;
	m_pLayout->setSpacing(16);
	m_lstLines.clear();

	if(pModel != Q_NULLPTR)
	{
		ScoreLines* lines(Q_NULLPTR);

		ScoreIndex idx(pModel->beginBeat());
		ScoreIndex startLine(idx);
		while(idx != pModel->endBeat())
		{
			idx = idx.advance(ScoreIndex::AM_NEXT_BEAT);
			if(idx.beatIndex()%static_cast<quint32>(m_iBeatPerLines) == 0)
			{
				lines = new ScoreLines(startLine, idx, this);
				m_lstLines.append(lines);
				startLine = idx;
			}
		}
		lines = new ScoreLines(startLine, idx, this);
		m_lstLines.append(lines);
	}
	foreach(ScoreLines* lines, m_lstLines)
	{
		lines->scoreLineWidget()->setBeatCount(m_iBeatPerLines);
		m_pLayout->addLayout(lines);
	}
	m_pLayout->addSpacerItem(new QSpacerItem(20, 24, QSizePolicy::Minimum,
											 QSizePolicy::MinimumExpanding));
	setLayout(m_pLayout);
}

void ScoreWidget::updateCurrentModel()
{
	adjustCursor();
	adjustSelection();
	updateModel(m_pModel);
}

void ScoreWidget::repaintChildren()
{
	foreach(ScoreLines* lines, m_lstLines)
	{
		lines->scoreLineWidget()->repaint();
	}
}

void ScoreWidget::adjustCursor()
{
	for(std::size_t i=0; i<CURSOR_LAST; ++i)
	{
		quint32 idxBeat(qBound(0u, m_idxCursor[i].beatIndex(), m_pModel->beatCount()));
		Subdiv sub(m_idxCursor[i].subdiv());
		if(idxBeat >= m_pModel->beatCount())
			sub = Subdiv::INVALID;
		else
		{
			quint32 idxAtom(m_pModel->beatAt(idxBeat).indexOfSubdiv(sub));
			sub = m_pModel->beatAt(idxBeat).subdivOfIndex(idxAtom);
		}
		m_idxCursor[i] = ScoreIndex(m_pModel, idxBeat, sub);
	}
}

void ScoreWidget::adjustSelection()
{
	m_selection.setBegin(m_idxCursor[CURSOR_DEFAULT]);
	m_selection.setEnd(m_idxCursor[CURSOR_DEFAULT]);
}

int ScoreWidget::beatPerLines() const
{
	return m_iBeatPerLines;
}

void ScoreWidget::setBeatPerLines(int iBeatPerLines)
{
	if(m_iBeatPerLines != iBeatPerLines)
	{
		m_iBeatPerLines = iBeatPerLines;
		updateCurrentModel();
	}
}

void ScoreWidget::setSelection(const ScoreGroup& selection)
{
	if(m_selection.begin() != selection.begin() ||
			m_selection.end() != selection.end())
	{
		m_selection = selection;
		emit selectionChanged(m_selection);
	}
}

ScoreGroup ScoreWidget::selection() const
{
	return m_selection;
}

void ScoreWidget::mousePressEvent(QMouseEvent* event)
{
	QWidget::mousePressEvent(event);
	if(event->button() == Qt::LeftButton)
	{
		setSelection(ScoreGroup());
	}
}

void ScoreWidget::keyPressEvent(QKeyEvent* event)
{
	QWidget::keyPressEvent(event);
	event->ignore();
	if(event->key() == Qt::Key_Left)
	{
		for(std::size_t i=0; i<CURSOR_LAST; ++i)
		{
			ScoreIndex pos(scoreCursor(i));
			pos.normalize(false);
			if(!pos.isBeginOfBeat())
			{
				pos = pos.advance(ScoreIndex::AM_PREV_ATOM);
			}
			else
			{
				if(!pos.isBegin())
				{
					pos = pos.advance(ScoreIndex::AM_PREV_BEAT);
					pos = pos.advance(ScoreIndex::AM_LAST_ATOM);
				}
			}
			setScoreCursor(i, pos);
		}
		event->accept();
	}
	if(event->key() == Qt::Key_Right)
	{
		for(std::size_t i=0; i<CURSOR_LAST; ++i)
		{
			ScoreIndex pos(scoreCursor(i));
			pos.normalize(false);
			if(!pos.isLastOfBeat())
			{
				pos = pos.advance(ScoreIndex::AM_NEXT_ATOM);
			}
			else
			{
				if(!pos.isEnd())
				{
					pos = pos.advance(ScoreIndex::AM_NEXT_BEAT);
					if(!pos.isEnd())
						pos = pos.advance(ScoreIndex::AM_FIRST_ATOM);
				}
			}
			setScoreCursor(i, pos);
		}
		event->accept();
	}
	if(event->key() == Qt::Key_Up)
	{
		event->accept();
	}
	if(event->key() == Qt::Key_Down)
	{
		event->accept();
	}
	if(event->key() == Qt::Key_PageUp)
	{
		event->accept();
	}
	if(event->key() == Qt::Key_PageDown)
	{
		event->accept();
	}
}

void ScoreWidget::focusInEvent(QFocusEvent* event)
{
	QWidget::focusInEvent(event);
	repaintChildren();
}

void ScoreWidget::focusOutEvent(QFocusEvent* event)
{
	QWidget::focusInEvent(event);
	repaintChildren();
}

ScoreIndex ScoreWidget::scoreCursor(std::size_t cursorName) const
{
	return m_idxCursor[cursorName];
}
void ScoreWidget::setScoreCursor(std::size_t cursorName,
								 const ScoreIndex& idxCursor,
								 bool updateSelection)
{
	if(m_idxCursor[cursorName] != idxCursor)
	{
		m_idxCursor[cursorName] = idxCursor;
		if(cursorName == CURSOR_DEFAULT && updateSelection)
			adjustSelection();
		emit scoreCursorChanged(cursorName, idxCursor);
	}
}
