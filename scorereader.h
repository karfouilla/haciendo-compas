#ifndef SCOREREADER_H_INCLUDED
#define SCOREREADER_H_INCLUDED
/**
 * @file scorereader.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-05
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QIODevice>
#include <QtCore/QXmlStreamReader>

#include "score.h"

class ScoreBeat;

class ScoreReader
{
public:
	ScoreReader(QIODevice* device);
	bool read(Score* score);

private:
	Q_DISABLE_COPY(ScoreReader)

	bool checkBegin(const QString& element);
	bool checkEnd(const QString& element);

	bool readResource(Resource& res);
	bool readSteps(QMap<QString, Step*>& steps);
	bool readStep(Step* step);
	bool readResourceName(Resource::Name& resourceName);
	bool readPaterns(QMap<QString, Patern>& paterns);
	bool readPatern(Patern& patern);
	bool readAtom(ScoreAtom& atom);
	bool readSection(ScoreSection* section);
	bool readBeat(ScoreBeat& beat);

private:
	QIODevice* m_pFile;
	Score* m_pScore;
	QXmlStreamReader m_reader;
};

#endif // SCOREREADER_H_INCLUDED
