/**
 * @file dockscore.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-07-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "dockscore.h"
#include "ui_dockscore.h"

#include "scoresection.h"

static const QVector<int> buttonDefault({1, 2, 3, 4, 6, 8, 12});

DockScore::DockScore(Score* pScore, ScoreWidget* wdgScore, QWidget* parent):
	QWidget(parent),
	ui(new Ui::DockScore),
	m_lstBeatDiv(),
	m_sbBeatDiv(Q_NULLPTR),
	m_pScore(pScore),
	m_wdgScore(wdgScore),
	m_index()
{
	ui->setupUi(this);

	for(int i=0; i<buttonDefault.size(); ++i)
	{
		int value(buttonDefault.at(i));
		QToolButton* button(new QToolButton(this));
		button->setText(QString("%1").arg(value));
		connect(button, &QToolButton::clicked,
				[this, value](){ m_sbBeatDiv->setValue(value); });
		ui->layoutBeatDiv->addWidget(button);
		m_lstBeatDiv.append(button);
	}
	m_sbBeatDiv = new QSpinBox(this);
	m_sbBeatDiv->setMinimum(1);
	m_sbBeatDiv->setMaximum(48);
	m_sbBeatDiv->setValue(m_pScore->defaultDivision());

	connect(m_sbBeatDiv, QOverload<int>::of(&QSpinBox::valueChanged),
			this, &DockScore::setDivision);

	ui->layoutBeatDiv->addWidget(m_sbBeatDiv);

	ui->layoutBeatDiv->addSpacerItem(new QSpacerItem(0, 0,
													 QSizePolicy::Expanding));

	// Cursor position setting

	connect(ui->cbAccent, &QCheckBox::toggled, this, &DockScore::accentToggled);
	connect(ui->cbTempo, &QCheckBox::toggled, this, &DockScore::updateTempo);
	connect(ui->cbVolume, &QCheckBox::toggled, this, &DockScore::updateVolume);

	connect(ui->sbTempo, QOverload<int>::of(&QSpinBox::valueChanged),
			this, &DockScore::updateTempo);
	connect(ui->sliderVolume, &QSlider::valueChanged,
			this, &DockScore::updateVolume);

	connect(m_wdgScore, &ScoreWidget::scoreCursorChanged,
			this, [this](std::size_t cursorName, ScoreIndex idx) {
		if(cursorName == ScoreWidget::CURSOR_DEFAULT)
			setIndex(idx);
	});
	connect(m_wdgScore->model(), &ScoreSection::changed, [this]() {
		setIndex(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT));
	});

	setIndex(m_wdgScore->scoreCursor(ScoreWidget::CURSOR_DEFAULT)); // update default
}

DockScore::~DockScore()
{
	delete ui;
}

void DockScore::setTempo(bool enable, bool activate, int value)
{
	ui->cbTempo->setEnabled(enable);
	if(enable)
		ui->cbTempo->setChecked(activate);
	else
		ui->cbTempo->setChecked(false); // useless

	ui->sbTempo->setEnabled(enable && activate);
	ui->sbTempo->setValue(value);
}
void DockScore::setVolume(bool enable, bool activate, float value)
{
	ui->cbVolume->setEnabled(enable);
	if(enable)
		ui->cbVolume->setChecked(activate);
	else
		ui->cbVolume->setChecked(false); // useless

	ui->sliderVolume->setEnabled(enable && activate);
	ui->sliderVolume->setValue(qRound(100.f*value));

	ui->sbVolume->setEnabled(enable && activate);
}
void DockScore::setAccent(bool enable, bool activate)
{
	ui->cbAccent->setEnabled(enable);
	ui->cbAccent->setChecked(activate);
}

void DockScore::updateTempo()
{
	setTempo(ui->cbTempo->isEnabled(), ui->cbTempo->isChecked(),
			 ui->sbTempo->value());

	if(m_index.section() && !m_index.isEnd())
	{
		ScoreAtom atom(m_index.atom());
		if(ui->cbTempo->isChecked())
			atom.setTempo(ui->sbTempo->value());
		else
			atom.setDefaultTempo();

		m_wdgScore->model()->emplace(m_index, atom);
	}
}

void DockScore::updateVolume()
{
	float volume(static_cast<float>(ui->sliderVolume->value())/100.f);
	setVolume(ui->cbVolume->isEnabled(), ui->cbVolume->isChecked(),
			  volume);

	if(m_index.section() && !m_index.isEnd())
	{
		ScoreAtom atom(m_index.atom());
		if(ui->cbVolume->isChecked())
			atom.setVolume(volume);
		else
			atom.setDefaultVolume();

		m_wdgScore->model()->emplace(m_index, atom);
	}
}

void DockScore::setIndex(const ScoreIndex& index)
{
	m_index = index;
	if(m_index.section() && !m_index.isEnd())
	{
		const ScoreAtom& atom(m_index.atom());

		// tempo
		ui->cbTempo->setEnabled(true);
		if(atom.tempo())
		{
			setTempo(true, true, *atom.tempo());
		}
		else
		{
			setTempo(true, false, 80);
			// 80 -> index.section()->tempoAt(index);
		}

		// volume
		if(atom.volume())
		{
			setVolume(true, true, *atom.volume());
		}
		else
		{
			setVolume(true, false, 1.f);
			// 1.f -> index.section()->volumeAt(index);
		}

		// accent
		setAccent(atom.step() != Q_NULLPTR, atom.haveAccent());
	}
	else
	{
		// tempo
		setTempo(false, false, 80);
		// 80 -> index.section()->tempoAt(atom);

		// volume
		setVolume(false, false, 1.f);
		// 1.f -> index.section()->volumeAt(index);

		// accent
		setAccent(false, false);
	}
}

void DockScore::accentToggled(bool haveAccent)
{
	if(m_index.section() && !m_index.isEnd())
	{
		ScoreAtom atom(m_index.atom());
		if(haveAccent != atom.haveAccent())
		{
			atom.setHaveAccent(haveAccent);
			m_wdgScore->model()->emplace(m_index, atom);
		}
	}
}

void DockScore::setDivision(int but)
{
	m_pScore->setDefaultDivision(static_cast<quint32>(but));
}
