#ifndef SCORELINES_H_INCLUDED
#define SCORELINES_H_INCLUDED
/**
 * @file scorelines.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-22
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtWidgets/QVBoxLayout>

#include "scoreindex.h"

class ScoreLineWidget;
class ScoreWidget;

class ScoreLines : public QVBoxLayout
{
public:
	ScoreLines(ScoreIndex begin, ScoreIndex end,
			   ScoreWidget* pParent);

	ScoreIndex begin() const;
	ScoreIndex end() const;

	ScoreLineWidget* scoreLineWidget() const;
	ScoreWidget* scoreWiget();

private:
	ScoreIndex m_idxBegin;
	ScoreIndex m_idxEnd;
	ScoreLineWidget* m_wdgScoreLine;
	ScoreWidget* m_wdgParentScore;
};

#endif // SCORELINES_H_INCLUDED
