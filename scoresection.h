#ifndef SCORESECTION_H_INCLUDED
#define SCORESECTION_H_INCLUDED
/**
 * @file scoresection.h
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-10
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtCore/QList>
#include <QtCore/QObject>

#include "patern.h"
#include "scoreatom.h"
#include "scoregroup.h"
#include "scoreindex.h"
#include "subdiv.h"

class ScoreSection : public QObject
{
	Q_OBJECT
public:
	explicit ScoreSection(QObject* parent = Q_NULLPTR);
	ScoreSection(const ScoreSection& other);
	ScoreSection& operator=(const ScoreSection& other);

	quint32 indexOfBeatIter(ScoreBeatListCIt itBeat) const Q_DECL_NOTHROW;
	ScoreBeatListCIt citerOfBeatIndex(quint32 uBeat) const Q_DECL_NOTHROW;
	inline quint32 beatCount() const {
		return static_cast<quint32>(m_lstBeats.size());
	}

	const ScoreBeat& beatAt(quint32 uBeat) const Q_DECL_NOTHROW;

	ScoreIndex insert(ScoreIndex position, const ScoreBeat& beat);
	ScoreIndex insert(ScoreIndex position, const ScoreAtom& atom);
	ScoreIndex insertPatern(ScoreIndex position, quint32 division,
							const Patern& patern);
	ScoreIndex emplacePatern(ScoreIndex position, quint32 division,
							 const Patern& patern);
	ScoreSection* createPatern(quint32 division,
							   const Patern& patern);
	ScoreIndex insert(ScoreIndex position, const ScoreSection& group);
	ScoreIndex emplace(ScoreIndex position, const ScoreBeat& rBeat);
	ScoreIndex emplace(ScoreIndex position, const ScoreAtom& atom);
	ScoreIndex emplace(ScoreIndex position, const ScoreSection& group);
	ScoreIndex erase(ScoreIndex position);
	ScoreIndex eraseFrom(ScoreIndex position);
	ScoreIndex erase(ScoreIndex begin, ScoreIndex end);

	ScoreIndex beginBeat() const Q_DECL_NOTHROW;
	ScoreIndex endBeat() const Q_DECL_NOTHROW;
	ScoreIndex index(ScoreBeatListIt itBeat,
					 Subdiv sub = Subdiv()) const Q_DECL_NOTHROW;
	ScoreIndex index(quint32 beat, Subdiv sub = Subdiv()) const Q_DECL_NOTHROW;

	Subdiv size() const Q_DECL_NOTHROW;

	Subdiv beginAt() const Q_DECL_NOTHROW;
	Subdiv endAt() const Q_DECL_NOTHROW;

	void setBeginAt(const Subdiv& beginAt) Q_DECL_NOTHROW;
	void setEndAt(const Subdiv& endAt) Q_DECL_NOTHROW;

	ScoreSection* multiply(Subdiv factor) const;
	ScoreSection* peek(ScoreIndex begin, ScoreIndex end) const;
	ScoreSection* take(ScoreIndex begin, ScoreIndex end);

	inline ScoreSection* peek(const ScoreGroup& group) const
	{
		return peek(group.begin(), group.end());
	}
	inline ScoreSection* take(const ScoreGroup& group)
	{
		return take(group.begin(), group.end());
	}

	ScoreGroup lastOp() const;
	void simplify();

signals:
	void changed();

protected:
	inline void insertBeat(quint32 i, const ScoreBeat& v) {
		m_lstBeats.insert(static_cast<int>(i), v);
	}
	inline void removeBeat(quint32 i) {
		m_lstBeats.removeAt(static_cast<int>(i));
	}

private:
	ScoreBeat& beat(quint32 uBeat);
	ScoreBeatListIt iterOfBeatIndex(quint32 uBeat) Q_DECL_NOTHROW;
	quint32 commonBeatDiv() const Q_DECL_NOTHROW;
	void shiftBeat(int count);
	ScoreIndex emplaceBeat(ScoreIndex position, const ScoreBeat &rBeat,
						   Subdiv from, Subdiv to);

private:
	ScoreBeatList m_lstBeats;
	ScoreGroup m_gpLastOp;
	Subdiv m_beginAt; // Taille de la première mesure (pour mesure incomplète)
	Subdiv m_endAt; // Taille de la dernière mesure (pour mesure incomplète)
};

#endif // SCORESECTION_H_INCLUDED
