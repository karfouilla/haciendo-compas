/**
 * @file regularlayout.cpp
 * @author Costa Séraphin
 * @version 0.1
 * @date 2019-06-20
 */
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Haciendo Compas.
//
// Haciendo Compas is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Haciendo Compas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Haciendo Compas.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "regularlayout.h"

#include <QtCore/QtMath>
#include <QtWidgets/QWidget>
#include <QtWidgets/QStyle>

RegularLayout::RegularLayout(QWidget* parent):
	QLayout(parent),
	m_lstItems(),
	m_orientation(Qt::Horizontal),
	m_iVisualCount(-1)
{ }

RegularLayout::RegularLayout(Qt::Orientation orient, QWidget* parent):
	QLayout(parent),
	m_lstItems(),
	m_orientation(orient),
	m_iVisualCount(-1)
{ }

RegularLayout::~RegularLayout()
{
	while(!isEmpty())
		delete takeAt(0);
}

void RegularLayout::addItem(QLayoutItem* item)
{
	m_lstItems.append(item);
}

int RegularLayout::count() const
{
	return m_lstItems.size();
}

int RegularLayout::visualCount() const
{
	return ((m_iVisualCount > 0) ? m_iVisualCount : count());
}

QLayoutItem* RegularLayout::itemAt(int index) const
{
	return m_lstItems.value(index);
}

QLayoutItem* RegularLayout::takeAt(int index)
{
	return m_lstItems.takeAt(index);
}

void RegularLayout::setGeometry(const QRect& rect)
{
	QLayout::setGeometry(rect);

	if(m_lstItems.isEmpty())
		return;

	QRect effectiveRect;
	float wdgSize;

	getSizes(effectiveRect, wdgSize);

	QPointF advance(0.f, 0.f);
	QSizeF size(0.f, 0.f);
	advance.setX(spacingReal()+wdgSize);
	size.setWidth(wdgSize);
	size.setHeight(effectiveRect.height());

	if(m_orientation == Qt::Vertical)
	{
		effectiveRect = effectiveRect.transposed();
		qSwap(advance.rx(), advance.ry());
		size.transpose();
	}

	QLayoutItem* item(m_lstItems.first());
	QPointF pos(effectiveRect.topLeft());
	QRectF wdgRect(pos, size);
	item->setGeometry(wdgRect.toRect());

	for(int i=1; i<count(); ++i)
	{
		item = m_lstItems[i];
		wdgRect.translate(advance);
		item->setGeometry(wdgRect.toRect());
	}
}

void RegularLayout::setVisualCount(int iVisualCount)
{
	m_iVisualCount = iVisualCount;
}

QSize RegularLayout::sizeHint() const
{
	return sizeValue(false);
}

QSize RegularLayout::minimumSize() const
{
	return sizeValue(true);
}

QRectF RegularLayout::spacerRect(int num) const
{
	QRect effectiveRect;
	float wdgSize;
	getSizes(effectiveRect, wdgSize);

	QSize spacerSize(spacingReal(), effectiveRect.height());

	float spacerShift(num*(spacerSize.width() + wdgSize) - spacerSize.width());
	spacerShift += effectiveRect.left();

	QPointF spacerPos(spacerShift, effectiveRect.top());

	if(m_orientation == Qt::Vertical)
	{
		spacerSize.transpose();
		qSwap(spacerPos.rx(), spacerPos.ry());
	}
	return QRectF(spacerPos, spacerSize);
}

QRectF RegularLayout::itemRect(int num) const
{
	QRect effectiveRect;
	float wdgLen;
	getSizes(effectiveRect, wdgLen);

	float wdgShift(num*(spacingReal() + wdgLen));
	wdgShift += effectiveRect.left();

	QPointF wdgPos(wdgShift, effectiveRect.top());
	QSizeF wdgSize(wdgLen, effectiveRect.height());

	if(m_orientation == Qt::Vertical)
	{
		wdgSize.transpose();
		qSwap(wdgPos.rx(), wdgPos.ry());
	}
	return QRectF(wdgPos, wdgSize);
}

QRectF RegularLayout::wdgRect(int num) const
{
	return m_lstItems.at(num)->geometry();
}

int RegularLayout::itemAtPos(QPoint pos) const
{
	QRect effectiveRect;
	float wdgLen;
	float adjustedPos;

	getSizes(effectiveRect, wdgLen);

	if(m_orientation == Qt::Horizontal)
		adjustedPos = static_cast<float>(pos.x() - effectiveRect.left());
	else
		adjustedPos = static_cast<float>(pos.y() - effectiveRect.left());

	int idx(qFloor(adjustedPos/(wdgLen + spacingReal())));
	if(idx < 0 || idx >= count())
		return -1;
	else if(itemRect(idx).contains(pos))
		return idx;
	else
		return -1;
}

int RegularLayout::wdgAtPos(QPoint pos) const
{
	QRect effectiveRect;
	float wdgLen;
	float adjustedPos;

	getSizes(effectiveRect, wdgLen);

	if(m_orientation == Qt::Horizontal)
		adjustedPos = static_cast<float>(pos.x() - effectiveRect.left());
	else
		adjustedPos = static_cast<float>(pos.y() - effectiveRect.left());

	int idx(qFloor(adjustedPos/(wdgLen + spacingReal())));
	if(idx < 0 || idx >= count())
		return -1;
	else if(wdgRect(idx).contains(pos))
		return idx;
	else
		return -1;
}

int RegularLayout::nearestSpacerAtPos(QPoint pos) const
{
	QRect effectiveRect;
	float wdgLen;
	float adjustedPos;

	getSizes(effectiveRect, wdgLen);

	if(m_orientation == Qt::Horizontal)
		adjustedPos = static_cast<float>(pos.x() - effectiveRect.left());
	else
		adjustedPos = static_cast<float>(pos.y() - effectiveRect.left());

	float fWdgSpace(wdgLen + spacingReal());
	float fShift(wdgLen/2.f + spacingReal());
	int idx(qFloor((adjustedPos+fShift)/fWdgSpace));
	return qBound(0, idx, count());
}

int RegularLayout::nearestItemAtPos(QPoint pos) const
{
	QRect effectiveRect;
	float wdgLen;
	float adjustedPos;

	getSizes(effectiveRect, wdgLen);

	if(m_orientation == Qt::Horizontal)
		adjustedPos = static_cast<float>(pos.x() - effectiveRect.left());
	else
		adjustedPos = static_cast<float>(pos.y() - effectiveRect.left());

	float fWdgSpace(wdgLen + spacingReal());
	float fShift(spacingReal()/2.f);
	int idx(qFloor((adjustedPos+fShift)/fWdgSpace));
	return qMax(0, qMin(idx, count()-1));
}

QSize RegularLayout::sizeValue(bool minimum) const
{
	QSize maxWdgSize(0, 0);
	for(int i=0; i<count(); ++i)
	{
		if(minimum)
			maxWdgSize = maxWdgSize.expandedTo(m_lstItems[i]->minimumSize());
		else
			maxWdgSize = maxWdgSize.expandedTo(m_lstItems[i]->sizeHint());
	}

	int widgetSize(visualCount());
	if(m_orientation == Qt::Vertical)
		widgetSize *= maxWdgSize.height();
	else
		widgetSize *= maxWdgSize.width();

	int left, top, right, bottom; // marges
	getContentsMargins(&left, &top, &right, &bottom);
	QSize margin(top+bottom, left+right);

	if(m_orientation == Qt::Vertical)
		return QSize(maxWdgSize.width(), widgetSize + spacingSpace()) + margin;
	else
		return QSize(widgetSize + spacingSpace(), maxWdgSize.height()) + margin;
}

void RegularLayout::getSizes(QRect& effectiveRect, float& wdgSize) const
{
	QRect rect(geometry());

	int left, top, right, bottom; // marges
	getContentsMargins(&left, &top, &right, &bottom);

	effectiveRect = rect.adjusted(+left, +top, -right, -bottom);

	if(m_orientation == Qt::Vertical)
		effectiveRect = effectiveRect.transposed();

	float availableSize(effectiveRect.width() - spacingSpace());
	wdgSize = availableSize / static_cast<float>(visualCount());
}

int RegularLayout::spacingReal() const
{
	if(spacing() < 0)
		return 1;
	else
		return spacing();
}

int RegularLayout::spacingCount() const
{
	return qMax(0, visualCount()-1);
}

int RegularLayout::spacingSpace() const
{
	return (spacingCount() * spacingReal());
}

Qt::Orientation RegularLayout::orientation() const
{
	return m_orientation;
}

void RegularLayout::setOrientation(Qt::Orientation orient)
{
	m_orientation = orient;
}
